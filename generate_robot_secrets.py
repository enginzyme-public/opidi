import secrets
import json


if __name__ == "__main__":
    # Generate a random secret key

    robots = ["ot2-isabella", "ot2-ceres", "ot2-hercules"]
    tokens = {}
    for robot in robots:
        tokens[robot] = secrets.token_hex(32)

    # Save the secret key to a JSON file
    with open("robot_secrets.json", "w") as f:
        json.dump(tokens, f)

    print("Robot secrets generated and saved to robot_secrets.json")