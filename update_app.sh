#!/usr/bin/env bash

## This script is used to deploy a specific version of the app

case $1 in
    -h|--help)
        echo "Usage: $0 <version>"
        echo "Deploy a specific version of the app"
        return 0
        ;;
    *)
        # Continue with the script
        ;;
esac


# Check if a version argument is provided
if [ $# -eq 0 ]; then
    echo "Error: No version argument provided."
    echo "Usage: $0 <version>"
    exit 1
fi

# Store the version argument
VERSION=$1

# Fetch from origin
git fetch origin

# Check if the version is a valid tag
if [ -z "$(git tag -l "$VERSION")" ]; then
    echo "Error: The version $VERSION is not a valid. Please check the correct version."
    return 1
fi

echo "Deploying version $VERSION"

## Check if there are any uncommitted changes
if ! git diff-index --quiet HEAD --; then
    echo "Error: There are uncommitted changes in the working directory."
    echo "Please clean your repo before running an update."
    return 1
fi

# Try to checkout the tag
if ! git checkout tags/"$VERSION" 2>/dev/null; then
    echo "Error: Tag $VERSION not found."
    return 1
fi

# If we've reached this point, the tag exists and has been checked out


# Update APP_VERSION in .env file
if [ -f .env ]; then
    sed -i "s/^APP_VERSION=.*/APP_VERSION=$VERSION/" .env
    echo "Updated APP_VERSION to $VERSION in .env file"
else
    echo "APP_VERSION=$VERSION" > .env
    echo "Created .env file with APP_VERSION=$VERSION"
fi

# Run Docker Compose commands
docker compose -f docker-compose.prod.yml down
docker compose -f docker-compose.prod.yml build
docker compose -f docker-compose.prod.yml up -d

echo "Successfully deployed version $VERSION"