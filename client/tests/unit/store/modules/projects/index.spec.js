import projectModule from "@/store/modules/projects";
import types from "@/types";

jest.mock('@/config/containers', () => ({
  __esModule: true,
  backendClient: {
    get: jest.fn(),
    post: jest.fn(),
    patch: jest.fn(),
    delete: jest.fn(),
  },
}));

describe("project module getters", () => {
  const state = {
    projects: [],
  };
  test("has correctly functioning 'projects' member", () => {
    expect(projectModule.getters.projects(state)).toStrictEqual(state.projects);
  });
});

describe("project module mutations", () => {
  test("has correctly functioning 'types.LOAD_PROJECT_OBJECTS' member", () => {
    const state = {
      projects: [],
    };
    const objects = [{}];
    projectModule.mutations[types.LOAD_PROJECT_OBJECTS](
      state,
      objects
    );
    expect(state.projects).toStrictEqual(objects);
  });
  test("has correctly functioning 'types.UNLOAD_PROJECT_OBJECTS' member", () => {
    const state = {
      projects: [],
    };
    projectModule.mutations[types.UNLOAD_PROJECT_OBJECTS](state);
    expect(state.projects).toStrictEqual([]);
  });
});
