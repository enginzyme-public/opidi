import authModule from "@/store/modules/auth";
import { AuthGetter, AuthMutation } from "../../../../src/types/store-modules/AuthTypes";

jest.mock('@/config/containers');

describe("auth module getters", () => {
  const state = {
    user: {
      name: "",
      email: "",
      imageURL: "",
      aToken: "",
      tokenRefresher: () => {},
      tokenExpiryTime: 0,
    },
  };
  test("has correctly functioning 'AuthGetter.userIsLoggedIn' member", () => {
    expect(authModule.getters[AuthGetter.userIsLoggedIn](state)).toStrictEqual(true);
  });
  test("has correctly functioning 'AuthGetter.userImageURL' member", () => {
    expect(authModule.getters[AuthGetter.userImageURL](state)).toStrictEqual(state.user.imageURL);
  });
  test("has correctly functioning 'AuthGetter.userName' member", () => {
    expect(authModule.getters[AuthGetter.userName](state)).toStrictEqual(state.user.name);
  });
  test("has correctly functioning 'AuthGetter.userEmail' member", () => {
    expect(authModule.getters[AuthGetter.userEmail](state)).toStrictEqual(state.user.email);
  });
});

describe("auth module mutations", () => {
  test("has correctly functioning 'AuthMutation.registerUser' member", () => {
    const state = {
      user: null,
    };
    const newUser = {
      name: "name",
      email: "email",
      imageURL: "url",
    };

    authModule.mutations[AuthMutation.registerUser](state, { user: newUser });
    expect(state.user.name).toStrictEqual(newUser.name);
    expect(state.user.email).toStrictEqual(newUser.email);
    expect(state.user.imageURL).toStrictEqual(newUser.imageURL);
  });
  test("has correctly functioning 'AuthMutation.deregisterUser' member", () => {
    const state = {
      user: {
        name: "name",
        email: "email",
        imageURL: "url",
      },
    };
    authModule.mutations[AuthMutation.deregisterUser](state);
    expect(state.user).toStrictEqual(null);
  });
});
