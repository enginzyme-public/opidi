import { findDeep, deleteDeep, duplicateDeep } from "./nestedObjectActions";
import { getFloatArrayFromString } from "./stringManipulation";

export {
  findDeep,
  deleteDeep,
  duplicateDeep,
  getFloatArrayFromString,
};
