/**
 * Identifies and returns all numbers within the input string in an array
 * @param {String} string - The input string from with the array of numbers is to be extracted
 * @returns {Array.<Number>} - An array of numbers extracted from the input string
 */
export function getFloatArrayFromString(string) {
  const regex = /[+-]?\d+(\.\d+)?/g;
  return string.match(regex).map((item) => parseFloat(item));
}

/**
 * Escapes a string, so it can be used inside a regex.
 *
 * @see https://stackoverflow.com/a/3561711/260389
 * @param {string} string 
 * @returns The escaped string.
 */
export function escapeRegex(string) {
  // eslint-disable-next-line no-useless-escape
  return string.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
}

/**
 * Transforms camelCase or PascalCase string to snake_case
 * @param {string} string 
 * @returns The snake_case string
 */
export function camelCaseToSnakeCase(string) {
  return string.replace(/[A-Z]/g, letter => `_${letter.toLowerCase()}`)
    .substring((string.slice(0, 1).match(/([A-Z])/g)) ? 1 : 0);
}

/**
 * Transform all the keys of an object from camel case to snake case.
 * @param {*} object 
 * @returns The object with the new keys.
 */
export function deepCamelCaseToSnakeCase(object) {
  if (Array.isArray(object)) {
    return object.map(deepCamelCaseToSnakeCase);
  }

  if (object && typeof object === 'object') {
    return Object.fromEntries((Object.entries(object).map(([key, value]) => [camelCaseToSnakeCase(key), deepCamelCaseToSnakeCase(value)])));
  }

  return object;
}
