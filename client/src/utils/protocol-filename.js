import { TEAMS_MAP } from '../constants/teams';

function twoDigits(string) {
  return `0${string}`.slice(-2);
}

export default function cachedProtocolToFilename(protocol) {
  const now = new Date();

  const date = `\
${now.getFullYear()}-\
${twoDigits(now.getMonth()+1)}-\
${twoDigits(now.getDate())}_\
${twoDigits(now.getHours())}-\
${twoDigits(now.getMinutes())}-\
${twoDigits(now.getSeconds())}`;


  return `\
${protocol.data.metadata.name.replaceAll(' ', '_')}_\
${protocol.data.metadata.subcategory || 'unknown'}_\
${TEAMS_MAP[protocol.data.metadata.team] ?
    TEAMS_MAP[protocol.data.metadata.team].shortName
    : 'unknown'}_\
${date}`;
}
