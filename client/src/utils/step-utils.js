import { nth } from "./number-utils";
import { escapeRegex } from "./stringManipulation";

/**
 * If step name already exists, it returns a safe version of the name, as name repetition is forbidden.
 * @param {String} stepName The intended step name
 * @param {String[]} flattedSteps The list of all the steps, flatten.
 * @param {Boolean} includesSelf Optional. If present and true, then it will assume that the given step name is already
 * present in the flattedSteps array.
 * @returns 
 */
export function getSafeStepName(stepName, flattedSteps, includesSelf = false) {
  const stepNames = flattedSteps.map(({ name }) => name);

  let count = flattedSteps.reduce((count, { name }) => count + (name === stepName ? 1 : 0), 0);

  if (includesSelf) {
    count -= 1; // removing self
  }
  if (count === 0) {
    return stepName;
  }

  let originalName = stepName;
  const match = /^(.*)\( (?:(11th|12th|13th|\d*(?:1st|2nd|3rd|[04-9]th)))?\)$/.exec(stepName);
  if (match) {
    [, originalName] = match;
  }

  // Count the number of steps that were cloned form the same original name.
  const expression = new RegExp(`^${escapeRegex(originalName)}( \\((?:(11th|12th|13th|\\d*(?:1st|2nd|3rd|[04-9]th)))?\\))?$`);
  count = stepNames.reduce((count, stepName) => {
    const doesMatch = expression.test(stepName);
    return count + (doesMatch ? 1 : 0);
  }, 0);

  if (includesSelf) {
    count -= 1; // removing self
  }

  return `${originalName} (${nth(count + 1)})`;
}

/**
 * Returns a copy of the given steps array with all the index based positions added (for example: `3.1.2`).
 *
 * @param {Step[]} steps Array of steps
 * @param {String | null} parentPosition Optional. The string that represents
 * the position of the parent step. Null by Default.
 * @returns 
 */
export function addPositionToSteps(steps, parentPosition = null) {
  return steps.map((step, index) => {
    const position = `${parentPosition !== null ? `${parentPosition}.` : ''}${index + 1}`;
    return {
      ...step,
      position,
      substeps: addPositionToSteps(step.substeps || [], position),
    };
  })
}
