/**
 * Minimal one-line approach for ordinal suffixes
 *
 * @see https://stackoverflow.com/a/39466341/260389
 * @param {number} n A number
 * @returns the ordinal of the given number
 */
export function nth(n){
  return `${n}${["st","nd","rd"][((n+90)%100-10)%10-1]||"th"}`;
}
