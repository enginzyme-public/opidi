import Vue from "vue";
import VueRouter from "vue-router";

import store from '../store';
import { AuthGetter } from "../types/store-modules/AuthTypes";

import ProtocolDesignerPage from "../pages/ProtocolDesignerPage";
import ProtocolDetails from "../pages/ProtocolDetails";
import AuthenticationPage from "../pages/AuthenticationPage";
import PageNotFoundPage from "../pages/PageNotFoundPage";
import ProtocolSidePanel from "../components/layout/ProtocolSidePanel";
import ProtocolEditorSidePanel from "../components/layout/ProtocolEditorSidePanel";
import TopNavBar from "../components/layout/TopNavBar";
import ProtocolEditorTopNavBar from "../components/layout/ProtocolEditorTopNavBar";
import CreateProtocolForm from "../components/forms/CreateProtocolForm";
import ProtocolMetaDataForm from "../components/forms/ProtocolMetaDataForm";
import TeamsView from "../components/views/TeamsView";
import ProtocolsView from "../components/views/ProtocolsView";
import DeckSetupView from "../components/views/DeckSetupView";
import SequencesView from "../components/views/SequencesView";
import StepsView from "../components/views/StepsView";
import GenerateView from "../components/views/GenerateView";
import ConfigView from "../components/views/ConfigView";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: "/create",
    name: "ProtocolDesignerPage",
    component: ProtocolDesignerPage,
    meta: { requiresAuth: true },
    children: [
      {
        path: "create",
        name: "create-protocol",
        components: {
          LeftSidePanel: ProtocolSidePanel,
          DisplayView: CreateProtocolForm,
          TopNavBar: TopNavBar,
        },
      },
      {
        path: "explore",
        name: "explore-protocols-root",
        components: {
          LeftSidePanel: ProtocolSidePanel,
          DisplayView: TeamsView,
          TopNavBar: TopNavBar,
        },
      },
      {
        path: "explore/:teamId",
        name: "explore-protocols",
        components: {
          LeftSidePanel: ProtocolSidePanel,
          DisplayView: ProtocolsView,
          TopNavBar: TopNavBar,
        },
      },
      {
        path: "protocol/:id",
        name: 'protocol-root',
        components: {
          LeftSidePanel: ProtocolEditorSidePanel,
          DisplayView: ProtocolDetails,
          TopNavBar: ProtocolEditorTopNavBar,
        },
        children: [
          {
            path: "metadata",
            name: 'protocol-metadata',
            component: ProtocolMetaDataForm,
          },
          {
            path: "deck",
            name: 'protocol-deck',
            component: DeckSetupView,
          },
          {
            path: "sequences",
            name: 'protocol-sequences',
            component: SequencesView,
          },
          {
            path: "steps",
            name: 'protocol-steps',
            component: StepsView,
          },
          {
            path: "generate",
            name: 'protocol-generate',
            component: GenerateView,
          },
          {
            path: "config",
            name: 'protocol-config',
            component: ConfigView,
          },
        ],
      },
    ],
  },
  {
    path: "/auth",
    name: "Authentication",
    component: AuthenticationPage,
    meta: { requiresAnonymity: true },
  },
  {
    path: "*",
    name: "PageNotFound",
    component: PageNotFoundPage,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

// Authentication or anonimity required
router.beforeEach((to, _from, next) => {
  const userIsLoggedIn = store.getters[AuthGetter.userIsLoggedIn];

  if (to.matched.some((record) => record.meta.requiresAuth) && !userIsLoggedIn) {
    return next({
      name: 'Authentication',
      query: { redirect: to.query.redirect || to.fullPath },
    });
  }

  if (to.matched.some((record) => record.meta.requiresAnonymity) && userIsLoggedIn) {
    if (to.query.redirect) {
      return next(to.query.redirect);
    } else {
      return next({
        name: 'ProtocolDesignerPage',
      });
    }
  }

  return next();
});

export default router;
