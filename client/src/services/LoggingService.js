import BackendClient from "../data/source/remote/BackendClient";

export const LogLevel = Object.freeze({
  CRITICAL: 'CRITICAL',
  ERROR: 'ERROR',
  WARNING: 'WARNING',
  INFO: 'INFO',
  DEBUG: 'DEBUG',
  NOTSET: 'NOTSET',
});

class LoggingService {
  client = null;
  logLevel;

  constructor({ logLevel, loggerURL, serviceName }) {
    this.logLevel = logLevel || LogLevel.INFO;
    this.client = new BackendClient({ baseURL: loggerURL });
    this.service = serviceName;
  }

  /**
   * 
   * @param message string | null Log message
   * @param level string | null Log level
   * @param event object | null Event that triggered the log entry
   * @param extra object | string | null Additional information
   */
  _log(message = null, extra = null, level = null, event = null) {
    this.client.post('/events', {
      service: this.service,
      level: level || this.logLevel,
      created_at: Date.now(),
      message,
      event,
      extra,
    })
  }

  logMessage(message) {
    this._log(message);
  }

  /**
   * 
   * @param {{
   *   user: string;
   *   action: string;
   *   summary: string;
   *   errors?: string;
   *   sessionStatus?: string;
   *   sessionId?: string;
   * }} object 
   */
  logObject(object) {
    let logLevel = LogLevel.INFO;
    if (object.errors) {
      logLevel = LogLevel.ERROR;
    }
    this._log(object.summary, JSON.stringify(object, null, 2), logLevel);
  }
}

export default LoggingService;
