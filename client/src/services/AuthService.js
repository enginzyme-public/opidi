class Auth extends EventTarget {
  /**
   * Initializes the auth service.
   * @returns google's auth instance.
   */
  async init() {
    // nothing to be done here
  }


  login(jwtToken) {
    localStorage.setItem('jwtToken', jwtToken);

    this.dispatchEvent(new Event("loggedIn"));
  }

  getJwtToken() {
    return localStorage.getItem('jwtToken');
  }

  async getUser() {
    try {

      const jwtToken = this.getJwtToken();

      if (!jwtToken) {
        return null;
      }

      const base64Url = jwtToken.split('.')[1];
      const base64 = base64Url.replace('-', '+').replace('_', '/');
      const payload = JSON.parse(window.atob(base64));

      const user = {
        id: payload.sub,
        email: payload.email,
        name: payload.name,
        imageURL: payload.picture,
      };
      
      return user;
    } catch {
      return null;
    }
  }

  async getAuthToken() {
    return this.getJwtToken();
  }

  async getIdToken() {
    return this.getJwtToken();
  }

  async signOut() {
    localStorage.removeItem('jwtToken');

    this.dispatchEvent(new Event("loggedOut"));
  }
}

export default Auth;
