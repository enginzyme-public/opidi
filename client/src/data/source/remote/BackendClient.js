import axios from 'axios';
import { AuthAction } from '../../../types/store-modules/AuthTypes';
import { authService } from '../../../config/containers';
import store from "../../../store";

export default class BackendClient {
  constructor({ baseURL } = {}) {
    this.client = axios.create({ baseURL });

    this.client.interceptors.request.use(
      async (config) => {
        const newConfig = { headers: {}, ...config };

        const token = await store.dispatch(AuthAction.getUserAuthToken);

        if (token) {
          newConfig.headers['Bearer-Token'] = token;
        }

        return newConfig;
      },
    );
    this.client.interceptors.response.use(
      (response) => response,
      async (error) => {
        const token = await store.dispatch(AuthAction.getUserAuthToken);

        if (token && error.response && error.response.status === 401) {
          authService.signOut();
        } else {
          return Promise.reject(error);
        }
      },
    );
  }

  getBaseURL() {
    return this.client.defaults.baseURL;
  }

  getUri({ url, params }) {
    return this.client.getUri({ url, params });
  }

  post(uri, data, config) {
    return this.client.post(uri, data, config);
  }

  put(uri, data, config) {
    return this.client.put(uri, data, config);
  }

  patch(uri, data, config) {
    return this.client.patch(uri, data, config);
  }

  get(uri, config) {
    return this.client.get(uri, config);
  }

  delete(uri, config) {
    return this.client.delete(uri, config);
  }
}
