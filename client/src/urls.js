/**
 * Contains a URL that points to the protocol object API to be used to fetch protocol, labware, pipettes, etc.
 * @type {String}
 */
export const CRUD_API_URL =
  process.env.VUE_APP_CRUD_API_URL || `${window.location.origin}`;
/**
 * Contains a URL that points to the opentrons protocol generator service to be used to simulate and generate Python protocol files
 * from protocol objects as input
 * @type {String}
 */
export const DEVICE_API_URL =
  process.env.VUE_APP_DEVICE_API_URL || `${window.location.origin}`;

/**
 * Contains a URL that points to the event logger API to be used to log events
 * @type {String}
  */
export const EVENT_LOGGER_API_URL =
  process.env.VUE_APP_EVENT_LOGGER_API_URL || `${window.location.origin}`;