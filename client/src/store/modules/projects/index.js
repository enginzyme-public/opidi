import { backendClient } from "../../../config/containers";
import types from "../../../types";
import { CRUD_API_URL } from "../../../urls";

export default {
  /**
   * @returns {object} - The Vuex state
   * @namespace state
   */
  state: () => ({
    /**
     * A representation of all available projects in state
     * @type {array}
     * @memberof state
     */
    projects: [],
  }),
  /**
   * Mutations for the Vuex state
   * @type {object}
   * @namespace mutations
   */
  mutations: {
    /**
     * Saves project objects to the store
     * @param {Object} state - (Vuex Arg) The state within this local context
     * @param {Array.<Object>} projects - An array containing project objects to be saved to the store
     * @memberof mutations
     */
    [types.LOAD_PROJECT_OBJECTS](state, projects) {
      state.projects = [];
      state.projects.push(...projects);
    },
    /**
     * Removes all saved project objects from the store
     * @param {Object} state - (Vuex Arg) The state within this local context
     * @memberof mutations
     */
    [types.UNLOAD_PROJECT_OBJECTS](state) {
      state.projects = [];
    },
  },
  /**
   * Actions for the Vuex state
   * @type {object}
   * @namespace actions
   */
  actions: {
    /**
     * Commits a mutation to load project objects into the store
     * @param {Object} context - (Vuex Arg) Exposes the same set of methods/properties on the store instance
     * @memberof actions
     */
    async [types.LOAD_PROJECT_OBJECTS]({ commit }) {
      const response = await backendClient.get(`${CRUD_API_URL}/category/`);
      commit(types.LOAD_PROJECT_OBJECTS, response.data.categories);
    },
    /**
     * Removes all saved project objects from the store
     * @param {Object} context - (Vuex Arg) Exposes the same set of methods/properties on the store instance
     * @memberof actions
     */
    [types.UNLOAD_LABWARES]({ commit }) {
      return new Promise((resolve, reject) => {
        commit(types.LOAD_PROJECT_OBJECTS);
        resolve();
        reject(new Error("…"));
      });
    },
  },
  /**
   * Getters for the Vuex state
   * @type {object}
   * @namespace getters
   */
  getters: {
    /**
     * Fetches an array of the projects available to be linked within a protocol
     * @param {Object} state - (Vuex Arg) The state within the deck module
     * @returns {array} - An array of the projects available to be chosen from
     * @memberof getters
     */
    projects: (state) => state.projects,
  },
};
