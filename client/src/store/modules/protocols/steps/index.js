import arrayTransferStep from "./_arrayTransferStep";
import bioshakeStep from "./_bioshakeStep";
import loopStep from "./_loopStep";
import pauseStep from "./_pauseStep";
import restockTips from "./_restockTips";
import sequenceTransferStep from "./_sequenceTransferStep";
import simpleTransferStep from "./_simpleTransferStep";
import slackMessageStep from "./_slackMessageStep";
import timerStep from "./_timerStep";
import types from "../../../../types";
import { deleteDeep, duplicateDeep } from "../../../../utils";
import { nth } from '../../../../utils/number-utils';
import { escapeRegex } from "../../../../utils/stringManipulation";
import { findDeep, flatDeep, rootIdsMap } from "../../../../utils/nestedObjectActions";
import { ParameterizedUserInputAction } from "../../../../types/store-modules/ParameterizedUserInputType";

let stepNameToMTypeMap = {
  loop: types.CREATE_LOOP_STEP,
  pause: types.CREATE_PAUSE_STEP,
  restock_tips: types.CREATE_RESTOCK_TIPS_STEP,
  sequence_transfer: types.CREATE_SEQUENCE_TRANSFER_STEP,
  bioshake_3000t: types.CREATE_BIOSHAKE_STEP,
  simple_transfer: types.CREATE_SIMPLE_TRANSFER_STEP,
  slack_message: types.CREATE_SLACK_MESSAGE_STEP,
  timer: types.CREATE_TIMER_STEP,
  array_transfer: types.CREATE_ARRAY_TRANSFER_STEP,
};

export default {
  /**
   * @returns {object} - The Vuex state
   * @namespace state
   */
  state: () => ({
    /**
     * Stores data related to step-related views and forms
     * @memberof state
     * @property {String} stepsstepID - Stores the index of the chosen steps card in the UI
     * @property {String} stepsFormType - Stores the form type of the steps form in the UI
     */
    vars: {
      stepsstepID: -1,
      stepsFormType: "",
    },
    /**
     * Serves as a cache storage for steps data of the protocol being viewed/edited by the user
     * @memberof state
     * @property {array} steps - The steps attribute of the object in the protocol cache
     * @property {number} stepsCounter - A number used to assign unique IDs to newly created step objects
     */
    cache: {
      stepCounter: 0,
      steps: [],
    },
  }),
  /**
   * Mutations for the Vuex state
   * @type {object}
   * @namespace mutations
   */
  mutations: {
    /**
     * Resets the step variables storing data related to the step UI view/forms
     * @param {Object} state - (Vuex Arg) The state within the step module
     * @memberof mutations
     */
    [types.RESET_STEP_VARIABLES](state) {
      state.vars.stepsstepID = -1;
      state.vars.stepsFormType = "";
    },
    /**
     * Updates the step variables storing data related to the step UI view/forms
     * @param {Object} state - (Vuex Arg) The state within the step module
     * @param {Object} data - Contains data used to update the step variables
     * @memberof mutations
     */
    [types.UPDATE_STEP_VARIABLES](state, data) {
      state.vars = { ...state.vars, ...data };
    },
    /**
     * Updates the value of the step counter within the protocol cache object
     * @param {Object} state - (Vuex Arg) The state within the step module
     * @param {Number} data - A value to be assigned to the step counter within the protocol cache object
     * @memberof mutations
     */
    [types.UPDATE_CACHED_PROTOCOL_STEP_COUNTER](state, data) {
      state.cache.stepCounter = data;
    },
    /**
     * Updates the value of the steps attribute of the protocol object cache
     * @param {Object} state - (Vuex Arg) The state within the step module
     * @param {Array.<Object>} data - An array of step objects to be set as the value of the steps attribute of the protocol object cache
     * @memberof mutations
     */
    [types.UPDATE_CACHED_PROTOCOL_STEPS](state, data) {
      state.cache.steps = data;
    },
    /**
     * Resets the value of the steps and step counter attributes of the protocol object cache
     * @param {Object} state - (Vuex Arg) The state within the step module
     * @memberof mutations
     */
    [types.RESET_STEPS](state) {
      state.cache.steps = [];
      state.cache.stepCounter = 0;
    },
    /**
     * Deletes a step (selected by ID) from the protocol cache object
     * @param {Object} state - (Vuex Arg) The state within the step module
     * @param {Number} id - The unique ID of the step to be deleted from the protocol cache object
     * @memberof mutations
     */
    [types.DELETE_STEP](state, id) {
      state.cache.steps = deleteDeep(state.cache.steps, id);
    },
    ...arrayTransferStep.mutations,
    ...bioshakeStep.mutations,
    ...loopStep.mutations,
    ...pauseStep.mutations,
    ...restockTips.mutations,
    ...sequenceTransferStep.mutations,
    ...simpleTransferStep.mutations,
    ...slackMessageStep.mutations,
    ...timerStep.mutations,
  },
  /**
   * Actions for the Vuex state
   * @type {object}
   * @namespace actions
   */
  actions: {
    /**
     * Commits a mutation that creates a new step that falls under a specified category
     * @param {Object} context - (Vuex Arg) Exposes the same set of methods/properties on the store instance
     * @param {String} category - Specifies the type of step to be created (eg. Array transfer)
     * @memberof actions
     */
    [types.CREATE_STEP]({ commit, dispatch, getters }, category) {
      const categoriesCount = getters.flattedCachedProtocolSteps.reduce((count, step) => {
        return count + (step.type === category ? 1 : 0);
      }, 0);
      dispatch(stepNameToMTypeMap[category], { categoriesCount });
      commit(types.REGISTER_PROCOTOL_CACHE_CHANGE, null, { root: true });
    },
    /**
     * Commits a mutation that deletes a step (specified by ID) from the protocol cache object and deletes
     * all representations of its data dependencies from the dependencies value of the protocol cache object
     * @param {Object} context - (Vuex Arg) Exposes the same set of methods/properties on the store instance
     * @param {Number} id - The unique ID of the step to be deleted from the protocol cache object
     * @memberof actions
     */
    [types.DELETE_STEP]({ dispatch, commit, rootGetters }, id) {
      return new Promise((resolve, reject) => {
        commit(types.DELETE_STEP, id);
        commit(
          types.UPDATE_CACHED_PROTOCOL_DEPENDENCY_LINKS,
          rootGetters.cachedProtocolDependencies.filter(
            (obj) => !(obj.sink === "transfer" && obj.sink_hash === id)
          ),
          { root: true }
        );
        commit(types.RESET_STEP_VARIABLES);
        commit(types.REGISTER_PROCOTOL_CACHE_CHANGE, null, { root: true });
        dispatch(ParameterizedUserInputAction.removeUnusedParameterizedUserInput, { root: true })
        resolve();
        reject(new Error("…"));
      });
    },
    /**
     * Duplicates a step within the protocol cache object and creates corresponding duplicate representations
     * of the data dependencies it has with other attributes of the protocol cache object
     * @param {Object} context - (Vuex Arg) Exposes the same set of methods/properties on the store instance
     * @param {Number} id - The unique identifier of the step to be duplicated within the protocol cache object
     * @memberof mutations
     */
    [types.DUPLICATE_STEP]({ state, commit, dispatch }, id) {
      return new Promise((resolve, reject) => {
        const clonedStep = duplicateDeep(state.cache.steps, id);
        function registerStep(clonedStep) {
          for (let childStep of clonedStep.substeps) {
            registerStep(childStep);
          }

          if (clonedStep.type.indexOf("transfer") !== -1) {
            commit(
              types.DUPLICATE_CACHED_PROTOCOL_DEPENDENCY_LINK,
              {
                oldIdx: clonedStep.id,
                newIdx: state.cache.stepCounter,
                sink: "transfer",
              },
              { root: true }
            );
          }

          clonedStep.id = state.cache.stepCounter++;

          // Fetch original name, as it may be an already cloned step.
          let originalName = clonedStep.name;
          const match = /^(.*) \((?:(11th|12th|13th|\d*(?:1st|2nd|3rd|[04-9]th)) )?copy\)$/.exec(clonedStep.name);
          if (match) {
            [, originalName] = match;
          }

          // Count the number of steps that were cloned form the same original name.
          const expression = new RegExp(`^${escapeRegex(originalName)}( \\((?:(11th|12th|13th|\\d*(?:1st|2nd|3rd|[04-9]th)) )?copy\\))?$`);
          const count = state.cache.steps.reduce((count, step) => {
            const doesMatch = expression.test(step.name);
            return count + (doesMatch ? 1 : 0);
          }, 0);

          if (count === 0) {
            clonedStep.name = `${originalName} (copy)`;
          } else {
            clonedStep.name = `${originalName} (${nth(count)} copy)`;
          }
        }
        registerStep(clonedStep);
        state.cache.steps.push(clonedStep);
        commit(types.UPDATE_CACHED_PROTOCOL_STEPS, state.cache.steps);
        dispatch(ParameterizedUserInputAction.copyParameterizedUserFromClonedStep, {
          sourceStep: findDeep(state.cache.steps, id),
          targetStep: clonedStep,
        })
        commit(types.REGISTER_PROCOTOL_CACHE_CHANGE, null, { root: true });
        resolve();
        reject(new Error("…"));
      });
    },
    /**
     * Commits a mutation to update the value of the steps attribute of the protocol cache object
     * @param {Object} context - (Vuex Arg) Exposes the same set of methods/properties on the store instance
     * @param {Array.<Object>} data - A value to be assigned to the steps attribute of the protocol cache object
     * @memberof actions
     */
    [types.UPDATE_CACHED_PROTOCOL_STEPS]({ commit, dispatch }, data) {
      return new Promise((resolve, reject) => {
        commit(types.UPDATE_CACHED_PROTOCOL_STEPS, data);
        commit(types.REGISTER_PROCOTOL_CACHE_CHANGE, null, { root: true });
        dispatch(ParameterizedUserInputAction.removeUnusedParameterizedUserInput, { root: true })
        resolve();
        reject(new Error("…"));
      });
    },
    /**
     * Commits a mutation to update the step variables that contain steps form/view UI related data
     * @param {Object} context - (Vuex Arg) Exposes the same set of methods/properties on the store instance
     * @param {Object} data - Contains data to update the step variables containing steps form/view UI related data
     * @memberof actions
     */
    [types.UPDATE_STEP_VARIABLES]({ commit }, data) {
      return new Promise((resolve, reject) => {
        commit(types.UPDATE_STEP_VARIABLES, data);
        resolve();
        reject(new Error("…"));
      });
    },
    ...arrayTransferStep.actions,
    ...bioshakeStep.actions,
    ...loopStep.actions,
    ...pauseStep.actions,
    ...restockTips.actions,
    ...sequenceTransferStep.actions,
    ...simpleTransferStep.actions,
    ...slackMessageStep.actions,
    ...timerStep.actions,
  },
  /**
   * Getters for the Vuex state
   * @type {object}
   * @namespace getters
   */
  getters: {
    /**
     * Fetches a number that qualifies as a unique ID for a newly created step within the protocol cache object
     * @param {Object} state - (Vuex Arg) The state within the step module
     * @returns {number} - A value that can be assigned to newly created steps as a unique ID
     * @memberof getters
     */
    cachedProtocolStepCounter: (state) => state.cache.stepCounter,
    /**
     * Fetches the array of steps defined within the protocol cache object
     * @param {Object} state - (Vuex Arg) The state within the step module
     * @returns {array} - The array of steps defined within the protocol cache object
     * @memberof getters
     */
    cachedProtocolSteps: (state) => state.cache.steps,
    /**
     * The map of stepId - parents array.
     * The parents array contains all the steps from the first parent to the last root step.
     * @param {Object} state - (Vuex Arg) The state within the step module
     * @returns {Object} - The map of stepId - parents array.
     */
    stepsRootMap: (state) => rootIdsMap(state.cache.steps),
    /**
     * Fetches the flatted array of steps defined within the protocol cache object
     * @param {Object} state - (Vuex Arg) The state within the step module
     * @returns {array} - The array of steps defined within the protocol cache object
     * @memberof getters
     */
    flattedCachedProtocolSteps: (state) => flatDeep(state.cache.steps),
  },
};
