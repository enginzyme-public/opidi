// Please note: this module is not to be used as a Vuex store module
// Instead mutations and actions here are used to populate those of an actual Vuex store module

import types from "../../../../types";
import { findDeep } from "../../../../utils";
import { getSafeStepName } from '../../../../utils/step-utils';
import { getNewRestockTipsStep } from "../../../defaultData/stepsData";

export default {
  /**
   * Mutations for the Vuex state
   * @type {object}
   * @namespace mutations
   */
  mutations: {
    /**
     * Creates a pause step within the protocol cache object
     * @param {Object} state - (Vuex Arg) The state within the step module
     * @memberof mutations
     */
    [types.CREATE_RESTOCK_TIPS_STEP](state, { categoriesCount }) {
      state.cache.steps.push(getNewRestockTipsStep(state.cache.stepCounter++, categoriesCount));
    },
    /**
     * Updates the name attribute of a pause step (specified by ID) within the protocol cache object
     * @param {Object} state - (Vuex Arg) The state within the step module
     * @param {Object} data - Contains data required to update the name attribute of the pause step in question
     * @param {Number} data.id - The unique ID of the step to be updated
     * @param {String} data.newValue - The new value of the "name" attribute of the step
     * @memberof mutations
     */
    [types.UPDATE_RESTOCK_TIPS_NAME](state, data) {
      findDeep(state.cache.steps, data.id).name = data.newValue;
    },
    [types.UPDATE_RESTOCK_TIPS_TIPS_TO_REFILL](state, { id, pipetteKey, tipsToRefill }) {
      findDeep(state.cache.steps, id).tipsToRefill[pipetteKey] = tipsToRefill;
    },
    [types.CREATE_MISSING_RESTOCK_TIPS_STEPS](
      state,
      { stepName, flattedCachedProtocolSteps, cachedProtocolDeck, stepsRootMap },
    ) {
      const step = findDeep(state.cache.steps, stepName, 'name');
      if (!step) {
        return;
      }

      
      
      // Create new step
      const newRestockStep = getNewRestockTipsStep(state.cache.stepCounter++, 0);
      const safeName = getSafeStepName(`Refill tips for step '${stepName}'`, flattedCachedProtocolSteps);
      newRestockStep.name = safeName;
      const pipette = step.parameters.pipette; // this pipette needs to be refilled.
      newRestockStep.tipsToRefill[pipette] = [...cachedProtocolDeck[pipette].tipracks];

      // insert right before the step that failed.
      const parentStep = stepsRootMap[step.id][0];
      const siblingSteps = parentStep ? parentStep.substeps : state.cache.steps;
      const failedStepIndex = siblingSteps.findIndex(({ id }) => id === step.id);
      siblingSteps.splice(failedStepIndex, 0, newRestockStep);
    },
  },
  /**
   * Actions for the Vuex state
   * @type {object}
   * @namespace actions
   */
  actions: {
    /**
     * Commits a mutation that updates the name attribute of a pause step (specified by ID) within the protocol cache object
     * @param {Object} context - (Vuex Arg) Exposes the same set of methods/properties on the store instance
     * @param {Object} data - Contains data required to update the name attribute of the pause step in question
     * @param {Number} data.id - The unique ID of the step to be updated
     * @param {String} data.newValue - The new value of the "name" attribute of the step
     * @memberof actions
     */
    [types.UPDATE_RESTOCK_TIPS_NAME]({ commit }, data) {
      commit(types.UPDATE_RESTOCK_TIPS_NAME, data);
      commit(types.REGISTER_PROCOTOL_CACHE_CHANGE, null, { root: true });
    },
    [types.UPDATE_RESTOCK_TIPS_TIPS_TO_REFILL]({ commit }, data) {
      commit(types.UPDATE_RESTOCK_TIPS_TIPS_TO_REFILL, data);
    },
    /**
     * Commits a mutation that creates a pause step within the protocol cache object
     * @param {Object} context - (Vuex Arg) Exposes the same set of methods/properties on the store instance
     * @memberof actions
     */
    [types.CREATE_RESTOCK_TIPS_STEP]({ commit }, { categoriesCount }) {
      return commit(types.CREATE_RESTOCK_TIPS_STEP, { categoriesCount });
    },
    [types.CREATE_MISSING_RESTOCK_TIPS_STEPS]({ commit, getters }, { stepName }) {
      commit(types.CREATE_MISSING_RESTOCK_TIPS_STEPS, {
        stepName,
        flattedCachedProtocolSteps: getters.flattedCachedProtocolSteps,
        cachedProtocolDeck: getters.cachedProtocolDeck,
        stepsRootMap: getters.stepsRootMap,
      })
    },
  },
};
