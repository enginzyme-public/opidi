import Vue from 'vue';
import { ParameterizedUserInputAction, ParameterizedUserInputGetter, ParameterizedUserInputMutation } from '../../../types/store-modules/ParameterizedUserInputType';
import { findDeepValue } from '../../../utils/nestedObjectActions';

function getMapKey({ id, type, path }) {
  return `${type}-${id}-${path}`;
}

export default {
  /**
   * @returns {object} - The Vuex state
   * @namespace state
   */
  state: () => ({
    /**
     * @type {{ [`${stepId}-${pathFromStep}`: number]: { type: 'step' | 'sequence'; id: string; path: string; label: string; dataType: string;} }}
     */
    parameterizedUserInputMap: {},
  }),
  getters: {
    /**
     * @returns {(path: string) => { type: 'step' | 'absolute'; stepId?: string; pathFromStep?: string; absolutePath?: string; label: string; dataType: string;} | null}
     */
    [ParameterizedUserInputGetter.getParameterizedUserInput]({ parameterizedUserInputMap }) {
      return ({ id, type, path }) => {
        return parameterizedUserInputMap[getMapKey({ id, type, path })] || null
      };
    },
    [ParameterizedUserInputGetter.parameterizedUserInput]({ parameterizedUserInputMap }) {
      return Object.values(parameterizedUserInputMap);
    },
  },
  /**
   * Mutations for the Vuex state
   * @type {object}
   * @namespace mutations
   */
  mutations: {
    [ParameterizedUserInputMutation.setParameterizedUserInput](state, parameterizedUserInput) {
      state.parameterizedUserInputMap = {};
      parameterizedUserInput.forEach((parameterizedUserInput) => {
        Vue.set(
          state.parameterizedUserInputMap,
          getMapKey(parameterizedUserInput),
          parameterizedUserInput,
        );
      });
    },
    [ParameterizedUserInputMutation.resetParameterizedUserInput](state) {
      state.parameterizedUserInputMap = {};
    },
    [ParameterizedUserInputMutation.updateParameterizedUserInput](state, parameterizedUserInput) {
      Vue.set(
        state.parameterizedUserInputMap,
        getMapKey(parameterizedUserInput),
        parameterizedUserInput,
      );
    },
    [ParameterizedUserInputMutation.removeParameterizedUserInput](state, parameterizedUserInput) {
      Vue.delete(
        state.parameterizedUserInputMap,
        getMapKey(parameterizedUserInput),
      );
    },
    /**
     * Removes unused ParameterizedUserInput
     */
    [ParameterizedUserInputMutation.removeParameterizedUserInputsByKeys](state, keysToRemove) {
      keysToRemove.forEach((key) => Vue.delete(state.parameterizedUserInputMap, key));
    },
    /**
     * Copies a ParameterizedUserInput to a new id
     */
    [ParameterizedUserInputMutation.copyParameterizedUserToNewId](state, { sourceId, targetId }) {
      Object.entries(state.parameterizedUserInputMap).forEach(([, parameterizedUserInput]) => {
        if (parameterizedUserInput.id === `${sourceId}`) {
          Vue.set(
            state.parameterizedUserInputMap,
            getMapKey({ ...parameterizedUserInput, id: targetId }),
            { ...parameterizedUserInput, id: `${targetId}` },
          );
        }
      });
    },
  },
  /**
   * Actions for the Vuex state
   * @type {object}
   * @namespace actions
   */
  actions: {
    [ParameterizedUserInputAction.removeUnusedParameterizedUserInput]({ state, getters, commit }) {
      const keysToRemove = Object.entries(state.parameterizedUserInputMap).reduce((keysToRemove, [key, parameterizedUserInput]) => {
        if (!parameterizedUserInput.label) {
          // parameterizedUserInput with an empty label are not valid
          return [...keysToRemove, key];
        } else if (parameterizedUserInput.type === 'step') {
          const step = getters.flattedCachedProtocolSteps.find(({ id }) => `${id}` === `${parameterizedUserInput.id}`); // transform to string to avoid type mismatch
          if (!step) {
            return [...keysToRemove, key];
          }

          const pathExists = findDeepValue(step, parameterizedUserInput.path) !== undefined;
          if (!pathExists) {
            return [...keysToRemove, key];
          }
        } else if (parameterizedUserInput.type === 'sequence') {
          const sequence = getters.cachedProtocol.data.sequences.find(({ id }) => `${id}` === `${parameterizedUserInput.id}`); // transform to string to avoid type mismatch

          if (!sequence) {
            return [...keysToRemove, key];
          }
          const pathExists = findDeepValue(sequence, parameterizedUserInput.path) !== undefined;
          if (!pathExists) {
            return [...keysToRemove, key];
          }
        } else {
          // malformed parameterizedUserInput
          return [...keysToRemove, key];
        }

        return keysToRemove;
      }, []);
      
      commit(ParameterizedUserInputMutation.removeParameterizedUserInputsByKeys, keysToRemove);
    },
    /**
     * Copies the user inputs from the source step to the target step, including all it's sub steps.
     * We are assuming that the target step is a clone of the source step, so that it will have the same number of
     * sub steps.
     */
    [ParameterizedUserInputAction.copyParameterizedUserFromClonedStep]({ commit, dispatch }, { sourceStep, targetStep }) {
      commit(ParameterizedUserInputMutation.copyParameterizedUserToNewId, {
        sourceId: sourceStep.id,
        targetId: targetStep.id,
      });

      if (sourceStep.substeps) {
        sourceStep.substeps.forEach((step, index) => {
          // we are assuming that both steps are identical
          if (targetStep.substeps[index]) {
            dispatch(ParameterizedUserInputAction.copyParameterizedUserFromClonedStep, {
              sourceStep: step,
              targetStep: targetStep.substeps[index],
            });
          }
        });
      }
    },
  },
};
