import { authService } from "../../config/containers";
import types from "../../types";
import { AuthAction, AuthGetter, AuthMutation } from "../../types/store-modules/AuthTypes";

export default {
  /**
   * @returns {object} - The Vuex state
   * @namespace state
   */
  state: () => ({
    user: null,
  }),
  /**
   * Mutations for the Vuex state
   * @type {object}
   * @namespace mutations
   */
  mutations: {
    [AuthMutation.registerUser](state,  { user }) {
      state.user = user;
    },
    [AuthMutation.deregisterUser](state) {
      state.user = null;
    },
  },
  /**
   * Actions for the Vuex state
   * @type {object}
   * @namespace actions
   */
  actions: {
    async [AuthAction.initAuthentication]({ dispatch, commit }) {
      const user = await authService.getUser();
      if (user) {
        await dispatch(AuthAction.registerUser, { user });
      } else {
        commit(AuthMutation.deregisterUser);
      }
    },
    async [AuthAction.registerUser]({ commit }, { user }) {
      commit(AuthMutation.registerUser, { user });
    },
    async [AuthAction.signOut]({ dispatch, commit }) {
      // dispatch and redirection is done in AuthService
      await authService.signOut();
      commit(types.UNLOAD_PROTOCOLS);
      dispatch(types.UNLOAD_LABWARES);
      dispatch(types.UNLOAD_PROJECT_OBJECTS);
    },
    async [AuthAction.getUserAuthToken]() {
      return authService.getAuthToken();
    },
    async [AuthAction.getUserIDToken]() {
      return authService.getIdToken();
    },
  },
  /**
   * Getters for the Vuex state
   * @type {object}
   * @namespace getters
   */
  getters: {
    /**
     * Fetches an indicator of the presence of an authenticated user from the store
     * @param {Object} state - (Vuex Arg) The state within the authentication module
     * @returns {boolean} - An indicator of the presence of an authenticated user
     * @memberof getters
     */
     [AuthGetter.userIsLoggedIn]: (state) => !!state.user,

     [AuthGetter.currentUser]: (state) => state.user,
     /**
     * Fetches a URL pointing to the avatar of the authenticated user from the store
     * @param {Object} state - (Vuex Arg) The state within the authentication module
     * @returns {String} - A URL pointing to the avatar of the authenticated user
     * @memberof getters
     */
    [AuthGetter.userImageURL]: (state) => state.user?.imageURL,
    /**
     * Fetches the name of the authenticated user from the store
     * @param {Object} state - (Vuex Arg) The state within the authentication module
     * @returns {boolean} - The name of the authenticated user
     * @memberof getters
     */
    [AuthGetter.userName]: (state) => state.user?.name,
    /**
     * Fetches the email address of the authenticated user from the store
     * @param {Object} state - (Vuex Arg) The state within the authentication module
     * @returns {boolean} - The email address of the authenticated user
     * @memberof getters
     */
    [AuthGetter.userEmail]: (state) => state.user?.email,
  },
};
