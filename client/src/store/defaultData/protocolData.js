import { getDefaultDeckData } from "./deckData";
import { getDefaultStepsCount, getDefaultStepsData } from "./stepsData";
import {
  getDefaultSequencesCount,
  getDefaultSequencesData,
} from "./sequencesData";
import {
  getDefaultLiquidClassesCount,
  getDefaultLiquidClassesData,
} from "./liquidClassesData";

/**
 * A function that returns an object representation of a newly created protocol
 * @param {String} protocolName - The name of the protocol to be created
 * @param {String} protocolDescription - The description of the protocol to be created
 * @returns {object} - An object representation of a newly created protocol
 */
export function getDefaultProtocol(
  protocolName,
  protocolDescription,
  protocolTeam,
  protocolSubcategory,
) {
  return {
    name: protocolName,
    description: protocolDescription,
    team: protocolTeam,
    subcategory: protocolSubcategory,
    isVerified: false,
    isShared: false,
    data: {
      deck: getDefaultDeckData(),
      sequences: getDefaultSequencesData(),
      steps: getDefaultStepsData(),
      liquid_classes: getDefaultLiquidClassesData(),
      stepCounter: getDefaultStepsCount(),
      liquidClassCounter: getDefaultLiquidClassesCount(),
      sequenceCounter: getDefaultSequencesCount(),
    },
  };
}
