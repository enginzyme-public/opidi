import AuthService from '../services/AuthService';
import LoggingService from '../services/LoggingService';
import BackendClient from '../data/source/remote/BackendClient';
import MockableBackendClient from '../data/source/mock/MockableBackendClient';
import { EVENT_LOGGER_API_URL } from "../urls";

export const authService = new AuthService({
  clientId: process.env.VUE_APP_GOOGLE_AUTH_CLIENT_ID,
});

export const loggingService = new LoggingService({
  loggerURL: EVENT_LOGGER_API_URL,
  serviceName: process.env.VUE_APP_LOG_GROUP_NAME,
});

const Client = process.env.VUE_APP_MOCK_BACKEND ? MockableBackendClient : BackendClient;
export const backendClient = new Client();
