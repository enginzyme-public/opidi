export const ParameterizedUserInputGetter = Object.freeze({
  parameterizedUserInput: 'ParameterizedUserInputGetter.parameterizedUserInput',
  getParameterizedUserInput: 'ParameterizedUserInputGetter.getParameterizedUserInput',
});

export const ParameterizedUserInputMutation = Object.freeze({
  setParameterizedUserInput: 'ParameterizedUserInputMutation.setParameterizedUserInput', 
  resetParameterizedUserInput: 'ParameterizedUserInputMutation.resetParameterizedUserInput', 
  updateParameterizedUserInput: 'ParameterizedUserInputMutation.updateParameterizedUserInput',
  removeParameterizedUserInput: 'ParameterizedUserInputMutation.removeParameterizedUserInput',
  removeParameterizedUserInputsByKeys: 'ParameterizedUserInputMutation.removeParameterizedUserInputsByKeys',
  copyParameterizedUserToNewId: 'ParameterizedUserInputMutation.copyParameterizedUserToNewId',
});

export const ParameterizedUserInputAction = Object.freeze({
  removeUnusedParameterizedUserInput: 'ParameterizedUserInputMutation.removeUnusedParameterizedUserInput',
  copyParameterizedUserFromClonedStep: 'ParameterizedUserInputMutation.copyParameterizedUserFromClonedStep',
});
