export const AuthGetter = Object.freeze({
  userIsLoggedIn: 'AUTH_USER_IS_LOGGED_IN',
  currentUser: 'AUTH_CURRENT_USER',

  userImageURL: 'AUTH_USER_IMAGE_URL',
  userName: 'AUTH_USER_NAME',
  userEmail: 'AUTH_USER_EMAIL',
});

export const AuthMutation = Object.freeze({
  registerUser: 'AUTH_REGISTER_USER',
  deregisterUser: 'AUTH_DEREGISTER_USER',
});

export const AuthAction = Object.freeze({
  initAuthentication: 'AUTH_INIT_AUTHENTICATION',
  registerUser: 'AUTH_REGISTER_USER',
  signOut: 'AUTH_SIGN_OUT',
  getUserAuthToken: 'AUTH_GET_USER_AUTH_TOKEN',
  getUserIDToken: 'AUTH_GET_USER_ID_TOKEN',
});
