const CATEGORIES_TREE = Object.freeze({
  "root": [
    {
        "key": "support-modification",
        "label": "Support Modification",
        "children": [
          { "key": "support-modification/metal-deposition", "label": "Metal deposition" },
        ]
    },
    {
        "key": "sample-immobilization",
        "label": "Sample Immobilization",
        "children": [
            { "key": "sample-immobilization/immobilization", "label": "Immobilization" },
            { "key": "sample-immobilization/washes", "label": "Washes" },
            { "key": "sample-immobilization/x-linking", "label": "X-linking" },
            { "key": "sample-immobilization/leaching", "label": "Leaching" },
        ]
    },
    {
      "key": "assay",
      "label": "Assay",
      "children": [
          { "key": "assay/recyclability", "label": "Recyclability" },
          { "key": "assay/activity Assay", "label": "Activity Assay" },
      ]
    },
    {
      "key": "workstations",
      "label": "Workstations",
      "children": [
          { "key": "workstations/staging", "label": "Staging" },
          { "key": "workstations/washbot", "label": "Washbot" },
          { "key": "workstations/fatbot", "label": "Fatbot" },
          { "key": "workstations/samplebot", "label": "Samplebot" },
          { "key": "workstations/retired", "label": "Retired" },
      ]
    },
  ]
});

function getLeavesKeys(children) {
  return children.reduce((leaves, tree) => {
    if (!tree.children) {
      return [...leaves, tree.key];
    }

    return [
      ...leaves,
      ...getLeavesKeys(tree.children),
    ];
  }, []);
}

function getLeaves(children) {
  return children.reduce((leaves, tree) => {
    if (!tree.children) {
      return [...leaves, tree];
    }

    return [
      ...leaves,
      ...getLeaves(tree.children),
    ];
  }, []);
}

export default CATEGORIES_TREE;

export const CATEGORIES_LEAVES_KEYS = getLeavesKeys(CATEGORIES_TREE.root);

export const CATEGORIES_LEAVES_MAP = getLeaves(CATEGORIES_TREE.root).reduce((map, leave) => ({
  ...map,
  [leave.key]: leave.label,
}), {});
