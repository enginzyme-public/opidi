const SHARING_MODES = Object.freeze({
  me: 'me',
  shared: 'shared',
});

export default SHARING_MODES;
