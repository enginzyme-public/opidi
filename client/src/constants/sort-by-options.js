export const SORT_BY_KEY = Object.freeze({
  modifiedAt: 'modifiedAt',
  createdAt: 'createdAt',
});

export const SORT_BY_DIRECTION = Object.freeze({
  ASC: 'ASC',
  DESC: 'DESC',
});
