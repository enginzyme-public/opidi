const TEAMS = Object.freeze([
  {
    id: 'analytics',
    name: 'Analytics',
    shortName: 'ANL',
  },
  {
    id: 'biocat',
    name: 'Biocat',
    shortName: 'BIO',
  },
  {
    id: 'enzyme-engineering',
    name: 'Enzyme Engineering',
    shortName: 'EE',
  },
  {
    id: 'process-engineering',
    name: 'Process Engineering',
    shortName: 'PE',
  },
  {
    id: 'automation',
    name: 'Automation',
    shortName: 'AUTO',
  },
]);

export default TEAMS;

export const NO_TEAM = {
  id: 'no-team',
  name: 'No Team',
  shortName: 'NOT',
};

export const TEAMS_MAP = [...TEAMS, NO_TEAM].reduce((map, team) => ({
  ...map,
  [team.id]: team,
}), {});


