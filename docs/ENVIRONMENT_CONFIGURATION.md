# Environment Configuration

This guide will help you to understand the different environment variables that you can set in the `.env` file to configure the OPiDi application as well as specific configuration for each service.

## Table of Contents

- [Root Directory `.env` Variables](#root-directory-env-variables)
- [Frontend Service `.env` Variables](#frontend-service-env-variables)
- [Backend services `.env` Variables](#backend-services-env-variables)

## Root Directory `.env` Variables

> Template path: [`.env`](../.env.template)

In this file we will have an ammalgamated `.env` file that will contain all the variables that are needed to run the project. This file will be used to set the variables that are needed to run the project in a local environment.

Please check the explanation for each variable in the following sections.

## Frontend Service `.env` Variables
> Template path: [`.env`](../client/.env.template)

- `VUE_APP_CRUD_API_URL`: This variable is used to set the URL of the CRUD API. If you are expecting to run everything locally, you can set this variable to `http://localhost/opidi-crud`. If you are running the backend services in a different way, you can set this variable to the URL of the CRUD API. 
- `VUE_APP_DEVICE_API_URL`: Similar to the `VUE_APP_CRUD_API_URL`.
- `VUE_APP_EVENT_LOGGER_API_URL`: Similar to the `VUE_APP_CRUD_API_URL`.
- `VUE_APP_GOOGLE_AUTH_CLIENT_ID`: This variable is used to set the Google Auth Client ID. Be aware that this variable has to be setted with the same value as the `GOOGLE_CLIENT_ID` variable. The client ID can be found in the Google Cloud Console.
- `VUE_APP_LOG_GROUP_NAME`: This variable is used to set the name of the log group in logger-event.
- `VUE_APP_MOCK_BACKEND`: This variable is used to set the mock backend. This variable is usually not used, but if needed you can add this variable to the `.env` (or `.env.local`) file and set it to `true` to use the mock backend.

## Backend services `.env` Variables

> Template path CRUD API: [`./services/crud_api/.env.template`](../services/crud_api/.env.template)
>
> Template path DEVICE API: [`./services/device_api/.env.template`](../services/device_api/.env.template)
>
> Template path EVENT LOGGER: [`./services/event_logger/.env.template`](../services/event_logger/.env.template)

### Google SSO Auth related variables.

- `GOOGLE_CLIENT_ID`: Similar to the `VUE_APP_GOOGLE_AUTH_CLIENT_ID`.
- `SESSION_TIMEOUT`: This variable is used to set the session timeout in seconds. The default value is 3600 seconds (1 hour).

### AWS Credentials related variables.

- `AWS_ACCESS_KEY_ID`: This variable is used to set the AWS Access Key ID. This variable is used to authenticate with AWS services. ***Note:*** Since this is a legacy part of the code to use dynamodb local, it is recommended to use the default values for this variable. (Defaults to `fakeKey`).
- `AWS_SECRET_ACCESS_KEY`: This variable is used to set the AWS Access Secret Key. This variable is used to authenticate with AWS services. ***Note:*** Since this is a legacy part of the code to use dynamodb local, it is recommended to use the default values for this variable. (Defaults to `fakeSecret`).

### OPIDI related variables.

- `OPIDI_CRUD_BUCKET_HOST`: This variable is used to set the host of the S3 bucket. The default value is `http://minio:9000`. If you're running the S3 service in a different way, you can set this variable to the host of the S3 bucket. Now if you are running the S3 service from aws you can set this variable to an empty string to let AWS to define the S3 Bucket host.
- `OPIDI_CRUD_BUCKET_NAME`: This variable is used to set the name of the S3 bucket. If you left the variable empty, the code will default the name to `enginzyme-opidi-protocol-dev`.

- `OPIDI_PROTOCOL_CRUD_TABLE_HOST`: This variable is used to set the host of the DynamoDB table. The default value is `http://dynamodb-local:4566`. If you're running the DynamoDB service in a different way, you can set this variable to the host of the DynamoDB table. Now if you are running the DynamoDB service from aws you can set this variable to an empty string to let AWS to define the DynamoDB Table host.
- `OPIDI_TAG_CRUD_TABLE_HOST`: Similar to the `OPIDI_PROTOCOL_CRUD_TABLE_HOST`.

### OPIRUN related variables

Opirun will eventually need to connect to this services to run the protocols. The following variables are used to set the tokens that are used to authenticate the opirun instance with the services that are running in the backend.

Is important to mention that the token that you use here has to match the ones that you deploy after in the opirun instance. To do so, it is recommended to check the .env file provided in the vault.

- `OPIRUN_TOKENS`: This variable expects to have a JSON that contains the tokens that are used to authenticate the opirun instance with the services that are running in the backend. The default value is an empty JSON `{}`.
   > <h4>Note:</h4> If you're expecting to connect to the current robots, you HAVE to set this variable with the correct tokens. If you're not sure about the tokens, please contact the project maintainers. If you want to run the project without the robots, you can leave this variable as an empty JSON or alternatively run the `generate_robot_secrets.py` script to generate the tokens and fill the variable with the output.

### S3 custom credentials

Now this variables are not mandatory, but will be useful if you want for example to connect the S3 bucket to a different S3 service with specific credentials. If you're running locally you can ignore this variables.

- `S3_ACCESS_KEY_ID`: This variable is used to set the S3 Access Key ID. This variable is used to authenticate with the S3 service.
- `S3_SECRET_ACCESS_KEY`: This variable is used to set the S3 Access Secret Key. This variable is used to authenticate with the S3 service.