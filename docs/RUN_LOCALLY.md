# How to run OPiDi locally

## Table of contents

1. [Document organization](#document-organization-and-quick-reference)
2. [Prerequsites](#prerequsites)
   1. [Software/dependencies](#softwaredependencies)
   2. [One time config for all methods.](#one-time-config-for-all-methods)
3. [Quick start](#quick-start)
4. [Environmental Variables Explanation](#environmental-variables-explanation)
5. [Running the stack with docker](#running-the-stack-with-docker)
6. [Specific service](#specific-service)

## Document Organization and quick reference.

To run this project locally first you have to understand the type of workflow
that you want to use:

- If you just want a quick start. Please refer to [Quick start](#quick-start).

- If you only want to run the app to either test it locally or install it, you
can check this [Running the stack with docker](#running-the-stack-with-docker).

- If you're intended to be developing and contributing to the project please
refer to [this document](./DEVELOPMENT_WORKFLOW.md).

- If you intend to deploy this project, please refer to [this document](./DEPLOYMENT_WORKFLOW.md).

## Prerequsites

### Software/dependencies

With that said, let's go with the base requirements:

- Docker and Docker compose are highly recommended but can be avoided (Under your own risk). Versions needed:

   - **Docker Engine version ^23.0.0** (`docker engine --version`)
   - **Docker compose version  ^2.22.0** (`docker compose version`)

   > **Note 1**: If you want to run with older versions you may encounter many problems, usually you'll have to comment different parts of the `docker-compose.yaml` file in order to make it work. Google is your friend here.
   
   > **Note 2**: Docker installation will depend on your OS, please check [Get started with Docker](https://docs.docker.com/get-started/get-docker/) for more details. 

- Alternatively, if you want to run without Docker: **Python 3.9** and newer are neccessary to run the backend code, we recommend having at least Python 3.9 installed.

   > **Important Note:** [Opentrons Service](./services/opentrons_service/README.md) and subsequently [Device API](./services/device_api/README.md) are version ***LOCKED*** for Python 3.9, this is due to a restriction on the opentrons package.

- In addition, If you need to serve the frontend you'll need to have installed node.js in version 16 onwards. Recommended to have something like [NVM](https://github.com/nvm-sh/nvm/blob/master/README.md#installing-and-updating) to manage the versions correctly.


> ***IMPORTANT NOTE***: If your computer has `docker-compose` available you may need to update to a newer version. `docker-compose` and `docker compose` (with an space instead of a hyphen) are <ins><i>different</i></ins> commands with different behaviours. `docker-compose` is referred as V1 and it's currently being <ins><b>deprecated</b></ins>. [Ref doc](https://docs.docker.com/compose/migrate/)

### One time config for all methods.

This project contains many components that are tight together, so, in order to run
it you'll need to configure some environment variables. Be aware that some of these
variables are sensitive and ***should not*** share with anyone. Having said so,
let's explain a bit what are all those env template files:

- Root template [.env.template](./.env.template): This file contains the environment variables that are needed to run all services through Docker Compose. Reference of all the variables [here](#environmental-variables-explanation)
- Client template [client/.env.template](./client/.env.template): This file contains the environment variables that are needed to run the frontend service. If you're expecting to run the frontend in a separate way, you'll need to configure this file. Reference of all the variables [here](#environmental-variables-explanation)
- Backend templates [services/crud_api/.env.template](./services/crud_api/.env.template), [services/device_api/.env.template](./services/device_api/.env.template) and [services/event_logger/.env.template](./services/event_logger/.env.template): These files contain the environment variables that are needed to run the backend services. If you're expecting to run the backend in a separate way, you'll need to configure these files. Reference of all the variables [here](#environmental-variables-explanation)

Now the question is, what to use and when?. If that's your question, follow the next steps:

- I just need to test locally, no need to configure weird stuff: Don't worry, just use the root template with the variables `VUE_APP_GOOGLE_AUTH_CLIENT_ID` and `GOOGLE_CLIENT_ID` filled with the same value. All other variables can be removed from the file to let docker compose use the default values.
- Do you want to run/deploy the whole project? Use the root template.
- Do you want to run the frontend only? Use the client template.
- Do you want to run an specific backend service only? Use the specific template.

Now you know what to use, let's configure the environment variables:

1. Copy the template file and set the name to `.env` (If you're expecting to run the frontend only, copy the client template and set the name to `.env.local`)

2. Fill out the values in the `.env` file. 

   - If you want to run the whole project locally, you can avoid filling out the whole file.
   - If you're not sure about the values, please contact the project maintainers.

## Quick start

If you're looking for a quick start, you can run the project with Docker compose with a minimal config:

1. Copy `.env.template` to `.env` in the root directory.
2. In the `.env` file set both `VUE_APP_GOOGLE_AUTH_CLIENT_ID` and `GOOGLE_CLIENT_ID` to the same value
(you can obtain the value from the Google cloud console or directly from the Bitwarden vault). All
other variables can be removed from the file to let docker compose use the default values.
3. Run the command `docker compose up -d --build` in the root directory.
4. Access to the app through `http://localhost` in your browser.

## Environmental Variables Explanation

Please refer to the [Environment Configuration](./ENVIRONMENT_CONFIGURATION.md) document for a detailed explanation of the environment variables.

## Running the stack with docker

Now that you have the correct env file configured, you can run the project with Docker compose. It's as simple as running the following command:

```bash
docker compose up -d --build

# This command will build the images and run the containers in detached mode. Alternatively you can use `docker compose up` to run the containers in the foreground.

# Additionally if you have built all the images and you want to run the containers without building them again, you can use the following command:

# docker compose start

# Docker compose start will start the containers in the background.
```

This command the first time will take a while to download the images and build the containers. Once it's done, you can access the project in your browser at:
- [Frontend entrypoint](http://localhost)
- [CRUD API Docs](http://localhost/opidi-crud/docs)
- [Device API Docs](http://localhost/opidi-device/docs)
- [Event Logger API Docs](http://localhost/event-logger/docs)
- [Minio Dashboard](http://localhost:9001)
- [DynamoDB Local endpoint](http://localhost:4566) (Only accessible through AWS CLI or any GUI of your preference).
- [Treafik Dashboard](http://localhost:8080) (This dashboard it's just for debugging purposes, you can ignore it).


Now if you want to stop the project, you can run the following command:

```bash
docker compose down 

# This command will stop the containers and delete it. Alternatively you can use `docker compose stop` to stop the containers without deleting them.
```

If you want to see the logs of the containers, you can run the following command:

```bash
docker compose logs -f
```

If you want to see the logs of a specific container, you can run the following command:

```bash
docker compose logs -f <container_name> # Example docker compose logs -f crud-api
```


## Specific service

### Frontend

After filling out the `.env.local` file, you can run the following command to start the frontend service:

```bash
cd client
nvm use 16
npm ci
npm run serve-dev
```

The frontend service will be running in the port 8080 by default. You can access the frontend in your browser at [http://localhost:8080](http://localhost:8080). 

> ***NOTE:*** If you're running the frontend service in a different way, you can set the `VUE_APP_CRUD_API_URL`, `VUE_APP_DEVICE_API_URL` and `VUE_APP_EVENT_LOGGER_API_URL` variables to the URL of the backend services.

> ***NOTE 2:*** If you are mocking the backend, you can set the `VUE_APP_MOCK_BACKEND` variable to `true` to use the mock backend.

### Back-End Dev

Now if you want to run the system without all the services, you can run the services individually. Be aware that you'll still need to configure some environment to make it work. The services `minio` and `dynamodb-local` are needed to run the crud-api service. You can run the services with the following commands:

```bash
docker compose up -d minio dynamodb-local
```

This command will run minio and dynamodb-local in detached mode. Once the services are running, you can run the services individually.

All services are ment to be run using poetry. So in all cases you need to [install poetry](https://python-poetry.org/docs/#installation) and run the following commands:

```bash
cd services/<service_name>
poetry config virtualenvs.create true --local
# In the case of device_api and opentrons_service
# poetry env use python3.9
poetry install
```

Now in the case of `crud_api`, `device_api` and `event_logger` you can run the following command to start the service:

```bash
poetry run uvicorn <service_name>.main:app --reload --port <port>
```

Replacing `<service_name>` with the name of the service you want to run and `<port>` with the port you want to run the service. The services will be running in the port you specified. You can access the service in your browser at `http://localhost:PORT/SERVICE-NAME/docs`.