# Development Workflow

The deployment workflow of OPiDi is designed to be straightforward, with clear separation between 
development and production. This chapter will guide you through the development workflow of the OPiDi 
project.

Currently we have two instances:

- ***Local development:*** In this envinronment you can run the services individually and test the changes in a local environment.
- ***Production***: In this environment you can test the changes in a live environment. It is not recommended to apply changes in directly in this environment.

## Local development

Before you start developing, make sure you have the following tools installed on your machine:
- [Docker](https://docs.docker.com/get-docker/)
- [Node.js](https://nodejs.org/en/download/)
- [Python](https://www.python.org/downloads/)

The recommended versions for this apps are the following:

- **Docker Engine version ^23.0.0** (`docker engine --version`)
- **Docker Compose version ^2.22.0** (`docker compose version`)
- **Node.js version ^16.0.0** (Preferrably 18, but 16 is the minimum)
- **Python version ^3.9.0** (Preferrably 3.10, but 3.9 is the minimum)

> **Note:** If you need help installing Docker and Docker Compose, refer to the [Get started with Docker](https://docs.docker.com/get-started/get-docker/) guide.

### Step-by-Step Development

In this chapter we will guide you through the process of setting up the development environment for the OPiDi project.

For more details on how to set up the environmental variables please refer to the 
[RUN_LOCALLY.md](./RUN_LOCALLY.md) file.

Now that you have a valid environment and the project cloned, you can start developing. The project is organized into 4 primary directories:

- **client**: This directory houses the Vue.js frontend application.
- **services**: This directory consists of backend services implemented using Python and Fast API.
    - **CRUD API**: Web service exposing endpoints to store/retrieve client app objects.
    - **Device API**: Web service exposing endpoints to help with protocol transpiling and analysis.
    - **Event Logger**: Web service exposing endpoints to centralize the logs of the application.


### Frontend

After filling out the `.env.local` file, you can run the following command to start the frontend service:

```bash
cd client
nvm use 16
npm ci
npm run serve-dev
```

The frontend service will be running in the port 8080 by default. You can access the frontend in your browser at [http://localhost:8080](http://localhost:8080). 

> ***NOTE:*** If you're running the frontend service in a different way, you can set the `VUE_APP_CRUD_API_URL`, `VUE_APP_DEVICE_API_URL` and `VUE_APP_EVENT_LOGGER_API_URL` variables to the URL of the backend services.

> ***NOTE 2:*** If you are mocking the backend, you can set the `VUE_APP_MOCK_BACKEND` variable to `true` to use the mock backend.

### Backend services

Now if you want to run the system without all the services, you can run the services individually. Be aware that you'll still need to configure some environment to make it work. The services `minio` and `dynamodb-local` are needed to run the crud-api service. You can run the services with the following commands:

```bash
docker compose up -d minio dynamodb-local
```

This command will run minio and dynamodb-local in detached mode. Once the services are running, you can run the services individually.

All services are ment to be run using poetry. So in all cases you need to [install poetry](https://python-poetry.org/docs/#installation) and run the following commands:

```bash
cd services/<service_name>
poetry config virtualenvs.create true --local
# In the case of device_api and opentrons_service
# poetry env use python3.9
poetry install
```

Now in the case of `crud_api`, `device_api` and `event_logger` you can run the following command to start the service:

```bash
poetry run uvicorn <service_name>.main:app --reload --port <port>
```

Replacing `<service_name>` with the name of the service you want to run and `<port>` with the port you want to run the service. The services will be running in the port you specified. You can access the service in your browser at `http://localhost:PORT/SERVICE-NAME/docs`.

## Development Process

After you have the services running, you can start developing. The development workflow is as follows:

1. **Create a new branch**: Before starting any development, create a new branch from the `main` branch. The branch name should be descriptive of the feature or bug you are working on.

    ```bash
    git checkout -b feature/new-feature
    ```
2. **Make changes**: Make the necessary changes to the codebase. Remember to follow the project's coding standards and best practices.
3. **Run tests**: Before committing your changes, run the tests to ensure that your changes do not break 
any existing functionality. This will involve for example testing with Isabella if the changes are
expected to run in a end to end.
4. **Commit changes**: Once you are satisfied with your changes, commit them to your branch. Remember to write a clear and concise commit message that describes the changes you have made.

    ```bash
    git add .
    git commit -m "Add new feature"
    ```
5. **Push changes and create MR**: After commiting, push your changes to the remote repository and create a merge request (MR) to the `main` branch. Make sure to assign the MR to a reviewer and add a description of the changes you have made.


## Production

When you're satisfied that your service is working correctly, it's time to promote it to `production`.

Create a new tag for your latest commit on the `main` branch and push the tag to the repository.

Note that all tags should follow [Semantic Versioning](https://semver.org/) principles. This helps to keep track of changes and allows other services and systems to rely on certain versions of your service.

Here is an example of how to tag your commit:

```bash
git tag -a v1.0.0 -m "Release version 1.0.0"
git push origin v1.0.0
```

## Deploying to Production

To deploy the updated service to production, you can refer to the [Deployment Workflow](./DEPLOYMENT_WORKFLOW.md) documentation.

## Continuous Integration (Deprecated)
<details>
<summary>CI is currently deprecated</summary>
The project uses GitLab's built-in CI/CD pipelines, which are defined in the `.gitlab-ci.yml` file. When you push to any branch or tag, the pipeline will automatically start running. 

The pipeline includes steps such as building the Docker image for the service, running any defined tests, pushing the Docker image to the registry, and deploying the service to the appropriate environment.

Remember, each push to `main` deploys to the `development` environment, each push to `staging` deploys to the `staging` environment, and each new tag pushes to `production`.

Remember to always ensure your tests pass before pushing to avoid failed deployments. If the pipeline does fail, check the CI logs in GitLab to find out what went wrong.
</details>