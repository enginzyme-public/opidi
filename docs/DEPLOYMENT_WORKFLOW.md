# Deployment Workflow

This guide will help you deploy the OPiDi project on an on-premises server using Docker Compose. Follow the steps below carefully to ensure a successful deployment.

## Table of Contents

- [Prerequisites](#prerequisites)
- [Step-by-Step Deployment](#step-by-step-deployment)
- [Managing the Deployment](#managing-the-deployment)
- [Rolling updates](#rolling-updates)
- [Troubleshooting](#troubleshooting)

## Prerequisites

Before you begin, make sure you have the following installed on your server:

- **Docker Engine version ^23.0.0** (`docker engine --version`)
- **Docker Compose version ^2.22.0** (`docker compose version`)

> **Note:** If you need help installing Docker and Docker Compose, refer to the [Get started with Docker](https://docs.docker.com/get-started/get-docker/) guide.

## Step-by-Step Deployment

1. **Clone the Repository**

   First, clone the repository to your server:

   ```sh
   git clone https://gitlab.com/enginzyme/lims-companion/opidi
   cd opidi
   ```

2. **Copy Environment Template**

   Copy the `.env.template` file to `.env` in the root directory:

   ```sh
   cp .env.template .env
   ```

3. **Configure Environment Variables**

    > **Note:** A preconfigured .env file can be found in the bitwarden secret manager. If you don't
    > have access to the secret manager, please ask the project maintainers for assistance.
   
   Open the `.env` file and set the required environment variables. At a minimum, you need to set `VUE_APP_GOOGLE_AUTH_CLIENT_ID` and `GOOGLE_CLIENT_ID` to the same value. You can obtain these values from the Google Cloud Console or your Bitwarden vault.

   ```sh
   VUE_APP_GOOGLE_AUTH_CLIENT_ID=your-google-auth-client-id
   GOOGLE_CLIENT_ID=your-google-client-id
   ```

   For the rest of the variables you can refere to the Environment Variables section in the [README.md](../README.md) file.


4. **Run Docker Compose**

   Use the `docker-compose.prod.yml` file to deploy the project:

   ```sh
   docker compose -f docker-compose.prod.yml up -d --build
   ```

   This command will build the Docker images and start the containers in detached mode.

5. **Access the Application**

   Once the containers are up and running, you can access the application through your browser:

   - [Frontend entrypoint](http://your-server-ip)
   - [CRUD API Docs](http://your-server-ip/opidi-crud/docs)
   - [Device API Docs](http://your-server-ip/opidi-device/docs)
   - [Event Logger API Docs](http://your-server-ip/event-logger/docs)
   - [Minio Dashboard](http://your-server-ip:9001)
   - [DynamoDB Local endpoint](http://your-server-ip:4566) (Only accessible through AWS CLI or any GUI of your preference).
   - [Traefik Dashboard](http://your-server-ip:8080) (This dashboard is just for debugging purposes, you can ignore it).

## Managing the Deployment

### Stopping the Containers

To stop the containers without deleting them, run:

```sh
docker compose -f docker-compose.prod.yml stop
```

### Restarting the Containers

To restart the containers, run:

```sh
docker compose -f docker-compose.prod.yml start
```

### Viewing Logs

To view the logs of all containers, run:

```sh
docker compose -f docker-compose.prod.yml logs -f
```

To view the logs of a specific container, run:

```sh
docker compose -f docker-compose.prod.yml logs -f <container_name>
```

### Updating the Deployment

If you need to update the deployment (e.g., after pulling new changes from the repository), run:

```sh
docker compose -f docker-compose.prod.yml up -d --build
```

This command will rebuild the images and restart the containers with the updated code.

## Rolling updates:

In the case that this app has been deployed in the environment, and you want to roll an update.
There's an script provided in the root of the project called `update_app.sh` 
that will help you to do this.

### Usage

To use the script, you need to provide the version of the application you want to deploy as an argument. Here are the steps to follow:

```bash
./deploy.sh <version>
```

#### Example

```bash
./deploy.sh v1.2.3
```

### Options

- `-h` or `--help`: Displays usage instructions.
  
  ```bash
  ./deploy.sh -h
  ```

### But what does it do?

This script cover the following steps:

1.  **Check if the version exists as a valid Git tag.**

    The script uses `git tag` to list all the tags in the repository and then checks if the specified version exists in the list. If the version is not found, the script exits with an error message.
  
2. **Update the application version in the `.env` file.**

    The script uses `sed` to replace the `APP_VERSION` variable in the `.env` file with the specified version.

    ```bash
    sed -i "s/APP_VERSION=.*/APP_VERSION=$1/" .env
    ```

3. **Restart the Docker services using Docker Compose.**

    Finally, the script uses Docker Compose to restart the services based on the specified production configuration file.

    ```bash
    docker compose -f docker-compose.prod.yml down
    docker compose -f docker-compose.prod.yml build
    docker compose -f docker-compose.prod.yml up -d
    ```


## Troubleshooting

If you encounter any issues during the deployment, check the logs for error messages and consult the project maintainers for assistance.

List of common errors and their solutions:

- **Error:** `ERROR: Couldn't connect to Docker daemon at http+docker://localhost - is it running?`
  <details>
  <summary><b>Solution</b></summary> 

    Make sure the Docker daemon is running on your server. You can start the Docker daemon by running `sudo systemctl start docker`.

  </details>

- **Error:** `Validating /path/to/compose/docker-compose.yml: service service-name additional property develop is not allowed`
  <details>
  <summary><b>Solution</b></summary> 

    Make sure that your docker version is compatible as written in the prerequisites section. If you are using an older version of docker-compose, you may encounter this error.

  </details>

- **Error:** `docker: Error response from daemon: driver failed programming external connectivity on endpoint [...]: Bind for 0.0.0.0:PORT failed: port is already allocated.`
  <details>
  <summary><b>Solution</b></summary> 

    This means that the app is trying to bind to a port that is already in use. You can either stop the service that is using the port or change the port in the `docker-compose.prod.yml` file. This could involve changing the `OPIDI_PORT` environmental variable or just checking for the other ports.

    By default, opidi will run on port 4200 but also will expose the following ports:
    - 4200: Frontend
    - 8081: traefik dashboard (this can be commented, since is a debugging tool mainly)
    - 4566: DynamoDB Local.

  </details>

- **Error:** [dynamodb-local] `SQLiteQueue[shared-local-instance.db]: error running job queue`
  <details>
  <summary><b>Stacktrace</b></summary> 

    ```
    dynamodb-local  |
    SEVERE: [sqlite] SQLiteQueue[shared-local-instance.db]: error running job queue
            com.almworks.sqlite4java.SQLiteException: [14] unable to open database file
            at com.almworks.sqlite4java.SQLiteConnection.open0(SQLiteConnection.java:1480)
            at com.almworks.sqlite4java.SQLiteConnection.open(SQLiteConnection.java:282)
            at com.almworks.sqlite4java.SQLiteConnection.open(SQLiteConnection.java:293)
            at com.almworks.sqlite4java.SQLiteQueue.openConnection(SQLiteQueue.java:464)
            at com.almworks.sqlite4java.SQLiteQueue.queueFunction(SQLiteQueue.java:641)
            at com.almworks.sqlite4java.SQLiteQueue.runQueue(SQLiteQueue.java:623)
            at com.almworks.sqlite4java.SQLiteQueue.access$000(SQLiteQueue.java:77)
            at com.almworks.sqlite4java.SQLiteQueue$1.run(SQLiteQueue.java:205)
            at java.lang.Thread.run(Thread.java:748)
    ```

  </details>
  <details>
  <summary><b>Solution</b></summary> 

    This error usually appears when the docker volume is not mounted correctly or it does not have the correct permissions to write data.
    
    Make sure that:
    <ul>
    <li>
    <code>.data/</code> folder is created in the root directory of the project.
    </li>
    <li>
    <code>.data/</code> folder has the correct permissions to write data. (Usually, this will refer to the group permissions of the folder).

    <blockquote>
    You can change the permissions of the folder by running <code>sudo chmod -R 774 .data/</code>.
    </blockquote>
    
    </li>
    <li>The volume is mounted correctly in the <code>docker-compose.prod.yml</code> file.</li>
    </ul>

  </details>