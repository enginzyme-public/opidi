# OPiDi (Opentrons Protocol Designer)

## What is OPiDi?

OPiDi (pronounced Oh-Pee-Dee) is a web-based application designed to simplify the creation, use, and sharing of laboratory protocols for liquid handling robots, specifically the Opentrons OT-2. It offers a user-friendly "no-code" interface that allows scientists to design complex protocols without needing to write Python code directly.

![OPiDi Interface](client/public/assets/protocol_app_demo.png)

## Key Features

- **User-Friendly Interface**: Design protocols using a graphical interface, eliminating the need for coding knowledge.
- **Reproducible Protocols**: Create unambiguously specified, reproducible, and consistent protocols.
- **Flexible Configuration**: Easily set up robot decks with labware, pipettes, and tip racks.
- **Protocol Simulation**: Verify your protocol's correctness before running it on the actual robot.
- **Protocol Sharing**: Share verified protocols with other users within the application.
- **Multi-User Support**: Each user has their own workspace to create and manage protocols.
- **Error Logging**: Silent logging messages for usage analytics and error diagnostics.
- **Extensibility**: Designed to potentially support other robot platforms in the future.

## How It Works

1. **Sign In**: Users authenticate using their Google account.
2. **Create or Import**: Start a new protocol or import an existing one.
3. **Setup**: Configure the robot deck, labware, and pipettes.
4. **Design**: Specify robot operations step-by-step.
5. **Simulate**: Run a simulation to verify the protocol's correctness.
6. **Generate**: Create the robot-specific protocol file (Python for Opentrons OT-2).
7. **Verify**: Mark protocols as "verified" after successful test runs.
8. **Share**: Optionally share protocols with other users.

## Why OPiDi?

OPiDi bridges the gap between scientists and liquid handling robots. It empowers researchers to:

- Design complex protocols without programming knowledge
- Iterate quickly on experimental designs
- Ensure consistency and reproducibility in lab work
- Collaborate effectively by sharing verified protocols

Originally developed for internal use at EnginZyme, OPiDi has proven invaluable for automating lab work and maximizing the utility of affordable robots like the OT-2.

## Getting Started

For instructions on setting up OPiDi locally, please refer to the [RUN_LOCALLY](./docs/RUN_LOCALLY.md) guide.

## Documentation

- [Environment Configuration](./docs/ENVIRONMENT_CONFIGURATION.md)
- [Development Workflow](./docs/DEVELOPMENT_WORKFLOW.md)
- [Deployment Workflow](./docs/DEPLOYMENT_WORKFLOW.md)

## Architectural Overview

![opd_architecture.png](client/public/assets/opd_architecture.png)

The diagram above helps to illustrate the architecture of the project.

## Directory Structure

The project is organized into three primary directories:

- **client**: This directory houses the Vue.js frontend application.
- **infra**: This directory contains infrastructure configuration files written in Terraform.
- **services**: This directory consists of backend services implemented using Python and Fast API.

<details>
<summary><b>1. client</b></summary>

This directory contains the frontend application built using Vue.js. The structure is as follows:

- **public**: Contains static assets required for the frontend application.
- **src**: This directory houses the application's source files. It includes Vue components, router configurations, store files, and styling.
- **tests**: Contains unit test files for the frontend code.

</details>

<details>
<summary><b>2. services</b></summary>

The `services` directory consists of backend services, with each service residing in its own subdirectory (`crud_api` and `device_api`). Each service contains the source code, Dockerfile, and configuration files for the application and linter. 

The services are structured as follows:

- **crud_api**: Web service exposing endpoints to store/retrieve client app objects. It includes several API endpoints and business logic related to elements like health checks, protocols, labware and more. It also includes tests for the API and the models.
- **device_api**: Web service exposing endpoints to help with protocol transpiling and analysis.
- **opentrons_service**: Contains all Opentrons business logic required for transpiling OPiDi protocols into runnable scripts.
- **shared_data**: Contains shared robot equipment data used by the Opentrons service.
- **event_logger**: Web service exposing endpoints to centralize the logs of the application.

</details>

<details>
<summary><b> <del>3. infra</del> (DEPRECATED)</b></summary>

> ***Note:*** Currently the application is not using the `infra` directory for infrastructure setup. The infrastructure setup is now handled by the docker-compose file in the root directory.

The `infra` directory contains files and subdirectories related to infrastructure setup, configuration, and documentation. 

- **root files (.tf)**: The root files are Terraform configuration files used for creating and managing the project's infrastructure resources like Application Load Balancer (ALB), databases, Docker setups, Identity and Access Management (IAM) settings, and more. 
- **docs**: Contains a Python script (`main.py`) for generating project documentation. A requirements.txt file is included for Python dependencies. It also has a `doc_gen` folder for generating document schemas.
- **modules**: This directory contains submodules for various parts of the infrastructure like the Elastic Container Registry (ECR) and web applications. Each submodule has Terraform files (.tf) related to that specific module and scripts for operations like hashing and pushing.

</details>

## Getting Started

For instructions on setting up the application on your local machine for development and testing purposes, refer to the
[RUN_LOCALLY](./docs/RUN_LOCALLY.md) file in the docs folder.

## Contributing

We welcome contributions! Please read our [DEVELOPMENT_WORKFLOW](./docs/DEVELOPMENT_WORKFLOW.md) guide for details on our development process and how you can get involved.

## License

OPiDi is released under the MIT License. This permissive license allows you to freely use, modify, and distribute the software, subject to the condition of preserving the copyright and license notices.

See the [LICENSE](LICENSE) file for the full text of the license.

## Acknowledgements

OPiDi was originally developed at EnginZyme and is built upon the open-source work of Opentrons Labworks. We're grateful to the open-source community for making projects like this possible.
