protocol_instructions={
    "metadata": {
        "name": "test",
        "description": "An Initial attempt at a comprehensive OpenTrons Protocol that can be simulated or ran live to check new changes to the OpenTrons",
        "author": {
            "name": "Stefan Ionescu",
            "email": "stefan@enginzyme.com"
        },
        "is_verified": False,
        "is_shared": False,
        "project_id": None,
        "created": 1626862367051
    },
    "deck": {
        "left_pipette": {
            "pipette": {
                "name": "Single Channel 300 uL - Gen 1",
                "id": "p300_single",
                "is_multichannel": False
            },
            "tipracks": [
                7,
                10
            ]
        },
        "right_pipette": {
            "pipette": {
                "name": "8 Channel 300 uL - Gen 2",
                "id": "p300_multi_gen2",
                "is_multichannel": True
            },
            "tipracks": [
                8,
                9,
                11
            ]
        },
        "slots": {
            "1": {
                "name": "shakerlabware_1",
                "labware_name": "2.0ml Eppendorfs on Al-rack on 3000T",
                "labware_type": "shakerLabware",
                "labware_id": "2.0ml_eppendorfs_on_al_rack_on_3000t",
                "nickname": "",
                "display_name": "2.0ml Eppendorfs on Al-rack on 3000T [1]",
                "id": 1
            },
            "2": {},
            "3": {},
            "4": {
                "name": "wellplate_4",
                "labware_name": "VWR 96 well-plate 2ml",
                "labware_type": "wellPlate",
                "labware_id": "vwr_96_wellplate_2.0ml",
                "nickname": "",
                "display_name": "VWR 96 well-plate 2ml [4]",
                "id": 4
            },
            "5": {
                "name": "tuberack_5",
                "labware_name": "10 Tube Rack with 4x50 mL, 6x15 mL tubes",
                "labware_type": "tubeRack",
                "labware_id": "10_tuberack_4x50ml_6x15ml",
                "nickname": "",
                "display_name": "10 Tube Rack with 4x50 mL, 6x15 mL tubes [5]",
                "id": 5
            },
            "6": {
                "name": "reservoir_6",
                "labware_name": "Brand 1x slot 220 mL",
                "labware_type": "reservoir",
                "labware_id": "brand_1x_slot_220ml",
                "nickname": "",
                "display_name": "Brand 1x slot 220 mL [6]",
                "id": 6
            },
            "7": {
                "name": "tiprack_7",
                "labware_name": "Opentrons 96 Tip Rack 0.3 mL",
                "labware_type": "tipRack",
                "labware_id": "enginzyme_96_tiprack_300ul",
                "nickname": "",
                "display_name": "Opentrons 96 Tip Rack 0.3 mL [7]",
                "id": 7
            },
            "8": {
                "name": "tiprack_8",
                "labware_name": "Opentrons 96 Tip Rack 10 \u00b5L",
                "labware_type": "tipRack",
                "labware_id": "enginzyme_96_tiprack_10ul",
                "nickname": "",
                "display_name": "Opentrons 96 Tip Rack 10 \u00b5L [8]",
                "id": 8
            },
            "9": {
                "name": "tiprack_9",
                "labware_name": "Opentrons 96 Filter Tip Rack 20 \u00b5L",
                "labware_type": "tipRack",
                "labware_id": "enginzyme_96_tiprack_20ul",
                "nickname": "",
                "display_name": "Opentrons 96 Filter Tip Rack 20 \u00b5L [9]",
                "id": 9
            },
            "10": {
                "name": "tiprack_10",
                "labware_name": "Opentrons 96 Tip Rack 0.3 mL",
                "labware_type": "tipRack",
                "labware_id": "enginzyme_96_tiprack_300ul",
                "nickname": "",
                "display_name": "Opentrons 96 Tip Rack 0.3 mL [10]",
                "id": 10
            },
            "11": {
                "name": "tiprack_11",
                "labware_name": "Opentrons 96 Tip Rack 0.3 mL",
                "labware_type": "tipRack",
                "labware_id": "enginzyme_96_tiprack_300ul",
                "nickname": "",
                "display_name": "Opentrons 96 Tip Rack 0.3 mL [11]",
                "id": 11
            },
            "12": {}
        }
    },
    "steps": [
        {
            "type": "slack_message",
            "name": "Slack Message",
            "id": 3,
            "substeps": [],
            "channel": "opentrons05_hercules",
            "message": "Testing OpenTrons",
            "channel_object": {
                "channel": "opentrons05_hercules"
            }
        },
        {
            "type": "sequence_transfer",
            "name": "Sequence Transfer",
            "id": 2,
            "substeps": [],
            "parameters": {
                "pipette": "left_pipette",
                "pipette_obj": {
                    "name": "(Left) Single Channel 300 uL - Gen 1",
                    "category": "left_pipette",
                    "is_multichannel": False
                },
                "pipette_strategy": "",
                "volumes": [
                    54
                ],
                "volumes_string": "54",
                "source_sequence": "My Test Sequence Source",
                "sourceSequenceId": 1,
                "destination_sequence": " My Test Sequence Destination",
                "destinationSequenceId": 0,
                "liquidClass": "Default",
                "liquidClassId": 0,
                "tipsStrategy": "STANDARD",
                "offset": {
                    "source": 1,
                    "destination": 5,
                    "source_type": "Bottom",
                    "destination_type": "Bottom"
                }
            }
        },
        {
            "type": "pause",
            "name": "Pause User",
            "id": 1,
            "substeps": [],
            "auto_resume": False,
            "pause_time": 10
        },
        {
            "type": "loop",
            "name": "Loop Shake Transfer",
            "id": 4,
            "num_iterations": 4,
            "substeps": [
                {
                    "type": "bioshake_3000t",
                    "name": "Bioshake 3000T",
                    "id": 5,
                    "substeps": [],
                    "parameters": {
                        "speed": 1420,
                        "setpoint": 42,
                        "tempControl": False,
                        "cooldown": False,
                        "duration": 52,
                        "wait_for_stop": True,
                        "force_stop": False
                    }
                },
                {
                    "type": "loop",
                    "name": "Loop Inner Transfer",
                    "id": 7,
                    "num_iterations": 3,
                    "substeps": [
                        {
                            "type": "simple_transfer",
                            "name": "Simple Transfer Slow",
                            "id": 8,
                            "substeps": [],
                            "parameters": {
                                "pipette": "left_pipette",
                                "pipette_obj": {
                                    "name": "(Left) Single Channel 300 uL - Gen 1",
                                    "category": "left_pipette",
                                    "is_multichannel": False
                                },
                                "pipette_strategy": "",
                                "volumes": [
                                    250
                                ],
                                "volumes_string": "250",
                                "source": {
                                    "slot_number": 6,
                                    "slot_name": "reservoir_6",
                                    "wells": [
                                        "A1"
                                    ]
                                },
                                "destination": {
                                    "slot_number": 1,
                                    "slot_name": "shakerlabware_1",
                                    "wells": [
                                        "A1",
                                        "A6",
                                        "B3",
                                        "D5"
                                    ]
                                },
                                "liquidClass": "Liquid Class Slow",
                                "liquidClassId": 2,
                                "tipsStrategy": "ECO",
                                "offset": {
                                    "source": 1,
                                    "destination": 1,
                                    "source_type": "Bottom",
                                    "destination_type": "Bottom"
                                }
                            }
                        },
                        {
                            "type": "pause",
                            "name": "Pause Auto",
                            "id": 10,
                            "substeps": [],
                            "auto_resume": True,
                            "pause_time": 54
                        },
                        {
                            "type": "simple_transfer",
                            "name": "Simple Transfer Fast",
                            "id": 11,
                            "substeps": [],
                            "parameters": {
                                "pipette": "right_pipette",
                                "pipette_obj": {
                                    "name": "(Right) 8 Channel 300 uL - Gen 2",
                                    "category": "right_pipette",
                                    "is_multichannel": True
                                },
                                "pipette_strategy": "",
                                "volumes": [
                                    20
                                ],
                                "volumes_string": "20",
                                "source": {
                                    "slot_number": 4,
                                    "slot_name": "wellplate_4",
                                    "wells": [
                                        "A1",
                                        "A5",
                                        "A10"
                                    ]
                                },
                                "destination": {
                                    "slot_number": 6,
                                    "slot_name": "reservoir_6",
                                    "wells": [
                                        "A1"
                                    ]
                                },
                                "liquidClass": "Liquid Class Fast",
                                "liquidClassId": 1,
                                "tipsStrategy": "ECO",
                                "offset": {
                                    "source": 1,
                                    "destination": 1,
                                    "source_type": "Bottom",
                                    "destination_type": "Bottom"
                                }
                            }
                        }
                    ]
                }
            ]
        }
    ],
    "liquid_classes": {
        "Default": {
            "touch_tip": False,
            "blow_out": True,
            "carryover": True,
            "air_gap": 0,
            "blowout_location": "destination well",
            "mix_before": None,
            "mix_after": None,
            "aspirate_rate": 1,
            "dispense_rate": 1,
            "blow_out_rate": 1,
            "new_tip": "never"
        },
        "Liquid Class Fast": {
            "touch_tip": False,
            "blow_out": True,
            "blowout_location": "destination well",
            "carryover": True,
            "air_gap": 0,
            "mix_before": None,
            "mix_after": None,
            "aspirate_rate": 1.7,
            "dispense_rate": 1.6,
            "blow_out_rate": 2,
            "new_tip": "never"
        },
        "Liquid Class Slow": {
            "touch_tip": True,
            "blow_out": False,
            "blowout_location": "destination well",
            "carryover": True,
            "air_gap": 5,
            "mix_before": [
                1,
                0
            ],
            "mix_after": None,
            "aspirate_rate": 0.1,
            "dispense_rate": 0.2,
            "blow_out_rate": 0.5,
            "new_tip": "never"
        }
    },
    "sequences": {
        " My Test Sequence Destination": [
            {
                "slot_number": 4,
                "slot_name": "wellplate_4",
                "offset": {
                    "offset_type": "",
                    "value": 0
                },
                "wells": [
                    "B2",
                    "B9",
                    "E7",
                    "F1",
                    "F2",
                    "F3",
                    "F4",
                    "G1",
                    "G2",
                    "G3",
                    "G4",
                    "H1",
                    "H2",
                    "H3",
                    "H4"
                ],
                "id": 0
            }
        ],
        "My Test Sequence Source": [
            {
                "slot_number": 6,
                "slot_name": "reservoir_6",
                "offset": {
                    "offset_type": "",
                    "value": 0
                },
                "wells": [
                    "A1"
                ],
                "id": 0
            }
        ]
    },
    "stepCounter": 12,
    "sequenceCounter": 2,
    "liquidClassCounter": 3
}

import itertools
import re
import socket
import sys
import os
import time
import traceback
from dataclasses import dataclass, field
from typing import List, Union, Tuple, Type
from enum import Enum, auto
import uuid

import requests
import serial
from opentrons import protocol_api, types
from opentrons.protocol_api import ProtocolContext, InstrumentContext
from opentrons_shared_data.pipette import fuse_specs, model_config, name_config

SLACK_LOG_WEBHOOK = (
    "https://hooks.slack.com/services/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"
)
CLOUD_WATCH_EP = "https://6sx3gw0ux3.execute-api.eu-north-1.amazonaws.com/prod"
SLACK_MESSAGE_WEBHOOK = (
    "https://hooks.slack.com/services/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"
)

##General Settings for the protocol

## Flag set once and provided globally to functions that can only run code when not is_simulating
IS_SIMULATING = True
## Current version of this transpiler
TRANSPILER_VERSION = "2.3.0"

## PostgREST API Home
POSTGREST_HOME = "https://opidi-events-api.prod.lims.infra.enginzyme.com"


## Baud rate for Serial communication with Shaker
BAUDRATE = 9600
## Shaker Serial Port Settings
SERIALPORT = '/dev/serial/by-path/platform-3f980000.usb-usb-0:1.5:1.0-port0'
## Time of no response before serial com error is thrown
TIMEOUT = 1



def real_run_deco(f):
    """
    Functions tagged with this decorator will only run if the protocol is run on an OT...Not simulating
    """

    def wrapper(*args, **kwargs):
        global IS_SIMULATING
        if not IS_SIMULATING:
            return f(*args, **kwargs)
        else:
            return
    return wrapper


@dataclass
class Operator:
    equal: str = "eq"
    gte: str = "gte"
    lte: str = "lte"
    gt: str = "gt"
    lt: str = "lt"


@dataclass
class SerialPrimaryKey:
    run: str = "id"
    device: str = "id"
    event: str = "id"
    protocol: str = "id"
    system: str = "id"


class DeviceType(Enum):
    unknown = auto()
    p300_v1 = auto()
    p50_v1 = auto()
    p300_v2 = auto()
    p50_v2 = auto()
    m300_v2 = auto()
    m20_v2 = auto()
    bioshake_3000t = auto()


class EventType(Enum):
    ot_preflight_check = auto()
    ot_postflight_check = auto()
    ot_step = auto()
    error = auto()


class StepType(Enum):
    default = auto()
    simple_transfer = auto()
    sequence_transfer = auto()
    array_transfer = auto()
    shake = auto()
    pause = auto()
    loop = auto()


@dataclass
class Device:
    barcode: str = ""
    name: str = ""
    device_type: DeviceType = DeviceType.unknown


class TimeStamp:
    """
    Helper class for working with PSQL Timestamps
    """

    # DEFAULT_LOCATION = "Europe/Stockholm"
    DEFAULT_LOCATION = "UTC"

    def __init__(self, epoch_time: int = None) -> None:
        self._epoch_time = epoch_time or time.time()
        self._epoch_time = int(self._epoch_time)  # Drop milliseconds
        self._location = self.DEFAULT_LOCATION

    def __sub__(self, other):
        return self.epoch_time - other.epoch_time

    def __add__(self, other):
        return self.epoch_time + other.epoch_time

    @property
    def timestampz(self):
        # Example 2022-02-24 11:10:15 Australia/Sydney"
        time_struct = time.localtime(self._epoch_time)
        return f"{time.strftime('%Y-%m-%d %H:%M:%S', time_struct)} {self._location}"

    @property
    def epoch_time(self):
        return self._epoch_time

    @epoch_time.setter
    def epoch_time(self, time):
        self._epoch_time = int(time)  # Drop milliseconds

    @property
    def location(self):
        return self._location

    @location.setter
    def location(self, location):
        self._location = location  # Drop milliseconds


@dataclass
class EventData:
    message: str

    def __init__(self, message: str = "") -> None:
        self.message = message


@dataclass
class StepEvent(EventData):
    def __init__(self) -> None:
        EventData.__init__(self, "")

    device: Device = None
    step_id: int = None
    step_type: StepType = StepType.default
    step_metadata: dict = field(default_factory=dict)


@dataclass
class PipetteModelRegex:
    p300_v1 = "(?=.*p300)(?=.*single)(?=.*v1).*"
    p50_v1 = "(?=.*p50)(?=.*single)(?=.*v2).*"
    p300_v2 = "(?=.*p300)(?=.*single)(?=.*v2).*"
    p50_v2 = "(?=.*p50)(?=.*single)(?=.*v2).*"
    m300_v2 = "(?=.*p300)(?=.*multi)(?=.*v2).*"
    m20_v2 = "(?=.*p20)(?=.*multi)(?=.*v2).*"
    # None = "(?!x)x"



class OTName:
    """
    Class to retrieve and store OpenTrons Name for various logging and messaging actions
    """

    _OT_name: str

    def __init__(self) -> None:
        self._OT_name = self.extract_name()

    def extract_name(self) -> str:
        """
        Extract the name of the current Opentrons

        @return {string} The name of the Opentrons
        """
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            # this can be any non 0 IP address.
            s.connect(('10.255.255.255', 1))
            # Now that it is searching for a connection we can request the IP
            robot_ip_info = s.getsockname()[0]

            # This outputs a list with the [IP Address, port_number]
            endpoint = 'http://{robot_ip}:31950/server/name'
            r = requests.get(endpoint.format(robot_ip=robot_ip_info))
            return r.json()['name']
        except Exception:
            return "Unknown"

    @property
    def str_name(self):
        return self._OT_name


class SlackHandler:
    """
    Library of useful functions for communicating protocol status to Slack
    """

    _OT_name: str
    _protocol_title: str

    def __init__(self, protocol_title, ot_name: OTName):
        self._OT_name = ot_name.str_name
        self._protocol_title = protocol_title

    def send_slack_message(self, message, webhook):
        """
        Send a message to the specified slack channel

        @param message {string} The message to send
        @param webhook {string} Slack webhook for the slack channel where the message should be sent

        @return The request response if needed.
        """

        global IS_SIMULATING

        if not IS_SIMULATING:
            try:
                message = f"\"{self._protocol_title}\" on {self._OT_name} - {message}"
                data = {"text": message}
                requests.post(webhook, json=data)

            except Exception:
                return  # No connection, no protocol handler so no way to log error.

    def send_log_message(self, message):
        """
        Function containing code to handle specifically log messages. Currently just calls
        the send_slack_message() function with the SLACK_LOG_WEBHOOK. However future plans to
        integrate differing logging methods into this function.

        @param message The log message to send.
        """
        if "SLACK_LOG_WEBHOOK" in globals():
            self.send_slack_message(message, SLACK_LOG_WEBHOOK)


class CloudWatchHandler:
    """Library of useful functions for communicating with cloudwatch"""

    def post_cloud_watch(self, log_stream, payload):
        """Post to the opentrons-robot-logs log group on the Cloud Watch service

        @param log_stream {string} The name of the cloud watch log stream to post to.
        @param payload {dictionary} The payload structure from Quip, built in upload_protocol_run()
        @param endpoint {string} The endpoint for the AWS cloud watch service used.
        """
        if "CLOUD_WATCH_EP" not in globals():
            return
        try:
            if CLOUD_WATCH_EP is not None:
                header = {}
                header['log_group'] = 'opentrons-robot-logs'
                header['log_stream'] = log_stream
                header['payload'] = payload
                requests.post(CLOUD_WATCH_EP, json=header)
                # TODO: add error handling of the response.

        except Exception:
            return  # No connection, no protocol handler so no way to log error.


class CloudDBHandler:
    """Library of useful functions for communicating protocol status to the cloud based DB"""

    VALID_POSTGREST_RESPONSES = [200, 201, 204]

    class RequestType(Enum):
        Post = auto()
        Get = auto()
        Patch = auto()

    devices: List[Device]
    _protocol_context: ProtocolContext

    def __init__(
        self, protocol_instructions, protocol_context: ProtocolContext, OT_name: OTName
    ):
        self._protocol_instructions = protocol_instructions
        self._metadata = protocol_instructions.get("metadata", {})
        self._protocol_title = self._metadata.get("name", "unnamed")
        self._OT_name = OT_name.str_name
        self.devices = []
        self._run_id = 0
        self._protocol_context = protocol_context

    @property
    def _system_id(self) -> int:
        success, robot = self._db_lookup_rows(
            "system", "name", Operator.equal, self._OT_name
        )
        if success and robot:
            return robot[0].get("id", 0)
        else:
            return 0

    @property
    def _instructions_id(self) -> str:
        metadata_protocol_id = self._metadata.get("protocol_id", 0)
        try:
            uuid_instructions_id = uuid.UUID(
                str(metadata_protocol_id), version=4
            )  # Check given id is uuid
            return str(uuid_instructions_id)
        except ValueError:
            return str(uuid.uuid4())  # else return new uuid4

    @property
    def _protocol_author(self) -> str:
        """
        Queries protocol instruction metadata for the Author structure and extracts the name. Returns "no_author" if failure along the way

        @return String Author's name
        """
        author_dict = self._metadata.get("author", "no_author")
        return (
            author_dict.get("name", "no_author")
            if (isinstance(author_dict, dict))
            else "no_author"
        )

    @property
    def _protocol_id(self) -> Union[None, int]:
        """
        Checks if the instructions_id can be found in the instructions ID row column on the cloud database.
        If so, searches through each entry to check if any have the same instructions. If yes, return that protocol pkey "ID",
        else add a new entry.

        @return int primary key of the protocol (protocol_id) entry or None if something went wrong
        """
        success, protocols = self._db_lookup_rows(
            "protocol", "instructions_id", Operator.equal, self._instructions_id
        )
        if success and protocols:
            for protocol in protocols:
                if protocol.get('instructions') == self._protocol_instructions:
                    return protocol.get(getattr(SerialPrimaryKey, "protocol"))

        new_protocol = {
            "instructions_id": self._instructions_id,
            "name": self._protocol_title,
            "creator": self._protocol_author,
            "instructions": self._protocol_instructions,
        }
        return self._db_post_with_increment("protocol", new_protocol)

    @property
    def _device_ids(self) -> List[int]:
        device_ids = []
        for device in self.devices:
            device_ids.append(self.lookup_device(device))
        return device_ids

    def extract_name(self):
        """
        Extract the name of the current Opentrons

        @return {string} The name of the Opentrons
        """
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            # this can be any non 0 IP address.
            s.connect(('10.255.255.255', 1))
            # Now that it is searching for a connection we can request the IP
            robot_ip_info = s.getsockname()[0]

            # This outputs a list with the [IP Address, port_number]
            endpoint = 'http://{robot_ip}:31950/server/name'
            r = requests.get(endpoint.format(robot_ip=robot_ip_info))
            return r.json()['name']
        except Exception:
            return "Unknown"

    @real_run_deco
    def db_init_run(self, start_time: TimeStamp):
        new_run = {
            "system_id": self._system_id,
            "device_id": self._device_ids,
            "protocol_id": self._protocol_id,
            "start_time": start_time.timestampz,
            "stop_time": start_time.timestampz,
        }

        self._run_id = self._db_post_with_increment("run", new_run)

    @real_run_deco
    def db_end_run(self, stop_time: TimeStamp):
        if not self._run_id:
            return  # Run failed to initialise on the DB
        update = {"stop_time": stop_time.timestampz}
        self._db_update_entry('run', update, "id", Operator.equal, self._run_id)

    @real_run_deco
    def db_add_event(
        self,
        event_type: EventType,
        event_data: Type[EventData] = None,
        event_time: TimeStamp = TimeStamp(),
    ):
        event_request = {
            "time": event_time.timestampz,
            "run_id": self._run_id or 0,
            "event_type": event_type.name,
        }

        event_id = self._db_post_with_increment("event", event_request)
        if not event_id:
            return  # No event_id, no point to continue

        if (
            event_type == EventType.ot_step
            and event_data
            and event_data.step_type != StepType.default
        ):
            try:
                device_id = (
                    self.lookup_device(event_data.device) if event_data.device else 0
                )
                ot_step = {
                    "event_id": event_id,
                    "device_id": device_id or 0,
                    "step_id": event_data.step_id,
                    "step_type": event_data.step_type.name,
                    "step_metadata": event_data.step_metadata,
                }
                self._db_send_request(self.RequestType.Post, "ot_step_event", ot_step)
            except Exception:
                self.db_add_error("Failed to Add Step to DB")

        elif event_type == EventType.error and event_data:
            try:
                error_msg = {"event_id": event_id, "message": event_data.message}
                self._db_send_request(self.RequestType.Post, "error_event", error_msg)
            except Exception:
                return

    @real_run_deco
    def db_add_error(self, error_message: str):
        self.db_add_event(EventType.error, EventData(str(error_message)))

    @real_run_deco
    def lookup_device(self, device: Device) -> Union[int, None]:
        """
        Lookup a device in the Device table based on the barcode.
        If cant find one, then add a new entry. Either way return the device table ID(pkey)

        @return Union[int, None]  The device ID, None if something went wrong accessing DB
        """

        if device.device_type == DeviceType.unknown:
            return 0

        success, devices = self._db_lookup_rows(
            "device", "barcode", Operator.equal, device.barcode
        )

        if success and devices:
            # Should only be 1 match, so always take the first in the list.
            return devices[0].get('id', 0)
        else:
            device = {
                "barcode": device.barcode,
                "name": device.name,
                "device_type": device.device_type.name,
            }
            return self._db_post_with_increment("device", device)

    @real_run_deco
    def _db_lookup_rows(
        self,
        table: str,
        field: str,
        operator: Operator,
        compare_value: Union[int, str],
    ) -> Tuple[bool, Union[None, List]]:

        """
        Forms a parameter using the provided field, compare_value and operator and runs a GET request using db_get_table()

        @return None if the request was illformed or database connection failed, else a list of Rows from the provided table matching the search.
        """

        parameters = [dict(key=field, value=f"{operator}.{compare_value}")]
        success, response = self._db_send_request(
            self.RequestType.Get, table, parameters=parameters
        )
        return success, response.json()

    @real_run_deco
    def _db_update_entry(
        self,
        table: str,
        update: dict,
        field: str,
        operator: Operator,
        compare_value: Union[int, str],
    ) -> bool:
        """
        Update an entry in the database based on a parameter match.
        """
        parameters = [dict(key=field, value=f"{operator}.{compare_value}")]
        success, _ = self._db_send_request(
            self.RequestType.Patch, table, body=update, parameters=parameters
        )
        return success

    @real_run_deco
    def _db_post_with_increment(self, table: str, row: dict) -> Union[None, int]:
        """
        Same as db_post() however row must not contain primary key
        @return int primary key of the new entry or None if something went wrong
        """
        try:
            pkey = getattr(SerialPrimaryKey, table)
            if row.get(pkey):
                print("Error: Primary Key in Row when calling increment")
                return None  # Dont use Increment function when passsing primary key.

            header = {"Prefer": "return=headers-only"}
            success, response = self._db_send_request(
                self.RequestType.Post, table, body=row, headers=header
            )

            if not success:
                return None
            location = response.headers.get('location')
            if location:
                # trunk-ignore(flake8/W605)
                regex = re.search("eq\.(?P<ID>\d+)", location)
                if not regex:
                    return None
                return regex['ID']

        except AttributeError:
            self.db_add_error(f"Error: Cannot Find Primary Key for table: {table}")
            return None

    @real_run_deco
    def _db_send_request(
        self,
        request_type: RequestType,
        table: str,
        body: dict = {},
        headers: dict = {},
        parameters=[],
    ) -> Tuple[bool, Union[None, List]]:

        """
        Sends a request to the PostGREST API with a list of parameters, a header, and a json body

        @return (bool, Request) None if the request was illformed or database connection failed, else a list of Rows from the provided table.
        """

        request = f"{POSTGREST_HOME}/{str(table)}"
        if parameters:
            param = parameters[0]
            request += f"?{param['key']}={param['value']}"
            for param in parameters[1:]:
                request += f"&{param['key']}={param['value']}"

        # self._protocol_context.comment("Request: " +str(request))
        # for p in body:
        #     self._protocol_context.comment(str(p))
        #     self._protocol_context.comment(str(body[p]))

        try:
            if request_type == self.RequestType.Get:
                response = requests.get(request)
            elif request_type == self.RequestType.Post:
                response = requests.post(request, json=body, headers=headers)
            elif request_type == self.RequestType.Patch:
                response = requests.patch(request, json=body, headers=headers)
            else:
                return None

            # self._protocol_context.comment("Response: " +str(response.status_code))
            # self._protocol_context.comment("Reason: " + str(response.reason))
            if response.status_code in self.VALID_POSTGREST_RESPONSES:
                return True, response
            else:
                if table == "event" or table == "error_event":
                    return False, response  # Avoid cyclic calls
                self.db_add_error(
                    f"Error: Request failed: '{response.reason}' for call towards: '{table}' -- {response.content}"
                )
                # print(f"Error: Request failed: '{response.reason}' for call towards: '{table}' -- {response.content}")
                return False, response

        except Exception:
            print("Error: Failed to send Request")
            return False, response
            # TODO Request Exception Handling - DO NOT USE db_add_error()... probably cyclic callback


class CommunicationsWrapper:
    """
    Class for cleanly initialising and passing around communications based classes
    Support for :
        - SlackHandler
        - CloudWatchHandler
        - CloudDBHandler
        - OTName
    """

    _cloud_watch_handler: CloudWatchHandler
    _slack_handler: SlackHandler
    _cloud_db_handler: CloudDBHandler
    _OT_name: OTName

    def __init__(self, protocol_instructions, protocol_context) -> None:
        self._cloud_watch_handler = CloudWatchHandler()
        protocol_title = protocol_instructions.get("metadata", {}).get(
            "name", "unnamed"
        )
        self._OT_name = OTName()
        self._slack_handler = SlackHandler(protocol_title, self._OT_name)
        self._cloud_db_handler = CloudDBHandler(
            protocol_instructions, protocol_context, self._OT_name
        )

    @property
    def cloud_watch_handler(self):
        return self._cloud_watch_handler

    @property
    def slack_handler(self):
        return self._slack_handler

    @property
    def cloud_db_handler(self):
        return self._cloud_db_handler

    @property
    def OT_str_name(self):
        return self._OT_name.str_name

class TipsWatchdog:
    """
    Used to manage Opentrons tips. Contains functions to pick up, discard, etc.
    """

    def __init__(self, pipette, tip_strategy):
        """
        Initialise the pipettes and check the tip strategy. Since this class is reinitialised
        at the start of every pipette step, it should pick up a tip if it does not have one.
        If mode is ECO - new tip, drop the current tip and take a new one.

        Tip Strategies:
        - ECO - Take a tip if needed and reuse it without ever dropping it
        - ECO - NEW TIP - Take a new tip only at the start of each new pipette step
        - STRICT - New tip for every transfer
        - STANDARD - New tip per source

        @param pipette The pipette object to manage.
        @param tip_strategy {string} The Strategy to use for managing tips
        """
        ## The active pipette
        self.pipette = pipette
        ## The Tip Strategy used for this step
        self.strategy = tip_strategy
        ## The source labware for this step
        self.source_check = None

        if self.strategy == "ECO":
            if not self.pipette.hw_pipette['has_tip']:
                self.pipette.pick_up_tip()

        if self.strategy == "ECO - NEW TIP":
            if self.pipette.hw_pipette['has_tip']:
                self.pipette.drop_tip()
            self.pipette.pick_up_tip()

    def change_tip(self, source=None):
        """
        Run before every action, check which tip strategy and if not Eco, change the tip.

        @param source From which labware the current action should source liquid
        """
        if self.strategy == "STRICT":
            if self.pipette.hw_pipette['has_tip']:
                self.pipette.drop_tip()
            self.pipette.pick_up_tip()

        if self.strategy == "STANDARD":
            if source != self.source_check:
                if self.pipette.hw_pipette['has_tip']:
                    self.pipette.drop_tip()
                self.pipette.pick_up_tip()
                self.source_check = source

    def discard(self):
        """
        Run at the end of each step, checks which tip strategy is being used.
        If not an Eco mode, will drop the current tip.
        """
        if self.strategy != "ECO" and self.strategy != "ECO - NEW TIP":
            if self.pipette.hw_pipette['has_tip']:
                self.pipette.drop_tip()


class RobotHandler:
    """
    Class used for basic opentrons commands.
    """

    _devices: List[Device]
    _protocol_context: ProtocolContext
    _slack_handler: SlackHandler
    _cloud_db_handler: CloudDBHandler

    def __init__(
        self,
        protocol_context: ProtocolContext,
        protocol_instructions,
        communications_wrapper: CommunicationsWrapper,
    ):
        """
        Initialise the class members, extract the slack log webhook, and assign the labware,
        trash bin, and pipettes to their relative positions

        @param protocol The instance of the protocol API context
        @param protocol_instructions The robot instructions extracted from the input json file
        """
        ##The active protocol context
        self._protocol_context = protocol_context
        ## Robot instructions from JSON
        self._protocol_instructions = protocol_instructions
        self.assign_slots()
        self.assign_trash()
        self.assign_pipettes()
        self._slack_handler = communications_wrapper.slack_handler
        self._cloud_db_handler = communications_wrapper.cloud_db_handler
        self._devices = []
        self._get_devices()

    @property
    def devices(self):
        return self._devices

    def assign_slots(self):
        """
        Iterate over all deck slots in the instructions file and load them into the protocol context
        """
        for slot in self._protocol_instructions['deck']['slots']:
            slotContent = self._protocol_instructions['deck']['slots'][slot]
            if slotContent:
                element = self._protocol_context.load_labware(
                    slotContent['labware_id'], slot
                )
                setattr(
                    self, slotContent['name'], element
                )  ## User Interface has to make sure that names don't contain space and don't end with a number

    def assign_trash(self):
        """
        Assign the trash in the protocol
        """
        setattr(self, 'trash', self._protocol_context.fixed_trash)

    def assign_pipettes(self):
        """
        Iterate over the pipettes in the instructions file and load them
        as instruments into the protocol context. Also load their respective tip racks
        """
        if self._protocol_instructions['deck']['left_pipette']['pipette']:
            tipracks_slots = self._protocol_instructions['deck']['left_pipette'][
                'tipracks'
            ]
            tipracks_names = [
                self._protocol_instructions['deck']['slots'][str(key)]['name']
                for key in tipracks_slots
            ]
            tipracks = [getattr(self, x) for x in tipracks_names]

            ## The left pipette of the Opentrons
            self.left_pipette = self._protocol_context.load_instrument(
                self._protocol_instructions['deck']['left_pipette']['pipette']['id'],
                'left',
                tip_racks=tipracks,
            )

        if self._protocol_instructions['deck']['right_pipette']['pipette']:
            tipracks_slots = self._protocol_instructions['deck']['right_pipette'][
                'tipracks'
            ]
            tipracks_names = [
                self._protocol_instructions['deck']['slots'][str(key)]['name']
                for key in tipracks_slots
            ]
            tipracks = [getattr(self, x) for x in tipracks_names]
            ## The right pipette of the Opentrons
            self.right_pipette = self._protocol_context.load_instrument(
                self._protocol_instructions['deck']['right_pipette']['pipette']['id'],
                'right',
                tip_racks=tipracks,
            )

    def get_wells(self, sources):
        """
        Get the Well locations for each source labware.

        @param sources {list} A list of the labware used as sources for the current step
        @return locations {list} A list of well locations for the given labware sources
        """
        if not isinstance(sources, list):
            sources = [sources]

        locations = []
        for slot in sources:
            element = getattr(self, slot['slot_name'])
            locations += [element.wells_by_name()[x] for x in slot['wells']]

        return locations

    def apply_offsets(self, wells, offset_type, offset_value):
        """
        Allows for the option to specify an offset for the aspirate/dispense function.

        The offset type specifies where on the well the offset should start from.
        Offset types are currently Bottom and Top.

        @param wells {list} A list of well locations for the given labware sources for which to apply the offset
        @param offset_type {string} The location on the well were the offset should originate from
        @param offset_value {float} The offset value to apply to the wells

        @return The list of wells updated with offsets
        """
        if not isinstance(wells, list):
            wells = [wells]

        if offset_type == "Bottom":
            wells = [well.bottom(float(offset_value)) for well in wells]
        else:
            wells = [well.top(float(offset_value)) for well in wells]

        return wells

    def update_flow_rate(self, pipette: InstrumentContext, liquid_class: dict):
        """
        Used to first calculate the flow rate based on the ratio provided in the instructions file for aspirate, dispense,
        and blow_out. The absolute values of max, min, and default rate differ depending on the type of pipette used.
        Resource https://github.com/Opentrons/opentrons/blob/edge/shared-data/pipette/definitions/pipetteNameSpecs.json

        Once the absolute rate has been calculated, it is set in the pipette object ready for the transfer step.
        Note that the rates are stored/defined in the liquid class associated with the step.

        @param pipette The pipette for which to update the flow rate
        @param liquid_class Contains information on the different ratios for aspirate, dispense, blow_out rates.
        """

        def calc_rate(rate, pipette_action_type):
            if pipette_action_type == 'aspirate':
                pipette_flow_rates = name_config()[pipette.name].get(
                    'defaultAspirateFlowRate'
                )
                if pipette_flow_rates:
                    default_rate = pipette_flow_rates['value']
                    min_rate = pipette_flow_rates['min']
                    max_rate = pipette_flow_rates['max']
                else:
                    self._slack_handler.send_log_message(
                        "Error: Can not fetch aspirate rate from file."
                    )

            elif pipette_action_type == 'dispense':
                pipette_flow_rates = name_config()[pipette.name].get(
                    'defaultDispenseFlowRate'
                )
                if pipette_flow_rates:
                    default_rate = pipette_flow_rates['value']
                    min_rate = pipette_flow_rates['min']
                    max_rate = pipette_flow_rates['max']
                else:
                    self._slack_handler.send_log_message(
                        "Error: Can not fetch dispense rate from file."
                    )

            elif pipette_action_type == 'blow_out':
                pipette_flow_rates = name_config()[pipette.name].get(
                    'defaultBlowOutFlowRate'
                )
                if pipette_flow_rates:
                    default_rate = pipette_flow_rates['value']
                    min_rate = pipette_flow_rates['min']
                    max_rate = pipette_flow_rates['max']
                else:
                    self._slack_handler.send_log_message(
                        "Error: Can not fetch blow_out rate from file."
                    )

            if rate >= 0 and rate < 1:
                return (default_rate - min_rate) * rate
            elif rate == 1:
                return default_rate
            elif rate > 1 and rate <= 2:
                return ((max_rate - default_rate) * (rate - 1)) + default_rate
            else:
                self._slack_handler.send_log_message(
                    "Error: Rate Error, please enter proper rate"
                )
                return None

        # END calc_rate

        # get information about the liquid_class from the protocol design
        liquid = [
            obj
            for obj in self._protocol_instructions['liquid_classes'].items()
            if obj[0] == liquid_class
        ][0][1]
        aspirate_rate = liquid.get('aspirate_rate')
        if aspirate_rate:
            # print(aspirate_rate)
            pipette.flow_rate.aspirate = calc_rate(aspirate_rate, 'aspirate')

        dispense_rate = liquid.get('dispense_rate')
        if dispense_rate:
            # print(dispense_rate)
            pipette.flow_rate.dispense = calc_rate(dispense_rate, 'dispense')

        blow_out_rate = liquid.get('blow_out_rate')
        if blow_out_rate:
            # print(blow_out_rate)
            pipette.flow_rate.blow_out = calc_rate(blow_out_rate, 'blow_out')

    def simple_pipetting(self, parameters):
        """
        Stores the logic for a simple pipette step. Will prepare the step based on sources,
        offsets, destinations etc... Then run through each action transferring liquid.
        @param parameters {dictionary} The parameters for the step
        """
        pipette = getattr(self, parameters['pipette'])
        tips = TipsWatchdog(pipette, parameters['tipsStrategy'])

        volumes = parameters['volumes']

        if "source" in parameters:
            sources = self.get_wells(parameters['source'])
            destinations = self.get_wells(parameters['destination'])

        if "source_sequence" in parameters:
            srcs = self._protocol_instructions['sequences'][
                parameters['source_sequence']
            ]
            sources = self.get_wells(srcs)
            dsts = self._protocol_instructions['sequences'][
                parameters['destination_sequence']
            ]
            destinations = self.get_wells(dsts)

        if "offset" in parameters:
            sources = self.apply_offsets(
                sources,
                parameters['offset']['source_type'],
                parameters['offset']['source'],
            )
            destinations = self.apply_offsets(
                destinations,
                parameters['offset']['destination_type'],
                parameters['offset']['destination'],
            )

        self.update_flow_rate(pipette, parameters['liquidClass'])

        matched_iterator = match_sequences(
            volumes, sources, destinations
        )  ## User Interface has to make sure that names don't contain space and don't end with a number

        for vol, src, dst in zip(*matched_iterator):
            tips.change_tip(source=src)
            pipette.transfer(
                vol,
                src,
                dst,
                **self._protocol_instructions["liquid_classes"][
                    parameters['liquidClass']
                ],
            )

        tips.discard()

    def array_pipetting(self, parameters):
        """
        Stores the logic for an array pipette step. Similar to simple_pipetting() except using an array map as input.
        @param parameters {dictionary} The parameters for the step
        """
        pipette = getattr(self, parameters['pipette'])
        tips = TipsWatchdog(pipette, parameters['tipsStrategy'])

        for line in parameters['array_map']:

            sourceSlot, sourceWell, destSlot, destWell, volume = line

            src = self.get_wells({'slot_name': sourceSlot, 'wells': [sourceWell]})
            dst = self.get_wells({'slot_name': destSlot, 'wells': [destWell]})

            if "offset" in parameters:
                src = self.apply_offsets(
                    src,
                    parameters['offset']['source_type'],
                    parameters['offset']['source'],
                )
                dst = self.apply_offsets(
                    dst,
                    parameters['offset']['destination_type'],
                    parameters['offset']['destination'],
                )

            self.update_flow_rate(pipette, parameters['liquidClass'])
            tips.change_tip(source=src)
            pipette.transfer(
                int(volume),
                src,
                dst,
                **self._protocol_instructions["liquid_classes"][
                    parameters['liquidClass']
                ],
            )

        tips.discard()

    def shake(self, parameters):
        """
        Stores the logic for a simple shaker step utilising the BioShake 3000t.
        Only runs during the actual protocol since a simulation would not have access to the serial ports.
        Sets the temperature and RPM setpoint and starts the system. If users set "wait_for_stop" as an
        option, the code will block until the shaker step has completed (indicated by the shaker "homing").
        Either way will stop and cool-down the shaker post completion.

        @param parameters The parameters for the step

        """

        self.custom_home(only_waste=True)

        if not self._protocol_context.is_simulating():
            with serial.Serial(SERIALPORT, BAUDRATE, timeout=TIMEOUT) as ser:
                shaker = Shaker(ser)

                if parameters['tempControl']:
                    shaker.start_temp_control(int(parameters['setpoint']))

                if parameters['speed'] and parameters['duration']:
                    shaker.start_run(parameters['speed'], parameters['duration'])

                if parameters['wait_for_stop']:
                    while not shaker.is_shaker_home():
                        print(shaker.get_responses())

                if parameters['force_stop']:
                    shaker.stop()

                if parameters['cooldown']:
                    shaker.stop_temp_control()

    def custom_home(self, only_waste=False, drop_tips=False):
        """
        Custom code for homing the gantry to ensure that the pipette tips are raised
        and will not hit any custom labware. Also has the option to drop the tips at the end of the home

        @param drop_tips {boolean} If True, drop the tips on all loaded pipettes given that they have tips loaded.
        """

        centre_waste = types.Location(types.Point(350, 350, 120), None)
        if hasattr(self, 'right_pipette'):
            self.right_pipette.move_to(centre_waste, minimum_z_height=100)
        if hasattr(self, 'left_pipette'):
            self.left_pipette.move_to(centre_waste, minimum_z_height=100)
        else:
            print("Error, no pipette defined for homing.")

        if drop_tips:
            if hasattr(self, 'right_pipette'):
                tips = TipsWatchdog(self.right_pipette, 'STANDARD')
                tips.discard()
            if hasattr(self, 'left_pipette'):
                tips = TipsWatchdog(self.left_pipette, 'STANDARD')
                tips.discard()

        if only_waste:
            # option to skip the official homing step CU#2v7pcyg
            return
        self._protocol_context.home()

    def execute_step(self, step):
        """
        Takes the input step and processes it by calling the respective function based on step type and parameters present

        @param step {dictionary} The step to be processed
        """
        self._protocol_context.comment('\n------------\n' + step['name'] + '\n')
        self._log_step_db(step)

        if step['type'] == "simple_transfer" or step['type'] == "sequence_transfer":
            self.simple_pipetting(step['parameters'])

        elif step['type'] == "array_transfer":
            self.array_pipetting(step['parameters'])

        elif step['type'] == 'loop':
            for i in range(1, step['num_iterations'] + 1):
                self._protocol_context.comment(
                    '\n--' + step['name'] + ' Iteration ' + str(i) + '\n'
                )
                for substep in step['substeps']:
                    self.execute_step(substep)

        elif step['type'] == 'pause':

            self.custom_home(only_waste=True)

            if step['auto_resume']:
                self._protocol_context.delay(step['pause_time'])
            else:
                self._protocol_context.pause()
                self._slack_handler.send_log_message(
                    "INFO: Protocol Paused - Awaiting User Input"
                )

        elif step['type'] == 'bioshake_3000t':
            self.shake(step['parameters'])

        elif step['type'] == 'slack_message':
            if "SLACK_MESSAGE_WEBHOOK" in globals():
                self._slack_handler.send_slack_message(
                    step['message'], SLACK_MESSAGE_WEBHOOK
                )

        else:
            self._protocol_context.comment(
                '\n------------Step type: \n' + step['type'] + ' is unknown!!!\n'
            )

    def _get_devices(self) -> None:
        """
        Get devices for all loaded pipettes and the shaker if present in the protocol. Will populate the _devices
        list member with any devices found that match the criteria.
        """

        pipettes = self._get_pipettes()
        for p in pipettes:
            # https://github.com/Opentrons/opentrons/blob/edge/api/src/opentrons/hardware_control/dev_types.py
            pipette_id = p.hw_pipette.get("pipette_id", None)
            if pipette_id:
                pipette = Device()
                pipette.barcode = pipette_id
                pipette.name = p.hw_pipette.get("name", "")
                # Pipette Models https://github.com/Opentrons/opentrons/blob/edge/shared-data/pipette/definitions/pipetteModelSpecs.json
                pipette_model = p.hw_pipette.get("model", "")
                pipette.device_type = self._get_pipette_type(pipette_model)
                self._devices.append(pipette)

        # If shaker is present get its Device ID.
        slots = self._protocol_instructions['deck'].get("slots", {})
        shakerLabware = [
            obj
            for obj in slots.items()
            if obj[1].get('labware_type') == 'shakerLabware'
        ]
        if shakerLabware:
            shaker_id = self._get_shaker_id()
            if shaker_id:
                shaker = Device()
                shaker.barcode = shaker_id
                shaker.name = "Q.MTP-BIOSHAKE 3000-T"
                shaker.device_type = DeviceType.bioshake_3000t
                self._devices.append(shaker)

    def _get_step_device(self, step: dict) -> Device:
        step_type = step.get("type", "")
        if 'transfer' in step_type:
            pipette = getattr(self, step['parameters']['pipette'])
            barcode = pipette.hw_pipette.get("pipette_id", None)
            device = [device for device in self._devices if device.barcode == barcode]
            try:
                device = device[0]  # Should only be 1 pipette with the same barcode.
            except IndexError:
                device = Device()
            return device

        elif 'shake' in step_type:
            device = [
                device
                for device in self._devices
                if device.device_type == DeviceType.bioshake_3000t
            ]
            try:
                device = device[0]  # Only support for one shaker so far
            except IndexError:
                device = Device()
            return device

        else:
            return Device()

    @real_run_deco
    def _log_step_db(self, step: dict):

        step_event = StepEvent()
        step_event.device = self._get_step_device(step)
        step_event.step_id = step.get("id", 0)
        step_event.step_type = getattr(StepType, step.get("type", ""), StepType.default)
        # Temporary Fix since Shake step is BioShake. . .
        if step.get("type", "") == "bioshake_3000t":
            step_event.step_type = getattr(StepType, "shake", StepType.default)
        step_event.step_metadata = step
        self._cloud_db_handler.db_add_event(EventType.ot_step, step_event)

    def _get_pipette_type(self, pipette_model: str) -> DeviceType:
        if pipette_model == "":
            return DeviceType.unknown

        else:
            regex_attr = [a for a in dir(PipetteModelRegex) if not a.startswith('__')]
            regex_list = [getattr(PipetteModelRegex, a) for a in regex_attr]
            for i, regex in enumerate(regex_list):
                result = re.search(regex, pipette_model)
                if result:
                    try:
                        return DeviceType[regex_attr[i]]
                    except Exception:
                        return (
                            DeviceType.unknown
                        )  # PipetteModelRegex wrong member definition

            return DeviceType.unknown  # No Matches

    def _get_pipettes(self) -> List[protocol_api.InstrumentContext]:
        """
        returns the left and right pipettes if loaded

        @return List of InstrumentContext matching the loaded pipettes. List may be empty if no pipettes loaded.
        """
        pipettes = []
        if hasattr(self, "left_pipette"):
            pipettes.append(self.left_pipette)
        if hasattr(self, "right_pipette"):
            pipettes.append(self.right_pipette)
        return pipettes

    def _get_shaker_id(self) -> Union[int, None]:
        """
        Fetches and returns the ID of the loaded shaker. "shaker.get_id()" may return None if ID not found

        @return Shaker ID or None if ID not found
        """

        if not self._protocol_context.is_simulating():
            with serial.Serial(SERIALPORT, BAUDRATE, timeout=TIMEOUT) as ser:
                shaker = Shaker(ser)
                return shaker.get_id()
        return None



class ProtocolHandler:
    """
    A Class for more generic functions that apply to whole protocol
    """

    _cloud_watch_handler: CloudWatchHandler
    _slack_handler: SlackHandler
    _cloud_db_handler: CloudDBHandler
    _robot_handler: RobotHandler
    _protocol_context: ProtocolContext
    _start_time: TimeStamp
    _stop_time: TimeStamp
    _elapsed_time: TimeStamp
    _protocol_instructions: dict
    _OT_name: str

    def __init__(
        self,
        protocol_context: ProtocolContext,
        protocol_instructions: dict,
        robot_handler: 'RobotHandler',
        communications_wrapper: CommunicationsWrapper,
    ):
        self._protocol_instructions = protocol_instructions
        self._robot_handler = robot_handler
        self._protocol_context = protocol_context
        self._start_time = TimeStamp()
        self._stop_time = TimeStamp()
        self._elapsed_time = TimeStamp()
        self._cloud_watch_handler = communications_wrapper.cloud_watch_handler
        self._slack_handler = communications_wrapper.slack_handler
        self._cloud_db_handler = communications_wrapper.cloud_db_handler
        self._OT_name = communications_wrapper.OT_str_name

    def pre_protocol(self):
        """Runs commands at the start of every single protocol
        in order to ensure optimal functionality

        - Initialises a new run entry to the Cloud Database
        - Check if protocol is using shaker and check its connection
        - If shaker is used, check that it is working as expected see check_shaker()
        - If Successful print success message to the Slack Debug Channel

        @return True if passed else False
        """
        self._start_time.epoch_time = time.time()
        # How do we ensure that this device_ids are set before a call to _init_run? or dont, and if u fail device_ids = []
        self._cloud_db_handler.devices = self._robot_handler.devices
        self._cloud_db_handler.db_init_run(self._start_time)
        self._cloud_db_handler.db_add_event(EventType.ot_preflight_check)

        slots = self._protocol_instructions['deck'].get("slots", {})
        shakerLabware = [
            obj
            for obj in slots.items()
            if obj[1].get('labware_type') == 'shakerLabware'
        ]
        if not self._protocol_context.is_simulating():
            if shakerLabware:
                try:
                    self.check_shaker()
                except Exception as ex:
                    template = "Exception: {0} - Arguments:\n{1!r}"
                    message = template.format(type(ex).__name__, ex.args)
                    self._cloud_db_handler.db_add_error(message)
                    self._slack_handler.send_log_message(message)
                    return False

        self._slack_handler.send_log_message(
            "INFO: Pre-Method Successful - Running Protocol"
        )

        return True

    def post_protocol(self):
        """Commands to run at the end of every protocol

        - Calculates time_elapsed for logging purposes
        - Homes the pipette head to ensure protocol ends gracefully
        - Updates Slack with a notification the protocol has ended containing elapsed_time
        """
        self._cloud_db_handler.db_add_event(EventType.ot_postflight_check)
        self._robot_handler.custom_home(drop_tips=True)

        self._stop_time.epoch_time = time.time()
        self._elapsed_time.epoch_time = self._stop_time - self._start_time
        message = "INFO: Protocol has completed in: {}".format(
            self._elapsed_time.epoch_time
        )
        self._slack_handler.send_log_message(message)
        self._cloud_db_handler.db_end_run(self._stop_time)

    def check_shaker(self):
        """Start a run at a set Temperature to check shaker is operational

        Sends a slack message that shaker is under testing, runs the shaker for "shaker_test_duration"
        (20 seconds) with a setpoint "shaker_setpoint_delta" (10oC) higher than the current. For the
        most part this should be enough for the shaker to heat up "temp_boundary" (3oC). If this does
        not happen, there is something wrong with the shaker and an exception will be raised.
        """
        shaker_test_RPM = 50
        # Time to run test for and allow heat up to shaker_test_temp
        shaker_test_duration = 20

        temp_boundary = 3  # +/- Value in oC that the temperature is allowed to deviate
        # Value delta to set the shaker compared to its current value
        shaker_setpoint_delta = 10.0
        self._slack_handler.send_log_message("INFO: Testing Shaker")

        with serial.Serial(SERIALPORT, BAUDRATE, timeout=TIMEOUT) as ser:

            shaker = Shaker(ser)
            shaker.check_state()
            shaker_start_temp = float(shaker.get_responses()['temp='])

            shaker.start_temp_control(int(shaker_start_temp + shaker_setpoint_delta))
            shaker.start_run(shaker_test_RPM, shaker_test_duration)

            # Check to make sure that the shaker is spinning
            time.sleep(shaker_test_duration / 2)
            shaker.check_state()
            shaker_current_rpm = float(shaker.get_responses()['currentRPM'])
            if shaker_current_rpm <= 10:
                raise ValueError(
                    'Shaker is not at the required speed',
                    shaker_test_RPM,
                    shaker_current_rpm,
                )
            # Check to make sure that the shaker has heated
            time.sleep(shaker_test_duration / 2)
            shaker.check_state()
            shaker_temp_current = float(shaker.get_responses()['temp='])
            shaker.stop_temp_control()

            if (shaker_temp_current - shaker_start_temp) < temp_boundary:
                raise ValueError(
                    'Shaker temperature has not increased enough',
                    shaker_start_temp,
                    shaker_temp_current,
                )

    def upload_protocol_run(self, completed=True, error_code=None):
        """
        Build an object containing protocol metadata and run information to be pushed to AWS cloud watch

        @param completed {boolean} Used to notify the log group if the protocol was successful or not
        @param error_code {string} If completed flag is set to false, an error message should be provided

        """
        if not self._protocol_context.is_simulating():
            payload = {}
            payload['user'] = self._protocol_instructions['metadata']['author']['name']
            payload['email'] = self._protocol_instructions['metadata']['author'][
                'email'
            ]
            payload['protocol_name'] = self._protocol_instructions['metadata']['name']
            payload['protocol_description'] = self._protocol_instructions['metadata'][
                'description'
            ]
            payload['protocol_unique_id'] = 999999999
            payload['date_created'] = (
                self._protocol_instructions['metadata']['created'] // 1000
            )  # Convert ms to s
            payload['time_started'] = int(self._start_time.epoch_time)
            payload['time_elapsed'] = int(self._elapsed_time.epoch_time)
            payload['robot_name'] = self._OT_name
            payload['transpiler_version'] = TRANSPILER_VERSION
            payload['comment'] = "N/A"
            payload['completed'] = completed
            payload['error_code'] = error_code

            self._cloud_watch_handler.post_cloud_watch("protocol-logs", payload)


"""
Main Transpiler code-loop running pre- and post- protocol steps, and iterating
through all user defined steps stored in the instructions file. Initialises required classes,
and handles major step exceptions"""
metadata = {'apiLevel': '2.12', "transpiler_version": "220722"}
os.environ['TZ'] = 'UTC'  ## Ensure that the timezone of the system being used is UTC
time.tzset()


def match_sequences(seq_a, seq_b, seq_c):
    """Matching the sequences length, only works if the sequences are sub-multiples of the longest sequence"""

    maxlen = max(len(seq_a), len(seq_b), len(seq_c))

    new_seq_a = [[x] * (maxlen // len(seq_a)) for x in seq_a]
    new_seq_a = list(itertools.chain.from_iterable(new_seq_a))

    new_seq_b = [[x] * (maxlen // len(seq_b)) for x in seq_b]
    new_seq_b = list(itertools.chain.from_iterable(new_seq_b))

    new_seq_c = [[x] * (maxlen // len(seq_c)) for x in seq_c]
    new_seq_c = list(itertools.chain.from_iterable(new_seq_c))

    return [new_seq_a, new_seq_b, new_seq_c]


def run(protocol_context: protocol_api.ProtocolContext):
    """
    The main run loop, see the main page for more in depth information on
    how the transpiler architecture looks.
    """
    global IS_SIMULATING
    IS_SIMULATING = protocol_context.is_simulating()
    communications_wrapper = CommunicationsWrapper(
        protocol_instructions, protocol_context
    )
    robot_handler = RobotHandler(
        protocol_context, protocol_instructions, communications_wrapper
    )
    protocol_handler = ProtocolHandler(
        protocol_context, protocol_instructions, robot_handler, communications_wrapper
    )

    if protocol_handler.pre_protocol():
        for step in protocol_instructions['steps']:
            try:
                robot_handler.execute_step(step)
            except Exception as ex:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                # error_msg = f'ERROR: {exc_type} @ {exc_traceback.tb_lineno} - Step: {step["name"]}\n'
                error_msg = f'ERROR: {ex} @ {exc_traceback.tb_lineno}'
                try:
                    protocol_context.comment(error_msg)
                    communications_wrapper.cloud_db_handler.db_add_error(error_msg)
                    communications_wrapper.slack_handler.send_log_message(
                        traceback.format_exc()
                    )
                except Exception:
                    continue

        protocol_handler.post_protocol()
        protocol_handler.upload_protocol_run()

    else:
        communications_wrapper.slack_handler.send_log_message(
            "INFO: Protocol has Failed!"
        )
        protocol_handler.upload_protocol_run(False, "Protocol Failed to run Pre Step")

class Shaker:
    """A class for defining QInstruments BioShake 3000T specific commands

    Contains functions to communicate with the shaker over serial as well as
    support for shaker responses
    """

    def __init__(self, serial):
        """
        Assign the serial port to the class and Leave Eco Mode for the shaker

        @param serial The opened serial port for the shaker
        """

        ## The current shaker serial port object
        self.serial = serial
        self.write('lem\r')
        ## Contains the shaker response for any commands sent
        self.response = {}

    def write(self, cmd):
        """
        Write a command to the shaker, and try to read and return the response

        @param cmd command to send to shaker

        @return the decoded response from the shaker
        """
        self.serial.reset_input_buffer()
        self.serial.reset_output_buffer()
        self.serial.write(str.encode(cmd))
        time.sleep(0.2)
        rawResponse = self.serial.readline()
        try:
            value = rawResponse.decode()
            return value[:-2]
        except Exception:
            return None

    def start_run(self, rpm, duration):
        """
        Start a shaker run with at a set speed for a duration

        @param rpm Shaker speed
        @param duration Duration for shaking
        """
        self.write('ssts' + str(rpm) + '\r')  # Set Shake Target Speed
        self.write('sonwr' + str(duration) + '\r')  # Shake On With Run-time
        self.write('sgh\r')  # Shake Go Home
        self.check_state()

    def check_state(self):
        """
        Get shaker information such as state, Temperature, setpoint and save to response member variable
        """
        self.response['state'] = self.write('gsst\r')  # Get Shaker State
        self.response['tempState'] = self.write('gtsas\r')  # Get Temp State
        self.response['temp='] = self.write('gta\r')  # Get Temp Reading
        self.response['setpoint'] = self.write('gtt\r')  # Get setpoint
        self.response['currentRPM'] = self.write('gsas\r')  # Get current RPM

    def is_shaker_home(self):
        """
        Check if the shaker is in "home" state and return boolean

        @return True if "home" else False. Serial Error or no response also return false
        """
        self.check_state()
        if self.response:
            return self.response['state'] == str(
                3
            )  # 3 is hard-coded for the "home" state
        else:
            return False

    def start_temp_control(self, setpoint):
        """
        Set the shaker to the setpoint provided and start temperature tracking
        @param setpoint The value for the shaker setpoint
        """
        self.write('stt' + str(setpoint * 10) + '\r')  # Temperature setpoint
        if setpoint > 0:
            self.write('ton\r')  # Turn on Temp Control
        self.response['tempState'] = self.write('gtsas\r')  # Get Temp State

    def stop_temp_control(self):
        """
        Stop shaker temperature tracking
        """
        self.write('toff\r')  # Turn off Temp Control

    def get_responses(self):
        """
        Getter for the responses member to check that it worked
        """
        return self.response

    def stop(self):
        """
        Stop the shaker shaking
        """
        self.write('soff\r')  # Shaker OFF

    def get_id(self):
        self.write('info\r')
        time.sleep(0.5)
        info = self.serial.read(self.serial.in_waiting).split(b'\r\n')
        for line in info:
            if re.search("Serial", str(line)):
                # trunk-ignore(flake8/W605)
                return re.search("Serial.:.(?P<ID>\d+.\d+)", str(line))['ID']
        return None
