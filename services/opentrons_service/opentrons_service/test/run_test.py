#!/usr/bin/env python3

import argparse
import os
import subprocess

test_protocol = "test_instructions"
dir_path = os.path.dirname(os.path.realpath(__file__))
OPG_path = dir_path + "/.."


def main(args):
    parser = argparse.ArgumentParser(
        description='Protocol Generator and Simulate')
    parser.add_argument(
        '--test_instructions',
        '-i',
        default=test_protocol,
        metavar='-i',
        help='Robot Instructions Json file',
        required=False,
    )

    args = parser.parse_args()

    print(
        f"\n#############################################################################\
        \n   Generating and running protocol based on \"{args.test_instructions}.json\"\
        \n#############################################################################\n")

    if args.test_instructions.find('.json') != -1:
        print(
            "\n##############\nPlease remove .json from input file argument\n##############\n")
        exit()

    try:
        subprocess.run(["python3 " + OPG_path + "/opentrons_cli.py --input " +
                        args.test_instructions + ".json  --output " +
                        args.test_instructions + ".py"], check=True, shell=True)
    except Exception as e:
        print(
            f"\n##############\nFailed to run instruction file \"{args.test_instructions}.json\"\nPlease make sure the input file name is correct and the .json file is stored in /instructions\n##############\n")
        exc_type, exc_value, exc_traceback = sys.exc_info()
        print(f'ERROR: {exc_type}\n{exc_value} @ {exc_traceback.tb_lineno}')
        exit()

    subprocess.run(["opentrons_simulate " + dir_path + "/protocols/" +
                    args.test_instructions + ".py -L " + dir_path +
                    "/../../../shared_data/labware/"], check=True, shell=True)


if __name__ == '__main__':
    import sys

    sys.exit(main(sys.argv))
