# Code Lint Header
import re
import time

import serial
from bioshake_3000T import Shaker
from global_definitions import BAUDRATE, SERIALPORT, TIMEOUT, TRANSPILER_VERSION
from opentrons.protocol_api import InstrumentContext, ProtocolContext
from opentrons_communications_handlers import (
    AssetEventsDBHandler,
    CloudWatchHandler,
    CommunicationsWrapper,
    SlackHandler,
)
from opentrons_models import EventType, TimeStamp
from opentrons_robot_handler import RobotHandler

# End Code Lint Header


class ProtocolHandler:
    """
    A Class for more generic functions that apply to whole protocol
    """

    _cloud_watch_handler: CloudWatchHandler
    _slack_handler: SlackHandler
    _asset_events_db_handler: AssetEventsDBHandler
    _robot_handler: RobotHandler
    _protocol_context: ProtocolContext
    _start_time: TimeStamp
    _stop_time: TimeStamp
    _elapsed_time: TimeStamp
    _protocol_instructions: dict
    _OT_name: str

    def __init__(
        self,
        protocol_context: ProtocolContext,
        protocol_instructions: dict,
        robot_handler: RobotHandler,
        communications_wrapper: CommunicationsWrapper,
    ):
        self._protocol_instructions = protocol_instructions
        self._robot_handler = robot_handler
        self._protocol_context = protocol_context
        self._start_time = TimeStamp()
        self._stop_time = TimeStamp()
        self._elapsed_time = TimeStamp()
        self._cloud_watch_handler = communications_wrapper.cloud_watch_handler
        self._slack_handler = communications_wrapper.slack_handler
        self._asset_events_db_handler = communications_wrapper.asset_events_db_handler
        self._OT_name = communications_wrapper.OT_str_name

    def pre_protocol(self):
        """Runs commands at the start of every single protocol
        in order to ensure optimal functionality

        - Initialises a new run entry to the Cloud Database
        - Check if protocol is using shaker and check its connection
        - If shaker is used, check that it is working as expected see check_shaker()
        - Runs sanity check to ensure protocol instructions will run
        - If Successful print success message to the Slack Debug Channel

        @return True if passed else False
        """
        self._start_time.epoch_time = time.time()
        # How do we ensure that this device_ids are set before a call to _init_run? or don't, and if u fail device_ids = []
        self._asset_events_db_handler.devices = self._robot_handler.devices

        run_id = self._asset_events_db_handler.db_init_run(self._start_time)
        self._asset_events_db_handler.db_add_event(EventType.ot_preflight_check)

        slots = self._protocol_instructions['deck'].get("slots", {})
        shaker_labware = [
            obj
            for obj in slots.items()
            if obj[1].get('labware_type') == 'shakerLabware'
        ]

        self._protocol_context.comment("Pre-Protocol Step")

        if not self._protocol_context.is_simulating():
            if shaker_labware:
                try:
                    self.check_shaker()
                except Exception as ex:
                    template = "Exception: {0} - Arguments:\n{1!r}"
                    message = template.format(type(ex).__name__, ex.args)
                    self._asset_events_db_handler.db_add_error(message)
                    self._slack_handler.send_log_message(message)
                    return False

        if not self.instructions_sanity_check():
            self._protocol_context.comment("ERROR: JSON Sanity Check failed")
            return False

        self._slack_handler.send_log_message(
            "INFO: Pre-Method Successful - Running Protocol with DB ID:"
            + str(run_id or 0)
        )

        return True

    def instructions_sanity_check(self) -> bool:
        """
        A set of checks to be ran on the JSON instructions file containing the protocol setup
        in order to ensure that the protocol is valid and can run with no issues.
        Current Checks:
        - Ensure multichannel pippetes do not reference any non A row on labware.

        @return {bool} True if passed else False
        """

        transfer_steps = ["sequence_transfer", "simple_transfer"]
        success_flag = True

        for step in self._protocol_instructions['steps']:
            if step['type'] in transfer_steps:
                step_parameters = step['parameters']
                if hasattr(self._robot_handler, step_parameters['pipette']):
                    pipette: InstrumentContext
                    pipette = getattr(self._robot_handler, step_parameters['pipette'])
                    if pipette.channels > 1:
                        # Multichannel pipettes can only service wells in the top row.
                        wells = []
                        if step['type'] == "simple_transfer":
                            for well in step_parameters['source']['wells']:
                                wells.append(well)
                            for well in step_parameters['destination']['wells']:
                                wells.append(well)

                        elif step['type'] == "sequence_transfer":
                            step_sequences = [
                                step_parameters["source_sequence"],
                                step_parameters["destination_sequence"],
                            ]
                            for seq_name, labwares in self._protocol_instructions[
                                'sequences'
                            ].items():
                                if seq_name in step_sequences:
                                    for labware in labwares:
                                        for well in labware['wells']:
                                            wells.append(well)

                        unservicable_wells = [
                            well
                            for well in wells
                            # trunk-ignore(flake8/W605)
                            if re.search('[B-Z|b-z]\d{1,2}', well)
                        ]
                        if unservicable_wells:
                            self._protocol_context.comment(
                                f"ERROR: Multichannel Pipette cannot service {', '.join(unservicable_wells)} in step: {step['name']}"
                            )
                            success_flag = False
                else:
                    self._protocol_context.comment(
                        f"ERROR: Pipette does not exist for step: {step['name']}"
                    )

        return success_flag

    def post_protocol(self, run_success: bool):
        """Commands to run at the end of every protocol

        - Calculates time_elapsed for logging purposes
        - Homes the pipette head to ensure protocol ends gracefully
        - Updates Slack with a notification the protocol has ended containing elapsed_time
        """
        self._protocol_context.comment("Post-Protocol Step")
        self._asset_events_db_handler.db_add_event(EventType.ot_postflight_check)
        self._robot_handler.custom_home(drop_tips=True)

        self._stop_time.epoch_time = time.time()
        self._elapsed_time.epoch_time = self._stop_time - self._start_time
        if run_success:
            self._slack_handler.send_log_message(
                f"INFO: Protocol has successfully completed in: {self._elapsed_time.elapsed_time}"
            )
        else:
            self._slack_handler.send_log_message(
                f"INFO: Protocol has unsuccessfully completed in: {self._elapsed_time.elapsed_time}"
            )
        self._asset_events_db_handler.db_end_run(self._stop_time)

    def check_shaker(self):
        """Start a run at a set Temperature to check shaker is operational

        Sends a slack message that shaker is under testing, runs the shaker for "shaker_test_duration"
        (20 seconds) with a setpoint "shaker_setpoint_delta" (10oC) higher than the current. For the
        most part this should be enough for the shaker to heat up "temp_boundary" (3oC). If this does
        not happen, there is something wrong with the shaker and an exception will be raised.
        """
        shaker_test_RPM = 50
        # Time to run test for and allow heat up to shaker_test_temp
        shaker_test_duration = 20

        temp_boundary = 3  # +/- Value in oC that the temperature is allowed to deviate
        # Value delta to set the shaker compared to its current value
        shaker_setpoint_delta = 10.0
        self._slack_handler.send_log_message("INFO: Testing Shaker")

        with serial.Serial(SERIALPORT, BAUDRATE, timeout=TIMEOUT) as ser:

            shaker = Shaker(ser)
            shaker.check_state()
            shaker_start_temp = float(shaker.get_responses()['temp='])

            shaker.start_temp_control(int(shaker_start_temp + shaker_setpoint_delta))
            shaker.start_run(shaker_test_RPM, shaker_test_duration)

            # Check to make sure that the shaker is spinning
            time.sleep(shaker_test_duration / 2)
            shaker.check_state()
            shaker_current_rpm = float(shaker.get_responses()['current_RPM'])
            if shaker_current_rpm <= 10:
                raise ValueError(
                    'Shaker is not at the required speed',
                    shaker_test_RPM,
                    shaker_current_rpm,
                )
            # Check to make sure that the shaker has heated
            time.sleep(shaker_test_duration / 2)
            shaker.check_state()
            shaker_temp_current = float(shaker.get_responses()['temp='])
            shaker.stop_temp_control()

            if (shaker_temp_current - shaker_start_temp) < temp_boundary:
                raise ValueError(
                    'Shaker temperature has not increased enough',
                    shaker_start_temp,
                    shaker_temp_current,
                )

    def upload_protocol_run(self, completed=True, error_code=None):
        """
        Build an object containing protocol metadata and run information to be pushed to AWS cloud watch

        @param completed {boolean} Used to notify the log group if the protocol was successful or not
        @param error_code {string} If completed flag is set to false, an error message should be provided

        """
        if not self._protocol_context.is_simulating():
            payload = {}
            payload['user'] = self._protocol_instructions['metadata'][
                'created_by'
            ].split("@")[0]
            payload['email'] = self._protocol_instructions['metadata']['created_by']
            payload['protocol_name'] = self._protocol_instructions['metadata']['name']
            payload['protocol_description'] = self._protocol_instructions['metadata'][
                'description'
            ]
            payload['protocol_unique_id'] = 999999999
            payload['date_created'] = self._protocol_instructions['metadata'][
                'created_at'
            ]
            payload['time_started'] = int(self._start_time.epoch_time)
            payload['time_elapsed'] = int(self._elapsed_time.epoch_time)
            payload['robot_name'] = self._OT_name
            payload['transpiler_version'] = TRANSPILER_VERSION
            payload['comment'] = "N/A"
            payload['completed'] = completed
            payload['error_code'] = error_code

            self._cloud_watch_handler.post_cloud_watch("protocol-logs", payload)
