# Code Lint Header
import itertools
import os
import sys
import time
import traceback

from global_definitions import OT_CREDENTIALS_PATH
from opentrons import protocol_api
from opentrons_communications_handlers import CommunicationsWrapper
from opentrons_protocol_handler import ProtocolHandler
from opentrons_robot_handler import RobotHandler

protocol_instructions = {}
# End Code Lint Header

"""
Main Transpiler code-loop running pre- and post- protocol steps, and iterating
through all user defined steps stored in the instructions file. Initialises required classes,
and handles major step exceptions"""

metadata = {'apiLevel': '2.12', "transpiler_version": "220722"}
# Ensure that the timezone of the system being used is UTC
os.environ['TZ'] = 'UTC'
try:
    time.tzset()  # tzset is not implemented in windows environments, so we want to catch the error and continue
except AttributeError:
    pass


def match_sequences(seq_a, seq_b, seq_c):
    """Matching the sequences length, only works if the sequences are sub-multiples of the longest sequence"""

    maxlen = max(len(seq_a), len(seq_b), len(seq_c))

    new_seq_a = [[x] * (maxlen // len(seq_a)) for x in seq_a]
    new_seq_a = list(itertools.chain.from_iterable(new_seq_a))

    new_seq_b = [[x] * (maxlen // len(seq_b)) for x in seq_b]
    new_seq_b = list(itertools.chain.from_iterable(new_seq_b))

    new_seq_c = [[x] * (maxlen // len(seq_c)) for x in seq_c]
    new_seq_c = list(itertools.chain.from_iterable(new_seq_c))

    return [new_seq_a, new_seq_b, new_seq_c]


def run(protocol_context: protocol_api.ProtocolContext):
    """
    The main run loop, see the main page for more in depth information on
    how the transpiler architecture looks.
    """
    global IS_SIMULATING
    IS_SIMULATING = protocol_context.is_simulating()

    add_indexes_array(protocol_instructions['steps'])

    communications_wrapper = CommunicationsWrapper(
        protocol_instructions, protocol_context, OT_CREDENTIALS_PATH
    )
    robot_handler = RobotHandler(
        protocol_context, protocol_instructions, communications_wrapper
    )
    protocol_handler = ProtocolHandler(
        protocol_context, protocol_instructions, robot_handler, communications_wrapper
    )

    protocol_success = True

    if protocol_handler.pre_protocol():
        for step in protocol_instructions['steps']:
            try:
                robot_handler.execute_step(step)
            except Exception:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                tb_lines = traceback.format_exception(exc_type, exc_value, exc_traceback)
                tb_text = ''.join(tb_lines)
                error_msg = f'ERROR: {exc_type} - {exc_value} @ {exc_traceback.tb_lineno} - Step: {step["name"]}\n'
                error_msg += f'Traceback:\n{tb_text}'

                try:
                    protocol_context.comment(error_msg)
                    communications_wrapper.asset_events_db_handler.db_add_error(
                        error_msg
                    )
                    communications_wrapper.slack_handler.send_log_message(
                        traceback.format_exc()
                    )
                    communications_wrapper.slack_handler.send_slack_message(
                        build_failed_step(step)
                    )
                    communications_wrapper.slack_handler.send_log_message(
                        "INFO: Protocol has Failed!"
                    )
                    protocol_success = False
                except Exception:
                    break

        protocol_handler.post_protocol(protocol_success)
        protocol_handler.upload_protocol_run()

    else:
        communications_wrapper.slack_handler.send_log_message(
            "INFO: Protocol has Failed to run pre-flight check!"
        )
        protocol_handler.upload_protocol_run(
            False, "Protocol Failed to run pre-flight check"
        )
    
    return protocol_success;

def add_indexes_array(steps, indexes = []):
    """
    Adds to each step and sub-step an array containing it's own position
    in the array of steps, and all of their parent's positions.
    """
    for index, step in enumerate(steps):
        step["indexes"] = indexes + [index]
        if step['substeps'] is not None:
            add_indexes_array(step['substeps'], step["indexes"])

def build_failed_step(step: dict) -> str:
    step_name = step.get("name", "Unnamed")
    step_position = step.get("position", "")
    if step_position:
        return (
            f"Protocol failed at step: '{step_name}' found at position {step_position}"
        )
    else:
        return f"Protocol failed at step: '{step_name}'"
