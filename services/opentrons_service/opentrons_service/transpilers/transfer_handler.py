# Code Lint Header
import math
from typing import List, Sequence, Union

from opentrons import types
from opentrons.protocol_api import InstrumentContext, ProtocolContext, labware
from opentrons_communications_handlers import SlackHandler
from opentrons_models import TipMaxLiquidHeights, WellLocations
from opentrons_shared_data.pipette import name_config

# End Code Lint Header

AdvancedLiquidHandling = Union[
    labware.Well,
    types.Location,
    Sequence[Union[labware.Well, types.Location]],
    Sequence[Sequence[labware.Well]],
]


class LiquidClass:
    """
    Class to store Liquid Parameters for use in transfer steps
    """

    _touch_tip: bool
    _blow_out: bool
    _blowout_location: str
    _carryover: bool
    _air_gap: float
    _mix_before: Union[tuple, None]
    _mix_after: Union[tuple, None]
    _aspirate_rate: float
    _dispense_rate: float
    _blow_out_rate: float
    _new_tip: str
    _use_adaptive_speeds: bool
    _max_liquid_speed: int

    _pipette: InstrumentContext
    _slack_handler: SlackHandler

    def __str__(self):
        return f"{LiquidClass:} \n{str(self.__dict__)}"

    def __init__(
        self,
        liquid_class_parameters: dict,
        pipette: InstrumentContext,
        slack_handler: SlackHandler,
    ) -> None:
        # Init Stuff with defaults

        self._touch_tip = liquid_class_parameters.get("touch_tip", False)
        self._blow_out = liquid_class_parameters.get("blow_out", True)
        self._blowout_location = liquid_class_parameters.get(
            "blowout_location", "destination well"
        )
        self._carryover = liquid_class_parameters.get("carryover", True)
        self._air_gap = liquid_class_parameters.get("air_gap", 0)
        self._mix_before = liquid_class_parameters.get("mix_before", None)
        self._mix_after = liquid_class_parameters.get("mix_after", None)
        self._aspirate_rate = liquid_class_parameters.get("aspirate_rate", 1)
        self._dispense_rate = liquid_class_parameters.get("dispense_rate", 1)
        self._blow_out_rate = liquid_class_parameters.get("blow_out_rate", 1)
        self._new_tip = liquid_class_parameters.get("new_tip", "never")
        self._use_adaptive_speeds = liquid_class_parameters.get(
            "use_adaptive_speeds", False
        )
        self._max_meniscus_speed = liquid_class_parameters.get("max_meniscus_speed", 1)

        self._pipette = pipette
        self._slack_handler = slack_handler

    def calc_rate(self, action: str):
        """
        Used to first calculate the flow rate based on the ratio provided in the instructions file for aspirate, dispense,
        and blow_out. The absolute values of max, min, and default rate differ depending on the type of pipette used.
        Resource https://github.com/Opentrons/opentrons/blob/edge/shared-data/pipette/definitions/pipetteNameSpecs.json

        Once the absolute rate has been calculated, it is set in the pipette object ready for the transfer step.
        Note that the rates are stored/defined in the liquid class associated with the step.

        @param action The action to calculate for, [aspirate, dispense, blowout]
        """

        # Get Rates
        if action == 'aspirate':
            pipette_flow_rates = name_config()[self._pipette.name].get(
                'defaultAspirateFlowRate'
            )
            rate = self._aspirate_rate
        elif action == 'dispense':
            pipette_flow_rates = name_config()[self._pipette.name].get(
                'defaultDispenseFlowRate'
            )
            rate = self._dispense_rate
        elif action == 'blow_out':
            pipette_flow_rates = name_config()[self._pipette.name].get(
                'defaultBlowOutFlowRate'
            )
            rate = self._blow_out_rate
        else:
            self._slack_handler.send_log_message("Error: Unknown calc_rate action")
            return None

        # Populate default max min
        if pipette_flow_rates:
            default_rate = pipette_flow_rates['value']
            min_rate = pipette_flow_rates['min']
            max_rate = pipette_flow_rates['max']
        else:
            self._slack_handler.send_log_message(
                f"Error: Can not fetch {action} rate from file."
            )

        # Rates are defined between 0-2. With 0->1 representing a linear gradient
        # between the pipettes minimum rate and its defaul rate, and 1->2 a second linear
        # gradient between the default rate and the max rate. i.e. 0.4 and 1.4 represent 40%
        # on their respective gradients.

        if rate >= 0 and rate < 1:
            return ((default_rate - min_rate) * rate) + min_rate
        elif rate == 1:
            return default_rate
        elif rate > 1 and rate <= 2:
            return ((max_rate - default_rate) * (rate - 1)) + default_rate
        else:
            self._slack_handler.send_log_message(
                "Error: Rate Error, please enter proper rate"
            )
            return None

    @property
    def opentrons_transfer_kwargs(self):

        transfer_kwargs = dict(
            new_tip=self._new_tip,
            # trash = self._trash,
            touch_tip=self._touch_tip,
            blow_out=self._blow_out,
            blowout_location=self._blowout_location,
            mix_before=self._mix_before,
            mix_after=self._mix_after,
            # gradient = self._gradient,
            carryover=self._carryover,
            # disposal_ = self._disposal_volume
        )
        return transfer_kwargs

    @property
    def touch_tip(self):
        return self._touch_tip

    @property
    def blow_out(self):
        return self._blow_out

    @blow_out.setter
    def blow_out(self, value):
        self._blow_out = value

    @property
    def use_adaptive_speeds(self):
        return self._use_adaptive_speeds

    @property
    def mix_after(self):
        return self._mix_after

    @property
    def mix_before(self):
        return self._mix_before

    @property
    def max_meniscus_speed(self):
        return self._max_meniscus_speed


class TransferHandler:
    """
    In house transfer handler
    """

    ADAPTIVE_ASPIRATE_WITHDRAWAL_SPEED = 20  # mm/s
    ADAPTIVE_TRANSFER_NUM_SEGMENTS = 4

    _pipette: InstrumentContext
    _liquid_class: LiquidClass
    _protocol_context: ProtocolContext

    _volume: Union[float, Sequence[float]]
    _source: AdvancedLiquidHandling
    _destination: AdvancedLiquidHandling

    _segment_flowrates: List[float] = ADAPTIVE_TRANSFER_NUM_SEGMENTS * [0]
    _segment_volumes: List[float] = ADAPTIVE_TRANSFER_NUM_SEGMENTS * [0]

    def __init__(
        self,
        pipette: InstrumentContext,
        liquid_class: LiquidClass,
        protocol_context: ProtocolContext,
    ) -> None:

        self._pipette = pipette
        self._protocol_context = protocol_context
        self._liquid_class = liquid_class
        self._volume = None
        self._source = None
        self._destination = None

    def load_transfer_params(
        self,
        volume: Union[float, Sequence[float]],
        source: AdvancedLiquidHandling,
        destination: AdvancedLiquidHandling,
    ):
        """
        Loads the transfer params into the class
        """
        self._volume = volume
        self._source = source
        self._destination = destination

    def transfer(self, post_transfer_blowout=False):
        """
        Executes a transfer for the pipette loaded into the transfer handler based on parameters loaded
        Contains logic to select speciffic transfer methods (in-house, opentrons, etc..)
        """

        if self._liquid_class.use_adaptive_speeds:
            self.adaptive_speed_transfer()
        else:
            self.standard_opentrons_transfer()

        if post_transfer_blowout:
            if self._destination.labware.is_well:
                self._pipette.move_to(
                    self._destination.labware.as_well().top(3), publish=False
                )
                self._pipette.blow_out(self._destination)

    def standard_opentrons_transfer(self):
        """
        Standard opentrons transfer as documented in their Instrument Context class
        """
        self._pipette.transfer(
            self._volume,
            self._source,
            self._destination,
            **self._liquid_class.opentrons_transfer_kwargs,
        )

    def adaptive_speed_transfer(self):
        """
        An in house version of the pipette.transfer function using atomic Opentrons
        transfer functions to support pipetting of non standard liquids i.e. viscous, volatile ...
        """
        self._protocol_context.comment(
            f"\n###### Transfer {self._volume} from {self._source.labware} to { self._destination.labware} ######"
        )

        mix_before_params = self._liquid_class.mix_before

        if mix_before_params is not None:
            self._pipette.mix(mix_before_params[0], mix_before_params[1], self._source)

            self._pipette.blow_out()
            self.shake_tip(self._source, 2)

        # Grab current values so we can undo changes to come
        original_pipette_flow_rate = self._pipette.flow_rate.dispense
        original_gantry_speed = self._pipette.default_speed

        # split transfer in to smaller volumes, as needed
        step_count = math.ceil(self._volume / self._pipette.max_volume)
        for i in range(step_count):
            step_volume = self._volume / step_count
            self._protocol_context.comment(f"\nStep{i+1}: Transferring {step_volume}ul")

            # ASPIRATE
            self._pipette.aspirate(step_volume, self._source)

            # slow withdrawal & shake off drops
            self._pipette.default_speed = self.ADAPTIVE_ASPIRATE_WITHDRAWAL_SPEED
            self._pipette.move_to(self._source.labware.as_well().top(), publish=False)
            self._pipette.default_speed = original_gantry_speed
            self.shake_tip(self._source, 2)

            # DISPENSE
            self.calculate_segment_arrays(step_volume)
            # Segment dispense, blowout and shakeoff drops
            for i, speed in enumerate(self._segment_flowrates):
                if self._segment_volumes[i] > 0:
                    self._pipette.flow_rate.dispense = speed
                    self._pipette.dispense(self._segment_volumes[i], self._destination)

            self._pipette.blow_out()
            self.shake_tip(self._destination, 2)

        # Undo dispense flow rate adjustments made during segment dispense
        self._pipette.flow_rate.dispense = original_pipette_flow_rate

        mix_after_params = self._liquid_class.mix_after
        if mix_after_params is not None:
            self._pipette.mix(
                mix_after_params[0], mix_after_params[1], self._destination
            )

            self._pipette.blow_out()
            self.shake_tip(self._destination, 2)

    def calculate_segment_arrays(self, step_volume: float):
        """
        Calculates the 2 segment arrays storing speed and volume for each segment
        for use in the adaptive speed dispense.

        @param step_volume The total volume to be transferred this step to be split into segments

        """
        tip_volume = self._pipette._last_tip_picked_up_from.max_volume

        # VOLUME CUTTOFFS
        relative_segments = [1 / 12, 1 / 6, 1 / 4, 1 / 2]  # = [8%, 17%, 25%, 50%]
        if sum(relative_segments) != 1:
            raise Exception("The sume of the relative segmens must be 1")

        pipette_volume_cutoffs = [vol * tip_volume for vol in relative_segments]

        self.calculate_segment_flowrates(tip_volume, pipette_volume_cutoffs)
        self.calculate_segment_volumes(step_volume, pipette_volume_cutoffs)

        # Both arrays are calculated low to high for simplicity.
        # When running we want to operate high to low however.
        self._segment_flowrates.reverse()
        self._segment_volumes.reverse()

    def calculate_segment_flowrates(self, tip_volume, volume_cutoffs):
        """
        For each segment in the volume cuttoffs, calculate the required flow rate based on the
        segments free liquid surface area times the max meniscus speed as specified by the protocol.
        The segment area is calculated using the liquid volume and height assuming a conical tip.
        """
        # This is the standard value for the loaded pipette used as a max for the folllowing calcs
        max_flowrate = name_config()[self._pipette.name].get('defaultAspirateFlowRate')[
            'value'
        ]
        max_meniscus_speed = self._liquid_class.max_meniscus_speed

        tip_rack_name = self._pipette._last_tip_picked_up_from.parent.name
        max_liquid_height = getattr(TipMaxLiquidHeights, tip_rack_name, None)

        if not max_liquid_height:
            raise Exception(f"No max liquid height parameter found for {tip_rack_name}")

        r_max = (3 * tip_volume / (max_liquid_height * math.pi)) ** 0.5
        tan_alpha = r_max / max_liquid_height

        cumulative_volume = 0
        for i, segment_volume in enumerate(volume_cutoffs):
            cumulative_volume += segment_volume
            segment_radius = (3 * cumulative_volume * tan_alpha / math.pi) ** (1 / 3)
            segment_area = math.pi * segment_radius**2

            flowrate = max_meniscus_speed * segment_area
            self._segment_flowrates[i] = (
                flowrate if flowrate < max_flowrate else max_flowrate
            )

    def calculate_segment_volumes(self, step_volume, volume_cutoffs):
        """
        For each segment in the volume cuttoffs, calculate the required volume using the defined cutoffs.
        This function also accounts for situations where the pipette is not 100% full.
        """
        remaining_step_volume = step_volume
        for i, segment_volume in enumerate(volume_cutoffs):
            if remaining_step_volume > 0:
                self._segment_volumes[i] = (
                    segment_volume
                    if (remaining_step_volume > segment_volume)
                    else remaining_step_volume
                )
                remaining_step_volume = remaining_step_volume - segment_volume

    def shake_tip(self, location: AdvancedLiquidHandling, shake_count: int):
        """
        Simulates touchtip, but as a method to shake drops off (e.g. in a reservoir)

        @param location: The location for the shaking to occur
        @param shake_count The number of reps for the shaking action
        """

        self._protocol_context.comment(
            f"Shaking tip at {location.labware} - {shake_count} times"
        )

        shaking_locations = [
            WellLocations.middle,
            WellLocations.right,
            WellLocations.left,
            WellLocations.middle,
            WellLocations.top,
            WellLocations.bottom,
            WellLocations.middle,
        ]

        origin = location.labware.as_well().top()
        self._pipette.move_to(origin, publish=False)

        for _ in range(shake_count):
            for location in shaking_locations:
                self._pipette.move_to(origin.move(location), publish=False)
