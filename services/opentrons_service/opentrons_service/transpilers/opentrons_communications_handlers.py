# Code Lint Header
import json
import os
import re
import socket
import uuid
from enum import Enum, auto
from typing import List, Tuple, Type, Union

import requests
from opentrons.protocol_api import ProtocolContext
from opentrons_models import (
    Credentials,
    Device,
    DeviceType,
    EventData,
    EventType,
    Operator,
    PostgRESTCredentials,
    SerialPrimaryKey,
    StepType,
    TimeStamp,
    real_run_deco,
)

# End Code Lint Header


class OTName:
    """
    Class to retrieve and store OpenTrons Name for various logging and messaging actions
    """

    _OT_name: str

    def __init__(self) -> None:
        self._OT_name = self.extract_name()

    def extract_name(self) -> str:
        """
        Extract the name of the current Opentrons

        @return {string} The name of the Opentrons
        """
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            # this can be any non 0 IP address.
            s.connect(('10.255.255.255', 1))
            # Now that it is searching for a connection we can request the IP
            robot_ip_info = s.getsockname()[0]

            # This outputs a list with the [IP Address, port_number]
            endpoint = 'http://{robot_ip}:31950/server/name'
            r = requests.get(endpoint.format(robot_ip=robot_ip_info))
            return r.json()['name']
        except Exception:
            return "Unknown"

    @property
    def str_name(self):
        return self._OT_name


class SlackHandler:
    """
    Library of useful functions for communicating protocol status to Slack
    """

    _OT_name: str
    _protocol_title: str
    _log_webhook: str
    _message_webhook: str

    def __init__(
        self, protocol_title, ot_name: OTName, slack_log_wh: str, slack_msg_wh: str
    ):
        self._OT_name = ot_name.str_name
        self._protocol_title = protocol_title
        self._log_webhook = slack_log_wh
        self._message_webhook = slack_msg_wh

    @real_run_deco
    def send_slack(self, message, webhook):
        """
        Send a message to the specified slack channel

        @param message {string} The message to send
        @param webhook {string} Slack webhook for the slack channel where the message should be sent

        @return The request response if needed.
        """
        try:
            message = f"\"{self._protocol_title}\" on {self._OT_name} - {message}"
            data = {"text": message}
            requests.post(webhook, json=data)

        except Exception:
            # No connection, no protocol handler so no way to log error.
            return

    def send_slack_message(self, message):
        """
        Function containing code to handle specifically slack messages to OT_Messaging Channel. Currently just calls
        the send_slack() function with self._message_webhook. .

        @param message The message to send.
        """
        if self._message_webhook:
            self.send_slack(message, self._message_webhook)

    def send_log_message(self, log):
        """
        Function containing code to handle specifically slack los to OT_Testing Channel. Currently just calls
        the send_slack() function with self._log_webhook. .

        @param log The log to send.
        """
        if self._log_webhook:
            self.send_slack(log, self._log_webhook)


class CloudWatchHandler:
    """Library of useful functions for communicating with cloudwatch"""

    _endpoint: str

    def __init__(self, endpoint) -> None:
        self._endpoint = endpoint

    def post_cloud_watch(self, log_stream, payload):
        """Post to the opentrons-robot-logs log group on the Cloud Watch service

        @param log_stream {string} The name of the cloud watch log stream to post to.
        @param payload {dictionary} The payload structure from Quip, built in upload_protocol_run()
        @param endpoint {string} The endpoint for the AWS cloud watch service used.
        """

        try:
            if self._endpoint:
                header = {}
                header['log_group'] = 'opentrons-robot-logs'
                header['log_stream'] = log_stream
                header['payload'] = payload
                requests.post(self._endpoint, json=header)
                # TODO: add error handling of the response.

        except Exception:
            # No connection, no protocol handler so no way to log error.
            return


class AssetEventsDBHandler:
    """Library of useful functions for communicating protocol status to the cloud based DB"""

    VALID_POSTGREST_RESPONSES = [200, 201, 204]

    class RequestType(Enum):
        Post = auto()
        Get = auto()
        Patch = auto()

    devices: List[Device]
    _protocol_context: ProtocolContext
    _api_endpoint: str
    _auth: Tuple[str, str]

    def __init__(
        self,
        protocol_instructions,
        protocol_context: ProtocolContext,
        OT_name: OTName,
        credentials: PostgRESTCredentials,
    ):
        self._protocol_instructions = protocol_instructions
        self._metadata = protocol_instructions.get("metadata", {})
        self._protocol_title = self._metadata.get("name", "unnamed")
        self._OT_name = OT_name.str_name
        self.devices = []
        self._run_id = 0
        self._protocol_context = protocol_context
        self._api_endpoint = credentials.endpoint
        try:
            # If requests import fails, this should be ignored as we cant make requests anyway
            self._auth = requests.auth.HTTPBasicAuth(credentials.user, credentials.password)
        except Exception:
            pass


    @property
    def _system_id(self) -> int:
        success, robot = self._db_lookup_rows(
            "system", "name", Operator.equal, self._OT_name
        )
        if success and robot:
            return robot[0].get("id", 0)
        else:
            return 0

    @property
    def _instructions_id(self) -> str:
        metadata_protocol_id = self._metadata.get("protocol_id", 0)
        try:
            uuid_instructions_id = uuid.UUID(
                str(metadata_protocol_id), version=4
            )  # Check given id is uuid
            return str(uuid_instructions_id)
        except ValueError:
            return str(
                uuid.uuid3(
                    uuid.NAMESPACE_DNS,
                    f"{self._protocol_title} {self._protocol_author}",
                )
            )  # else return new uuid

    @property
    def _protocol_author(self) -> str:
        """
        Queries protocol instruction metadata for the created_by field and extracts the name by truncating the email.
        Returns "no_author" if failure along the way

        @return String Author's name
        """
        return self._metadata.get("created_by", "no_author").split("@")[0]

    @property
    def _protocol_id(self) -> Union[None, int]:
        """
        Checks if the instructions_id can be found in the instructions ID row column on the cloud database.
        If so, searches through each entry to check if any have the same instructions. If yes, return that protocol pkey "ID",
        else add a new entry.

        @return int primary key of the protocol (protocol_id) entry or None if something went wrong
        """
        success, protocols = self._db_lookup_rows(
            "protocol", "instructions_id", Operator.equal, self._instructions_id
        )
        if success and protocols:
            for protocol in protocols:
                if protocol.get('instructions') == self._protocol_instructions:
                    return protocol.get(getattr(SerialPrimaryKey, "protocol"))

        new_protocol = {
            "instructions_id": self._instructions_id,
            "name": self._protocol_title,
            "creator": self._protocol_author,
            "instructions": self._protocol_instructions,
        }
        return self._db_post_with_increment("protocol", new_protocol)

    @property
    def _device_ids(self) -> List[int]:
        device_ids = []
        for device in self.devices:
            device_ids.append(self.lookup_device(device))
        return device_ids

    @real_run_deco
    def db_init_run(self, start_time: TimeStamp) -> int:
        new_run = {
            "system_id": self._system_id,
            "device_id": self._device_ids,
            "protocol_id": self._protocol_id,
            "start_time": start_time.timestampz,
            "stop_time": start_time.timestampz,
        }

        self._run_id = self._db_post_with_increment("run", new_run)
        return self._run_id

    @real_run_deco
    def db_end_run(self, stop_time: TimeStamp):
        if not self._run_id:
            return  # Run failed to initialise on the DB
        update = {"stop_time": stop_time.timestampz}
        self._db_update_entry('run', update, "id", Operator.equal, self._run_id)

    @real_run_deco
    def db_add_event(
        self,
        event_type: EventType,
        event_data: Type[EventData] = None,
        event_time: TimeStamp = TimeStamp(),
    ):
        event_request = {
            "time": event_time.timestampz,
            "run_id": self._run_id or 0,
            "event_type": event_type.name,
        }

        event_id = self._db_post_with_increment("event", event_request)
        if not event_id:
            return  # No event_id, no point to continue

        if (
            event_type == EventType.ot_step
            and event_data
            and event_data.step_type != StepType.default
        ):
            try:
                device_id = (
                    self.lookup_device(event_data.device) if event_data.device else 0
                )
                ot_step = {
                    "event_id": event_id,
                    "device_id": device_id or 0,
                    "step_id": event_data.step_id,
                    "step_type": event_data.step_type.name,
                    "step_metadata": event_data.step_metadata,
                }
                self._db_send_request(self.RequestType.Post, "ot_step_event", ot_step)
            except Exception:
                self.db_add_error("Failed to Add Step to DB")

        elif event_type == EventType.error and event_data:
            try:
                error_msg = {"event_id": event_id, "message": event_data.message}
                self._db_send_request(self.RequestType.Post, "error_event", error_msg)
            except Exception:
                return

    @real_run_deco
    def db_add_error(self, error_message: str):
        self.db_add_event(EventType.error, EventData(str(error_message)))

    @real_run_deco
    def lookup_device(self, device: Device) -> Union[int, None]:
        """
        Lookup a device in the Device table based on the barcode.
        If cant find one, then add a new entry. Either way return the device table ID(pkey)

        @return Union[int, None]  The device ID, None if something went wrong accessing DB
        """

        if device.device_type == DeviceType.unknown:
            return 0

        success, devices = self._db_lookup_rows(
            "device", "barcode", Operator.equal, device.barcode
        )

        if success and devices:
            # Should only be 1 match, so always take the first in the list.
            return devices[0].get('id', 0)
        else:
            device = {
                "barcode": device.barcode,
                "name": device.name,
                "device_type": device.device_type.name,
            }
            return self._db_post_with_increment("device", device)

    @real_run_deco
    def _db_lookup_rows(
        self,
        table: str,
        field: str,
        operator: Operator,
        compare_value: Union[int, str],
    ) -> Tuple[bool, Union[None, List]]:
        """
        Forms a parameter using the provided field, compare_value and operator and runs a GET request using db_get_table()

        @return None if the request was illformed or database connection failed, else a list of Rows from the provided table matching the search.
        """

        parameters = [dict(key=field, value=f"{operator}.{compare_value}")]
        success, response = self._db_send_request(
            self.RequestType.Get, table, parameters=parameters
        )
        return success, response.json()

    @real_run_deco
    def _db_update_entry(
        self,
        table: str,
        update: dict,
        field: str,
        operator: Operator,
        compare_value: Union[int, str],
    ) -> bool:
        """
        Update an entry in the database based on a parameter match.
        """
        parameters = [dict(key=field, value=f"{operator}.{compare_value}")]
        success, _ = self._db_send_request(
            self.RequestType.Patch, table, body=update, parameters=parameters
        )
        return success

    @real_run_deco
    def _db_post_with_increment(self, table: str, row: dict) -> Union[None, int]:
        """
        Same as db_post() however row must not contain primary key
        @return int primary key of the new entry or None if something went wrong
        """
        try:
            pkey = getattr(SerialPrimaryKey, table)
            if row.get(pkey):
                # Don't use Increment function when passing primary key.
                return None

            header = {"Prefer": "return=headers-only"}
            success, response = self._db_send_request(
                self.RequestType.Post, table, body=row, headers=header
            )

            if not success:
                return None
            location = response.headers.get('location')
            if location:
                # trunk-ignore(flake8/W605)
                regex = re.search("eq\.(?P<ID>\d+)", location)
                if not regex:
                    return None
                return regex['ID']

        except AttributeError:
            self.db_add_error(f"Error: Cannot Find Primary Key for table: {table}")
            return None

    @real_run_deco
    def _db_send_request(
        self,
        request_type: RequestType,
        table: str,
        body: dict = {},
        headers: dict = {},
        parameters=[],
    ) -> Tuple[bool, Union[None, List]]:
        """
        Sends a request to the PostGREST API with a list of parameters, a header, and a json body

        @return (bool, Request) None if the request was illformed or database connection failed, else a list of Rows from the provided table.
        """

        request = f"{self._api_endpoint}/{str(table)}"
        if parameters:
            param = parameters[0]
            request += f"?{param['key']}={param['value']}"
            for param in parameters[1:]:
                request += f"&{param['key']}={param['value']}"
        try:
            if request_type == self.RequestType.Get:
                response = requests.get(request, auth=self._auth)
            elif request_type == self.RequestType.Post:
                response = requests.post(
                    request,
                    json=body,
                    headers=headers,
                    auth=self._auth,
                )
            elif request_type == self.RequestType.Patch:
                response = requests.patch(
                    request, json=body, headers=headers, auth=self._auth
                )
            else:
                return None

            if response.status_code in self.VALID_POSTGREST_RESPONSES:
                return True, response
            else:
                if table == "event" or table == "error_event":
                    return False, response  # Avoid cyclic calls
                self.db_add_error(
                    f"Error: Request failed: '{response.reason}' for call towards: '{table}' -- {response.content}"
                )
                # print(f"Error: Request failed: '{response.reason}' for call towards: '{table}' -- {response.content}")
                return False, response

        except Exception:
            return False, response
            # TODO Request Exception Handling - DO NOT USE db_add_error()... probably cyclic callback


class CommunicationsWrapper:
    """
    Class for cleanly initialising and passing around communications based classes
    Support for :
        - SlackHandler
        - CloudWatchHandler
        - AssetEventsDBHandler
        - OTName
    """

    _protocol_context: ProtocolContext
    _cloud_watch_handler: CloudWatchHandler
    _slack_handler: SlackHandler
    _asset_events_db_handler: AssetEventsDBHandler
    _OT_name: OTName
    _credentials: Credentials

    def __init__(
        self,
        protocol_instructions: dict,
        protocol_context: ProtocolContext,
        credentials_path: str,
    ) -> None:

        self._protocol_context = protocol_context
        self._credentials_path = credentials_path
        self._credentials = self.load_credentials()

        self._cloud_watch_handler = CloudWatchHandler(self._credentials.cloud_watch_ep)
        protocol_title = protocol_instructions.get("metadata", {}).get(
            "name", "unnamed"
        )
        self._OT_name = OTName()
        self._slack_handler = SlackHandler(
            protocol_title,
            self._OT_name,
            self._credentials.slack_log_webhook,
            self._credentials.slack_message_webhook,
        )

        self._asset_events_db_handler = AssetEventsDBHandler(
            protocol_instructions,
            protocol_context,
            self._OT_name,
            self.build_postgREST_credentials(),
        )

    @property
    def cloud_watch_handler(self):
        return self._cloud_watch_handler

    @property
    def slack_handler(self):
        return self._slack_handler

    @property
    def asset_events_db_handler(self):
        return self._asset_events_db_handler

    @property
    def OT_str_name(self):
        return self._OT_name.str_name

    def load_credentials(self) -> Credentials:
        """
        Trys to find credentials.json file in the self._credentials_path absolute location
        and loads the credentials from it. Empty Strings if not.

        Not that by default credentials path location on OT is OT_CREDENTIALS_PATH = "/data/credentials.json".
        This definition can be found in the global definitions file.

        @return Credentials A class "struct" with retrieved credentials or ""
        """

        credentials = Credentials()
        credentials_json = {}

        if os.path.isfile(self._credentials_path):
            with open(self._credentials_path) as f:
                credentials_json = json.load(f)
        else:
            return credentials

        # load credentials from file into Credentials Class
        attributes = [a for a in dir(credentials) if not a.startswith("__")]

        for credential in attributes:
            setattr(credentials, credential, credentials_json.get(credential, ""))
        return credentials

    def build_postgREST_credentials(self) -> PostgRESTCredentials:
        """
        Build an object containing PostgREST Authentication for use in all requests
        """

        return PostgRESTCredentials(
            endpoint=self._credentials.postgrest_api_ep,
            user=self._credentials.postgrest_user,
            password=self._credentials.postgrest_pw,
        )
