# Code Lint Header
import time
from dataclasses import dataclass, field
from enum import Enum, auto

from opentrons import types

# End Code Lint Header


def real_run_deco(f):
    """
    Functions tagged with this decorator will only run if the protocol is run on an OT...Not simulating
    """

    def wrapper(*args, **kwargs):
        global IS_SIMULATING
        if not IS_SIMULATING:
            try:
                return f(*args, **kwargs)
            except Exception:
                pass
        else:
            return

    return wrapper


@dataclass
class Operator:
    equal: str = "eq"
    gte: str = "gte"
    lte: str = "lte"
    gt: str = "gt"
    lt: str = "lt"


@dataclass
class SerialPrimaryKey:
    run: str = "id"
    device: str = "id"
    event: str = "id"
    protocol: str = "id"
    system: str = "id"


class DeviceType(Enum):
    unknown = auto()
    p300_v1 = auto()
    p50_v1 = auto()
    p300_v2 = auto()
    p50_v2 = auto()
    m300_v2 = auto()
    m20_v2 = auto()
    bioshake_3000t = auto()


class EventType(Enum):
    ot_preflight_check = auto()
    ot_postflight_check = auto()
    ot_step = auto()
    error = auto()


class StepType(Enum):
    default = auto()
    simple_transfer = auto()
    sequence_transfer = auto()
    array_transfer = auto()
    shake = auto()
    pause = auto()
    loop = auto()


@dataclass
class Device:
    barcode: str = ""
    name: str = ""
    device_type: DeviceType = DeviceType.unknown


class TimeStamp:
    """
    Helper class for working with PSQL Timestamps
    """

    # DEFAULT_LOCATION = "Europe/Stockholm"
    DEFAULT_LOCATION = "UTC"

    def __init__(self, epoch_time: int = None) -> None:
        self._epoch_time = epoch_time or time.time()
        self._epoch_time = int(self._epoch_time)  # Drop milliseconds
        self._location = self.DEFAULT_LOCATION

    def __sub__(self, other):
        return self.epoch_time - other.epoch_time

    def __add__(self, other):
        return self.epoch_time + other.epoch_time

    @property
    def timestampz(self):
        # Example 2022-02-24 11:10:15 Australia/Sydney"
        time_struct = time.localtime(self._epoch_time)
        return f"{time.strftime('%Y-%m-%d %H:%M:%S', time_struct)} {self._location}"

    @property
    def epoch_time(self):
        return self._epoch_time

    @epoch_time.setter
    def epoch_time(self, time):
        self._epoch_time = int(time)  # Drop milliseconds

    @property
    def location(self):
        return self._location

    @location.setter
    def location(self, location):
        self._location = location  # Drop milliseconds

    @property
    def elapsed_time(self) -> str:
        """ "
        Formatted elapsed time based on 'epoch_time'
        """
        return '{:02d}h {:02d}m {:02d}s'.format(
            self._epoch_time // 3600,
            (self._epoch_time % 3600 // 60),
            self._epoch_time % 60,
        )


@dataclass
class EventData:
    message: str

    def __init__(self, message: str = "") -> None:
        self.message = message


@dataclass
class StepEvent(EventData):
    def __init__(self) -> None:
        EventData.__init__(self, "")

    device: Device = None
    step_id: int = None
    step_type: StepType = StepType.default
    step_metadata: dict = field(default_factory=dict)


@dataclass
class PipetteModelRegex:
    p300_v1: str = "(?=.*p300)(?=.*single)(?=.*v1).*"
    p50_v1: str = "(?=.*p50)(?=.*single)(?=.*v2).*"
    p300_v2: str = "(?=.*p300)(?=.*single)(?=.*v2).*"
    p50_v2: str = "(?=.*p50)(?=.*single)(?=.*v2).*"
    m300_v2: str = "(?=.*p300)(?=.*multi)(?=.*v2).*"
    m20_v2: str = "(?=.*p20)(?=.*multi)(?=.*v2).*"
    # None = "(?!x)x"


@dataclass
class Credentials:
    slack_log_webhook: str = ""
    cloud_watch_ep: str = ""
    slack_message_webhook: str = ""
    postgrest_api_ep: str = ""
    postgrest_user: str = ""
    postgrest_pw: str = ""


@dataclass
class PostgRESTCredentials:
    endpoint: str = ""
    user: str = ""
    password: str = ""


@dataclass
class TipMaxLiquidHeights:
    """
    Stores the liquid height in tip when full in mm for various different types of tips.
    Must be measured in the lab.
    """

    enginzyme_96_tiprack_300ul: int = 15
    # Support for deprecated tip_racks appear in older protocols.
    opentrons_96_tiprack_300ul: int = 15


class WellLocations:
    """
    Mainly used for the shake_tip function, this class stores relative points on a well.
    """

    middle: types.Point = types.Point(x=0, y=0, z=-3)
    top = types.Point(x=0, y=2, z=-3)
    bottom = types.Point(x=0, y=-2, z=-3)
    left = types.Point(x=-2, y=0, z=-3)
    right = types.Point(x=2, y=0, z=-3)
