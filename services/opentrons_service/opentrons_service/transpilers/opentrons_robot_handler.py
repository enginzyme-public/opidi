# Code Lint Header
import re
import sys
from typing import List, Sequence, Union

import serial
from bioshake_3000T import Shaker
from global_definitions import BAUDRATE, SERIALPORT, TIMEOUT
from opentrons import protocol_api, types
from opentrons.protocol_api import InstrumentContext, ProtocolContext
from opentrons_communications_handlers import (
    AssetEventsDBHandler,
    CommunicationsWrapper,
    SlackHandler,
)
from opentrons_models import (
    Device,
    DeviceType,
    EventType,
    PipetteModelRegex,
    StepEvent,
    StepType,
    real_run_deco,
)
from opentrons_transpiler import match_sequences
from transfer_handler import AdvancedLiquidHandling, LiquidClass, TransferHandler

# End Code Lint Header
class TipsWatchdog:
    """
    Used to manage Opentrons tips. Contains functions to pick up, discard, etc.
    """

    def __init__(self, pipette, tip_strategy):
        """
        Initialise the pipettes and check the tip strategy. Since this class is reinitialised
        at the start of every pipette step, it should pick up a tip if it does not have one.
        If mode is ECO - new tip, drop the current tip and take a new one.

        Tip Strategies:
        - ECO - Take a tip if needed and reuse it without ever dropping it
        - ECO - NEW TIP - Take a new tip only at the start of each new pipette step
        - STRICT - New tip for every transfer
        - STANDARD - New tip per source

        @param pipette The pipette object to manage.
        @param tip_strategy {string} The Strategy to use for managing tips
        """
        # The active pipette
        self.pipette = pipette
        # The Tip Strategy used for this step
        self.strategy = tip_strategy
        # The source labware for this step
        self.source_check = None

        if self.strategy == "ECO":
            if not self.pipette.hw_pipette['has_tip']:
                self.pipette.pick_up_tip()

        if self.strategy == "ECO - NEW TIP":
            if self.pipette.hw_pipette['has_tip']:
                self.pipette.drop_tip()
            self.pipette.pick_up_tip()

    def change_tip(self, source=None):
        """
        Run before every action, check which tip strategy and if not Eco, change the tip.

        @param source From which labware the current action should source liquid
        """
        if self.strategy == "STRICT":
            if self.pipette.hw_pipette['has_tip']:
                self.pipette.drop_tip()
            self.pipette.pick_up_tip()

        if self.strategy == "STANDARD":
            if source != self.source_check:
                if self.pipette.hw_pipette['has_tip']:
                    self.pipette.drop_tip()
                self.pipette.pick_up_tip()
                self.source_check = source

    def discard(self):
        """
        Run at the end of each step, checks which tip strategy is being used.
        If not an Eco mode, will drop the current tip.
        """
        if self.strategy != "ECO" and self.strategy != "ECO - NEW TIP":
            if self.pipette.hw_pipette['has_tip']:
                self.pipette.drop_tip()


class RobotHandler:
    """
    Class used for basic opentrons commands.
    """

    _devices: List[Device]
    _protocol_context: ProtocolContext
    _slack_handler: SlackHandler
    _asset_events_db_handler: AssetEventsDBHandler

    def __init__(
        self,
        protocol_context: ProtocolContext,
        protocol_instructions: dict,
        communications_wrapper: CommunicationsWrapper,
    ):
        """
        Initialise the class members, extract the slack log webhook, and assign the labware,
        trash bin, and pipettes to their relative positions

        @param protocol The instance of the protocol API context
        @param protocol_instructions The robot instructions extracted from the input json file
        """
        # The active protocol context
        self._protocol_context = protocol_context
        # Robot instructions from JSON
        self._protocol_instructions = protocol_instructions
        self.assign_slots()
        self.assign_trash()
        self.assign_pipettes()
        self._slack_handler = communications_wrapper.slack_handler
        self._asset_events_db_handler = communications_wrapper.asset_events_db_handler
        self._devices = []
        self._get_devices()

    @property
    def devices(self):
        return self._devices

    def assign_slots(self):
        """
        Iterate over all deck slots in the instructions file and load them into the protocol context
        """
        for slot in self._protocol_instructions['deck']['slots']:
            slot_content = self._protocol_instructions['deck']['slots'][slot]
            if slot_content:
                element = self._protocol_context.load_labware(
                    slot_content['labware_id'], slot
                )
                setattr(
                    self, slot_content['name'], element
                )  # User Interface has to make sure that names don't contain space and don't end with a number

    def assign_trash(self):
        """
        Assign the trash in the protocol
        """
        setattr(self, 'trash', self._protocol_context.fixed_trash)

    def assign_pipettes(self):
        """
        Iterate over the pipettes in the instructions file and load them
        as instruments into the protocol context. Also load their respective tip racks
        """
        if self._protocol_instructions['deck']['left_pipette']['pipette']:
            tipracks_slots = self._protocol_instructions['deck']['left_pipette'][
                'tipracks'
            ]
            tipracks_names = [
                self._protocol_instructions['deck']['slots'][str(key)]['name']
                for key in tipracks_slots
            ]
            tipracks = [getattr(self, x) for x in tipracks_names]

            # The left pipette of the Opentrons
            self.left_pipette = self._protocol_context.load_instrument(
                self._protocol_instructions['deck']['left_pipette']['pipette']['id'],
                'left',
                tip_racks=tipracks,
            )

        if self._protocol_instructions['deck']['right_pipette']['pipette']:
            tipracks_slots = self._protocol_instructions['deck']['right_pipette'][
                'tipracks'
            ]
            tipracks_names = [
                self._protocol_instructions['deck']['slots'][str(key)]['name']
                for key in tipracks_slots
            ]
            tipracks = [getattr(self, x) for x in tipracks_names]
            # The right pipette of the Opentrons
            self.right_pipette = self._protocol_context.load_instrument(
                self._protocol_instructions['deck']['right_pipette']['pipette']['id'],
                'right',
                tip_racks=tipracks,
            )

    def get_wells(self, sources):
        """
        Get the Well locations for each source labware.

        @param sources {list} A list of the labware used as sources for the current step
        @return locations {list} A list of well locations for the given labware sources
        """
        if not isinstance(sources, list):
            sources = [sources]

        locations = []
        for slot in sources:
            element = getattr(self, slot['slot_name'])
            locations += [element.wells_by_name()[x] for x in slot['wells']]

        return locations

    def apply_offsets(self, wells, offset_type, offset_value):
        """
        Allows for the option to specify an offset for the aspirate/dispense function.

        The offset type specifies where on the well the offset should start from.
        Offset types are currently Bottom and Top.

        @param wells {list} A list of well locations for the given labware sources for which to apply the offset
        @param offset_type {string} The location on the well were the offset should originate from
        @param offset_value {float} The offset value to apply to the wells

        @return The list of wells updated with offsets
        """
        if not isinstance(wells, list):
            wells = [wells]

        if offset_type == "Bottom":
            wells = [well.bottom(float(offset_value)) for well in wells]
        else:
            wells = [well.top(float(offset_value)) for well in wells]

        return wells

    def update_flow_rates(self, pipette: InstrumentContext, liquid_class: LiquidClass):
        try:
            pipette.flow_rate.aspirate = liquid_class.calc_rate("aspirate")
            pipette.flow_rate.dispense = liquid_class.calc_rate("dispense")
            pipette.flow_rate.blow_out = liquid_class.calc_rate("blow_out")
        except AssertionError:
            _, exc_value, _ = sys.exc_info()
            self._slack_handler.send_slack_message(exc_value)

    def simple_pipetting(self, parameters):
        """
        Stores the logic for a simple pipette step. Will prepare the step based on sources,
        offsets, destinations etc... Then run through each action transferring liquid.
        @param parameters {dictionary} The parameters for the step
        """
        pipette = getattr(self, parameters['pipette'])
        tips = TipsWatchdog(pipette, parameters['tips_strategy'])
        post_transfer_blowout = False
        volumes = parameters['volumes']

        liquid_class = LiquidClass(
            self._protocol_instructions["liquid_classes"][parameters['liquid_class']],
            pipette,
            self._slack_handler,
        )

        if "source" in parameters:
            sources = self.get_wells(parameters['source'])
            destinations = self.get_wells(parameters['destination'])

        if "source_sequence" in parameters:
            srcs = self._protocol_instructions['sequences'][
                parameters['source_sequence']
            ]
            sources = self.get_wells(srcs)
            dsts = self._protocol_instructions['sequences'][
                parameters['destination_sequence']
            ]
            destinations = self.get_wells(dsts)

        if "offset" in parameters:
            sources = self.apply_offsets(
                sources,
                parameters['offset']['source_type'],
                parameters['offset']['source'],
            )
            destinations = self.apply_offsets(
                destinations,
                parameters['offset']['destination_type'],
                parameters['offset']['destination'],
            )

        self.update_flow_rates(pipette, liquid_class)

        matched_iterator = match_sequences(
            volumes, sources, destinations
        )  # User Interface has to make sure that names don't contain space and don't end with a number

        if liquid_class.touch_tip & liquid_class.blow_out:
            # Do blow out after the transfer has completed after centering tip head CU#2khm3j1
            liquid_class.blow_out = False
            post_transfer_blowout = True

        vol: Union[float, Sequence[float]]
        src: AdvancedLiquidHandling
        dst: AdvancedLiquidHandling
        for vol, src, dst in zip(*matched_iterator):
            tips.change_tip(source=src)

            transfer_handler = TransferHandler(
                pipette, liquid_class, self._protocol_context
            )
            transfer_handler.load_transfer_params(vol, src, dst)
            transfer_handler.transfer(post_transfer_blowout=post_transfer_blowout)

        tips.discard()

    def array_pipetting(self, parameters):
        """
        Stores the logic for an array pipette step. Similar to simple_pipetting() except using an array map as input.
        @param parameters {dictionary} The parameters for the step
        """
        pipette = getattr(self, parameters['pipette'])
        tips = TipsWatchdog(pipette, parameters['tips_strategy'])
        liquid_class = LiquidClass(
            self._protocol_instructions["liquid_classes"][parameters['liquid_class']],
            pipette,
            self._slack_handler,
        )

        for line in parameters['array_map']:

            source_slot, source_well, dest_slot, dest_well, volume = line

            src = self.get_wells({'slot_name': source_slot, 'wells': [source_well]})
            dst = self.get_wells({'slot_name': dest_slot, 'wells': [dest_well]})

            if "offset" in parameters:
                src = self.apply_offsets(
                    src,
                    parameters['offset']['source_type'],
                    parameters['offset']['source'],
                )
                dst = self.apply_offsets(
                    dst,
                    parameters['offset']['destination_type'],
                    parameters['offset']['destination'],
                )

            self.update_flow_rates(pipette, liquid_class)
            tips.change_tip(source=src)

            transfer_handler = TransferHandler(
                pipette, liquid_class, self._protocol_context
            )
            transfer_handler.load_transfer_params(int(volume), src, dst)
            transfer_handler.transfer()

        tips.discard()

    def shake(self, parameters):
        """
        Stores the logic for a simple shaker step utilising the BioShake 3000t.
        Only runs during the actual protocol since a simulation would not have access to the serial ports.
        Sets the temperature and RPM setpoint and starts the system. If users set "wait_for_stop" as an
        option, the code will block until the shaker step has completed (indicated by the shaker "homing").
        Either way will stop and cool-down the shaker post completion.

        @param parameters The parameters for the step

        """

        self.custom_home(only_waste=True)

        if not self._protocol_context.is_simulating():
            with serial.Serial(SERIALPORT, BAUDRATE, timeout=TIMEOUT) as ser:
                shaker = Shaker(ser)

                if parameters['temp_control']:
                    shaker.start_temp_control(int(parameters['setpoint']))

                if parameters['speed'] and parameters['duration']:
                    shaker.start_run(parameters['speed'], parameters['duration'])

                if parameters['wait_for_stop']:
                    while not shaker.is_shaker_home():
                        print(shaker.get_responses())

                if parameters['force_stop']:
                    shaker.stop()

                if parameters['cooldown']:
                    shaker.stop_temp_control()

    def custom_home(self, only_waste=False, drop_tips=False):
        """
        Custom code for homing the gantry to ensure that the pipette tips are raised
        and will not hit any custom labware. Also has the option to drop the tips at the end of the home

        @param drop_tips {boolean} If True, drop the tips on all loaded pipettes given that they have tips loaded.
        """

        centre_waste = types.Location(types.Point(350, 350, 120), None)
        if hasattr(self, 'right_pipette'):
            self.right_pipette.move_to(centre_waste, minimum_z_height=100)
        if hasattr(self, 'left_pipette'):
            self.left_pipette.move_to(centre_waste, minimum_z_height=100)
        else:
            print("Error, no pipette defined for homing.")

        if drop_tips:
            if hasattr(self, 'right_pipette'):
                tips = TipsWatchdog(self.right_pipette, 'STANDARD')
                tips.discard()
            if hasattr(self, 'left_pipette'):
                tips = TipsWatchdog(self.left_pipette, 'STANDARD')
                tips.discard()

        if only_waste:
            # option to skip the official homing step CU#2v7pcyg
            return
        self._protocol_context.home()

    def reset_tipracks(self, tips_to_refill_json):
        tips_to_refill: List[int] = []
        for racks in list(tips_to_refill_json.values()):
            tips_to_refill.extend(racks)
        if not tips_to_refill:
            return

        message = f"Please restock tipracks: {tips_to_refill}"
        self._protocol_context.comment(message)
        self._slack_handler.send_slack_message(message)
        self._protocol_context.pause()

        pipette_sides = ["left", "right"]
        for side in pipette_sides:
            try:
                pipette: InstrumentContext = getattr(self, f"{side}_pipette")
                for tiprack in pipette.tip_racks:
                    if int(tiprack.parent) in tips_to_refill:
                        tiprack.reset()
            except Exception:
                continue

    def execute_step(self, step):
        """
        Takes the input step and processes it by calling the respective function based on step type and parameters present

        @param step {dictionary} The step to be processed
        """
        step_index_position = '.'.join(str(index + 1) for index in step['indexes']) + '.'

        self._protocol_context.comment(f"{step_index_position} {step['name']}")
        self._log_step_db(step)

        if step['type'] == "simple_transfer" or step['type'] == "sequence_transfer":
            self.simple_pipetting(step['parameters'])

        elif step['type'] == "array_transfer":
            self.array_pipetting(step['parameters'])

        elif step['type'] == 'loop':
            for i in range(1, step['num_iterations'] + 1):
                self._protocol_context.comment(
                   f"{step_index_position} {step['name']} - Iteration {str(i)}"
                )
                for substep in step['substeps']:
                    self.execute_step(substep)

        elif step['type'] == 'pause':

            self.custom_home(only_waste=True)

            if step['auto_resume']:
                self._protocol_context.delay(step['pause_time'])
            else:
                self._protocol_context.pause()
                self._slack_handler.send_log_message(
                    "INFO: Protocol Paused - Awaiting User Input"
                )

        elif step['type'] == 'bioshake_3000t':
            self.shake(step['parameters'])

        elif step['type'] == 'slack_message':
            self._slack_handler.send_slack_message(step['message'])

        elif step['type'] == 'restock_tips':
            self.reset_tipracks(step["tips_to_refill"])

        else:
            self._protocol_context.comment(
                '\n------------Step type: \n' + step['type'] + ' is unknown!!!\n'
            )

    def _get_devices(self) -> None:
        """
        Get devices for all loaded pipettes and the shaker if present in the protocol. Will populate the _devices
        list member with any devices found that match the criteria.
        """

        pipettes = self._get_pipettes()
        for p in pipettes:
            # https://github.com/Opentrons/opentrons/blob/edge/api/src/opentrons/hardware_control/dev_types.py
            pipette_id = p.hw_pipette.get("pipette_id", None)
            if pipette_id:
                pipette = Device()
                pipette.barcode = pipette_id
                pipette.name = p.hw_pipette.get("name", "")
                # Pipette Models https://github.com/Opentrons/opentrons/blob/edge/shared-data/pipette/definitions/pipetteModelSpecs.json
                pipette_model = p.hw_pipette.get("model", "")
                pipette.device_type = self._get_pipette_type(pipette_model)
                self._devices.append(pipette)

        # If shaker is present get its Device ID.
        slots = self._protocol_instructions['deck'].get("slots", {})
        shaker_labware = [
            obj
            for obj in slots.items()
            if obj[1].get('labware_type') == 'shakerLabware'
        ]
        if shaker_labware:
            shaker_id = self._get_shaker_id()
            if shaker_id:
                shaker = Device()
                shaker.barcode = shaker_id
                shaker.name = "Q.MTP-BIOSHAKE 3000-T"
                shaker.device_type = DeviceType.bioshake_3000t
                self._devices.append(shaker)

    def _get_step_device(self, step: dict) -> Device:
        step_type = step.get("type", "")
        if 'transfer' in step_type:
            pipette = getattr(self, step['parameters']['pipette'])
            barcode = pipette.hw_pipette.get("pipette_id", None)
            device = [device for device in self._devices if device.barcode == barcode]
            try:
                # Should only be 1 pipette with the same barcode.
                device = device[0]
            except IndexError:
                device = Device()
            return device

        elif 'shake' in step_type:
            device = [
                device
                for device in self._devices
                if device.device_type == DeviceType.bioshake_3000t
            ]
            try:
                device = device[0]  # Only support for one shaker so far
            except IndexError:
                device = Device()
            return device

        else:
            return Device()

    @real_run_deco
    def _log_step_db(self, step: dict):

        step_event = StepEvent()
        step_event.device = self._get_step_device(step)
        step_event.step_id = step.get("id", 0)
        step_event.step_type = getattr(StepType, step.get("type", ""), StepType.default)
        # Temporary Fix since Shake step is BioShake. . .
        if step.get("type", "") == "bioshake_3000t":
            step_event.step_type = getattr(StepType, "shake", StepType.default)
        step_event.step_metadata = step
        self._asset_events_db_handler.db_add_event(EventType.ot_step, step_event)

    def _get_pipette_type(self, pipette_model: str) -> DeviceType:
        if pipette_model == "":
            return DeviceType.unknown

        else:
            regex_attr = [a for a in dir(PipetteModelRegex) if not a.startswith('__')]
            regex_list = [getattr(PipetteModelRegex, a) for a in regex_attr]
            for i, regex in enumerate(regex_list):
                result = re.search(regex, pipette_model)
                if result:
                    try:
                        return DeviceType[regex_attr[i]]
                    except Exception:
                        return (
                            DeviceType.unknown
                        )  # PipetteModelRegex wrong member definition

            return DeviceType.unknown  # No Matches

    def _get_pipettes(self) -> List[protocol_api.InstrumentContext]:
        """
        returns the left and right pipettes if loaded

        @return List of InstrumentContext matching the loaded pipettes. List may be empty if no pipettes loaded.
        """
        pipettes = []
        if hasattr(self, "left_pipette"):
            pipettes.append(self.left_pipette)
        if hasattr(self, "right_pipette"):
            pipettes.append(self.right_pipette)
        return pipettes

    def _get_shaker_id(self) -> Union[int, None]:
        """
        Fetches and returns the ID of the loaded shaker. "shaker.get_id()" may return None if ID not found

        @return Shaker ID or None if ID not found
        """

        if not self._protocol_context.is_simulating():
            with serial.Serial(SERIALPORT, BAUDRATE, timeout=TIMEOUT) as ser:
                shaker = Shaker(ser)
                return shaker.get_id()
        return None
