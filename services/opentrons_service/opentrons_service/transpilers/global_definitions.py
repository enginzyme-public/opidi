import itertools
import re
try:
    import socket
    import requests
except Exception:
    pass

import sys
import os
import time
import traceback
from dataclasses import dataclass, field
from typing import List, Union, Tuple, Type, Sequence
from enum import Enum, auto
import json
import uuid
import math
import serial
from opentrons import protocol_api, types
from opentrons.protocol_api import ProtocolContext, InstrumentContext, labware
from opentrons_shared_data.pipette import fuse_specs, model_config, name_config


# General Settings for the protocol

# Flag set once and provided globally to functions that can only run code when not is_simulating
IS_SIMULATING = True
# Current version of this transpiler
TRANSPILER_VERSION = "2.5.0"

# Baud rate for Serial communication with Shaker
BAUDRATE = 9600
# Shaker Serial Port Settings
SERIALPORT = '/dev/serial/by-path/platform-3f980000.usb-usb-0:1.5:1.0-port0'
# Time of no response before serial com error is thrown
TIMEOUT = 1

OT_CREDENTIALS_PATH = "/data/credentials.json"
