import re

import requests

API_ENDPOINT = '<YOUR_DB_API_ENDPOINT>'


# Post a row to a table
ots = {"id": 2, "name": "OT-12 Isabella", "system_type": "opentrons_ot2"}
ots = {"id": 0, "name": "Unknown", "system_type": "unknown"}
post = requests.post("API_KEY/system", json=ots)
# post = <Response [201]>

# Multiple Rows at once
ots = [
    {"id": "3", "name": "OT-19 Minerva", "system_type": "opentrons_ot2"},
    {"id": "4", "name": "OT-05 Hercules", "system_type": "opentrons_ot2"},
]
post = requests.post("API_ENDPOINT/system", json=ots)

# View whole Table
r = requests.get("API_ENDPOINT/system")
for item in r.json():
    print(item)
# {'id': 1, 'name': 'OT-07 CERES', 'system_type': 'opentrons_ot2'}
# {'id': 2, 'name': 'OT-12 Isabella', 'system_type': 'opentrons_ot2'}
# {'id': 3, 'name': 'OT-19 Minerva', 'system_type': 'opentrons_ot2'}
# {'id': 4, 'name': 'OT-05 Hercules', 'system_type': 'opentrons_ot2'}


# Add row allowing pkey to increment
headers = {"Prefer": "return=headers-only"}
body = {
    "system_id": 0,
    "device_id": [],
    "protocol_id": 0,
    "start_time": "2022-02-27 22:00:00 Australia/Sydney",
    "stop_time": "2022-02-27 22:00:00 Australia/Sydney",
}

reply = requests.post(
    "API_ENDPOINT/run",
    headers=headers,
    json=body,
)
# trunk-ignore(flake8/W605)
regex = re.search("eq\.(?P<ID>\d+)", reply.headers.get("location"))
regex["ID"]  # = '2'


###############   PSQL QUERIES ###################
# #Reset DB
# delete from api.protocol where id in (2,3,4,5)
# delete from api.protocol where id > 1

# # Reset Index
# ALTER SEQUENCE api.run_id_seq RESTART WITH 1;
