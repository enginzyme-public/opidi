import json
import os
import sys

"""Opentrons Generator Functions

Contains functions for all of the protocol generation. The main purpose is to take an Instructions json file
and concatenate the transpiler code thus outputting a protocol that can be run on the Opentrons
"""
#!/usr/bin/env python
# -*- coding: utf-8 -*-

sys.path.append(os.path.dirname(__file__))

# The path of this file for use in absolute paths for transpilers
dir_path = os.path.dirname(os.path.realpath(__file__))
# The path for all transpilers
transpilers_path = dir_path + "/transpilers"

# Contains the Global Imports and Definitions
opentrons_global_definitions = 'global_definitions.py'
# The main transpiler loop
opentrons_transpiler = 'opentrons_transpiler.py'
# Protocol Handler classes
opentrons_protocol_handler = 'opentrons_protocol_handler.py'
# Robot Handler and Tip Watchdog classes
opentrons_robot_handler = 'opentrons_robot_handler.py'
# QInstruments BioShake 3000T custom class
bioshake_3000T = 'bioshake_3000T.py'
# Data Models mainly for Communications Class
opentrons_models = 'opentrons_models.py'
# All Communications classes for Slack, CloudWatch, CloudDB
opentrons_communications_handlers = 'opentrons_communications_handlers.py'
# Liquid Class Definition
transfer_handler_file = 'transfer_handler.py'


def pretty_print(data):
    """Used to replace a few incompatible phrases in the instructions file

    @param data The instruction raw text to be replaces
    @return output The formatted instruction text compatible with Opentrons
    """
    output = json.dumps(data, indent=4)
    output = output.replace('null', 'None')
    output = output.replace('false', 'False')
    output = output.replace('true', 'True')
    return output


def load_transpiler_file(file_name):
    """Used to load a transpiler file so as to concatenate it later.

    @param file_name The name of the file to be grabbed from the transpiler directory (default: opentrons_service/transpilers)
    @return file_data The loaded transpiler text
    """
    bool_code_lint_header = False
    file_data = []
    with open(os.path.join(transpilers_path, file_name)) as f:
        transpiler_file = f.readlines()

    for line in transpiler_file:
        if "End Code Lint Header" in line:
            bool_code_lint_header = False
            continue
        elif "Code Lint Header" in line:
            bool_code_lint_header = True

        # Remove any code within "Code Lint Header" comment guards as it is there
        # to allow linting during transpiler development.
        if not bool_code_lint_header:
            file_data.append(line)

    file_data = ''.join(file_data)
    return file_data


def parse_lists_to_objects(protocol_instructions):
    """Used to parse the robot instructions file into Opentrons compatible objects

    It is more convenient for our front end to use a list of objects for certain parameters
    in the instructions file (i.e. Liquid Classes). However this is not what is expected by
    the Opentrons. Thus we should parse such objects, remove front-end only fields and add
    any back-end only fields.

    @param protocol_instructions The robot instructions
    @return protocol_instructions The parsed set of robot instructions
    """
    if isinstance(protocol_instructions["sequences"], list):
        parsedSequences = {}

        for element in protocol_instructions["sequences"]:
            parsedSequences[element['name']] = element["locations"]
        protocol_instructions["sequences"] = parsedSequences

    if isinstance(protocol_instructions["liquid_classes"], list):
        parsedClasses = {}

        for element in protocol_instructions["liquid_classes"]:

            element["liquid_config_object"]["new_tip"] = "never"
            mix_before_element = element["liquid_config_object"]["mix_before"]

            if mix_before_element["repetitions"]:
                element["liquid_config_object"]["mix_before"] = (
                    mix_before_element["repetitions"],
                    mix_before_element["volume"],
                )
            else:
                element["liquid_config_object"]["mix_before"] = None

            mix_after_element = element["liquid_config_object"]["mix_after"]

            if mix_after_element["repetitions"]:
                element["liquid_config_object"]["mix_after"] = (
                    mix_after_element["repetitions"],
                    mix_after_element["volume"],
                )
            else:
                element["liquid_config_object"]["mix_after"] = None

            parsedClasses[element['name']] = element["liquid_config_object"]

        protocol_instructions["liquid_classes"] = parsedClasses

    return protocol_instructions


def generate_protocol_string(protocol_instructions):
    """Used to generate the protocol

    Simply takes the robot instructions and concatenates them with all the required transpilers.

    @param protocol_instructions The robot instructions to add to the protocol
    @return protocol The concatenated protocol ready to be saved and executed on an Opentrons
    """

    protocol_instructions = parse_lists_to_objects(protocol_instructions)
    protocol_instructions = pretty_print(protocol_instructions)
    instructions = ''.join(["protocol_instructions=", protocol_instructions])

    global_definitions = load_transpiler_file(opentrons_global_definitions)
    transpiler_main = load_transpiler_file(opentrons_transpiler)
    protocol_handler = load_transpiler_file(opentrons_protocol_handler)
    robot_handler = load_transpiler_file(opentrons_robot_handler)
    shaker_modules = load_transpiler_file(bioshake_3000T)
    data_models = load_transpiler_file(opentrons_models)
    communications_handlers = load_transpiler_file(opentrons_communications_handlers)
    transfer_handler = load_transpiler_file(transfer_handler_file)

    protocol = ''.join(
        [
            instructions,
            '\n',
            global_definitions,
            '\n',
            data_models,
            '\n',
            communications_handlers,
            '\n',
            transfer_handler,
            '\n',
            transpiler_main,
            '\n',
            robot_handler,
            '\n',
            protocol_handler,
            '\n',
            shaker_modules,
        ]
    )
    return protocol
