\label{index_md_README}%
\Hypertarget{index_md_README}%
 The Opentrons Protocol Generator is designed first and foremost to parametrize the generation of Opentrons protocol files. The python files are created from a set of instructions in a JSON file following a very specific schema.

It is built with support for Opentrons API v2 and uses heavily the native \href{https://docs.opentrons.com/v2/\#}{\texttt{ OT-\/2 API V2}}

More detailed documentation can be found \href{https://enginzyme.gitlab.io/opentrons/opidi/}{\texttt{ here}}\hypertarget{index_autotoc_md1}{}\doxysection{Design Considerations}\label{index_autotoc_md1}
The generator was built as part of a custom protocol designer webapp from which users can define an instructions file and then generate, fetch, simulate and download the protocol.

Protocol python files are then uploaded to the robot using the \href{https://opentrons.com/ot-app/}{\texttt{ Opentrons App}}.

To change the robot configuration and execution from an instructions file, the file contents are converted to a python dictionary upon protocol generation. The rest of the file is composed of handlers that define pipettes, labware, tip change behavior, liquid steps and custom defined steps (external hardware integration, slack notifications, etc.)


\begin{DoxyInlineImage}
\includegraphics[height=\baselineskip,keepaspectratio=true]{OPG_High_Level_Architecture.jpg}%High Level System Design
\end{DoxyInlineImage}
\hypertarget{index_autotoc_md2}{}\doxysubsection{Generator / Transpiler}\label{index_autotoc_md2}
Since the app is designed to upload a single python file every handler is appended to the protocol by a main transpiler script.

The {\ttfamily opentrons\+\_\+generator.\+py} transpiles the instructions JSON file and a bunch of static transpiler templates is a static file that contains the code the robot will run through when a protocol is begun.


\begin{DoxyItemize}
\item Main Transpiler
\item Opentrons Protocols Helper
\item Opentrons Robot Handler
\item External Modules
\end{DoxyItemize}

The schemas of the instructions JSON file are outlined for each attribute in {\ttfamily opentrons\+\_\+service/schemas}.

The main transpiler {\ttfamily opentrons\+\_\+transpiler.\+py} and the robot handler {\ttfamily opentrons\+\_\+robot\+\_\+handler.\+py} files rely on the Opentrons Python package. For more information on the general setup and functions available read through the \href{https://docs.opentrons.com/v2/\#}{\texttt{ OT-\/2 API V2}}.

After initialising the required classes and Opentrons protocol context, the loop begins with a pre-\/protocol \char`\"{}flight check\char`\"{} whose purpose is to run a few basic tests to mitigate the risk the protocol will crash half way in, for example testing external modules like the \href{https://www.qinstruments.com/automation/bioshake-3000-t/}{\texttt{ Bio\+Shake 3000T}}. Next it iterates through all the steps in the instructions file deck configuration and list of steps read by the robot handler classes .

The {\ttfamily opentrons\+\_\+protocol\+\_\+helpers} classes manage additional protocol functionality. For example, if at any point an exception is encountered, it will be caught and sent via Slack to a debugging channel. If all goes well, the protocol will finish with a post-\/protocol \char`\"{}cleanup\char`\"{} step to ensure a graceful completion and log the experiment using an AWS service Cloudwatch instance. In the event of a failure log will include an error message. Webhooks for slack channels and AWS cloudwatch instances must be set as environment variables.


\begin{DoxyItemize}
\item SLACK\+\_\+\+LOG\+\_\+\+WEBHOOK\+: webhook of a slack channel to post in depth protocol activity.
\item SLACK\+\_\+\+MESSAGE\+\_\+\+WEBHOOK\+: webhook of a slack channel to post messages as steps in the instructions file.
\item CLOUD\+\_\+\+WATCH\+\_\+\+EP\+: url of the AWS Cloudwatch instance
\end{DoxyItemize}

For more detailed information on the transpiler, check the documentation for each class.\hypertarget{index_autotoc_md3}{}\doxysubsection{Command Line Interface}\label{index_autotoc_md3}
The generator can be used directly from the CLI {\ttfamily opentrons\+\_\+cli.\+py}


\begin{DoxyCode}{0}
\DoxyCodeLine{\$ python opentrons\_cli.py -\/-\/input <<ROBOT\_INSTRUCTIONS\_JSON\_FILE>> -\/-\/output <<PYTHON\_ROBOT\_PROTOCOL>>}

\end{DoxyCode}


and protocols can be simulated with {\ttfamily opentrons\+\_\+simulate.\+py}, a CLI that imports the {\ttfamily simulate} module from the Opentrons python package


\begin{DoxyCode}{0}
\DoxyCodeLine{\$ python opentrons\_simulate.py -\/-\/input <<PYTHON\_ROBOT\_PROTOCOL>>}

\end{DoxyCode}


The simulation output is the same as the one obtained when uploading a protocol to the \href{https://opentrons.com/ot-app/}{\texttt{ Opentrons App}}

Of course you can just use the CLI provided by the Opentrons Python package


\begin{DoxyItemize}
\item By calling {\ttfamily opentrons\+\_\+simulate} one can through the command line simulate any protocol.
\begin{DoxyItemize}
\item {\ttfamily usage\+: opentrons\+\_\+simulate \mbox{[}-\/h\mbox{]} \mbox{[}-\/l \{debug,info,warning,error,none\}\mbox{]} \mbox{[}-\/L CUSTOM\+\_\+\+LABWARE\+\_\+\+PATH\mbox{]} \mbox{[}-\/D \mbox{[}CUSTOM\+\_\+\+DATA\+\_\+\+PATH\mbox{]}\mbox{]} \mbox{[}-\/s CUSTOM\+\_\+\+HARDWARE\+\_\+\+SIMULATOR\+\_\+\+FILE\mbox{]} \mbox{[}-\/d CUSTOM\+\_\+\+DATA\+\_\+\+FILE\mbox{]} \mbox{[}-\/v\mbox{]} \mbox{[}-\/o \{runlog,nothing\}\mbox{]} PROTOCOL}
\end{DoxyItemize}
\end{DoxyItemize}\hypertarget{index_autotoc_md4}{}\doxysection{Generator server}\label{index_autotoc_md4}
To use the generator as the backend of a custom protocol designer, a flask server can be launched from {\ttfamily app.\+py} exposing REST API endpoints\+:

\tabulinesep=1mm
\begin{longtabu}spread 0pt [c]{*{4}{|X[-1]}|}
\hline
\PBS\centering \cellcolor{\tableheadbgcolor}\textbf{ Endpoint   }&\PBS\centering \cellcolor{\tableheadbgcolor}\textbf{ Type   }&\PBS\centering \cellcolor{\tableheadbgcolor}\textbf{ Input   }&\PBS\centering \cellcolor{\tableheadbgcolor}\textbf{ Output    }\\\cline{1-4}
\endfirsthead
\hline
\endfoot
\hline
\PBS\centering \cellcolor{\tableheadbgcolor}\textbf{ Endpoint   }&\PBS\centering \cellcolor{\tableheadbgcolor}\textbf{ Type   }&\PBS\centering \cellcolor{\tableheadbgcolor}\textbf{ Input   }&\PBS\centering \cellcolor{\tableheadbgcolor}\textbf{ Output    }\\\cline{1-4}
\endhead
{\ttfamily opidi-\/device/api/v1/generate/}   &POST   &\{ protocol\+: json\+\_\+instructions -\/$>$ obj \}   &\{\char`\"{}protocol\char`\"{}\+: protocol -\/$>$ str \}    \\\cline{1-4}
{\ttfamily opidi-\/device/api/v1/simulate/}   &POST   &\{ protocol\+: json\+\_\+instructions -\/$>$ obj \}   &\{\char`\"{}simulation\char`\"{}\+: simulation -\/$>$ str \}   \\\cline{1-4}
\end{longtabu}
\hypertarget{index_autotoc_md5}{}\doxysubsection{A note on Custom Labware}\label{index_autotoc_md5}
Opentrons have a set of native labware and accompanying \href{https://labware.opentrons.com/}{\texttt{ Labware Library}} containing the JSON based definitions for each piece of labware. However, there are situations where one wishes to add their own custom labware to the deck or to copy and rename the native labwares to match exactly those available in a particular lab. One example of this is the use of labware mounted on a \href{https://www.qinstruments.com/automation/bioshake-3000-t/}{\texttt{ Bio\+Shake 3000T}}. This repo contains a set of basic custom labware definitions that must be provided as input to the simulation scripts and also in the Opentrons app when uploading protocol to the robots. These definitions can be used as examples to creare new ones\hypertarget{index_autotoc_md6}{}\doxysection{Contributing}\label{index_autotoc_md6}
Clone the repo and install all package dependencies in a virtual environment\+: \begin{DoxyVerb}poetry install
\end{DoxyVerb}
 It can be useful to also have the Opentrons App installed for the simulation and testing packages on a physical Opentrons. The App Image can be found at\+: \href{https://opentrons.com/ot-app/}{\texttt{ https\+://opentrons.\+com/ot-\/app/}}

Documentation has been generated using \href{https://www.doxygen.nl/download.html}{\texttt{ Doxygen}} and can be run\+: doxygen Doxyfile

Repo has been Formatted according to google style guide using \href{https://github.com/google/yapf}{\texttt{ YAPF}} yapf . --style google -\/i -\/r And spell-\/checked using \href{https://facelessuser.github.io/pyspelling/}{\texttt{ pyspelling}} pyspelling 