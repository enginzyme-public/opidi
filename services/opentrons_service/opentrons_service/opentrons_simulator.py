"""Opentrons Simulator support functions

Simulates a given protocol using the native Opentrons Simulate library

@param input The protocol file containing the protocol to be simulated
"""
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import types
from pathlib import Path
import os
import json

from opentrons import simulate

# Copied from labware.py in the device_api
def get_labware_map() -> dict:
    """Returns a labware map object obtained from files in labware folder.

    Returns:
        dict: The said labware object
    """
    p = Path(".") # pylint: disable=invalid-name
    labware_path = os.path.join(p.parent.parent.resolve(), "", "shared_data", "labware")
    labware_map = {}

    for child in Path(labware_path).glob("*/*.json"):
        if child.is_file():
            labware_obj = json.loads(child.read_text())
            labware_id = labware_obj["parameters"]["loadName"]
            labware_map[labware_id] = labware_obj

    return labware_map

def run_simulation(protocol_string, labware_map):
    """Simulate the protocol passed in and return the output.

    @param protocol_string The protocol to be simulated
    @param labware_map Location of any custom labware that the protocol requires.
    @return simulation The output of the opentrons simulation
    """
    try:
        # Versions greater than 2.8 (2.15 was tested) fail with:
        # API version 2.15 is not supported by this robot software. Please either reduce your requested API version or update your robot.
        protocol_context = simulate.get_protocol_api('2.8',
                                                    extra_labware=labware_map)
        protocol_context.home()

        module = types.ModuleType('tools')

        exec(protocol_string, module.__dict__)

        protocol_success = module.run(protocol_context)

        simulation = "\n".join(protocol_context.commands())
        return simulation, protocol_success
    except Exception as e:
        # return an error message
        return "The simulation failed with the following error mesasge:\n" + str(e), False


def main(args):
    """Main loop to parse the arguments and call, protocol simulate function, and print out the simulation output

    @param args system arguments from the command line

    @return N/A
    """

    parser = argparse.ArgumentParser(
        description='Opentrons Protocol Simulator for API v2 ')
    parser.add_argument('-i', '--input', help='Protocol File', required=True)

    args = parser.parse_args()

    with open(args.input, 'r') as protocol_file:
        protocol_string = protocol_file.read()

    simulation, result = run_simulation(protocol_string, get_labware_map())

    if result:
        print('Simulation Successful')
    else:
        print('Simulation Failed')

    print(simulation)


if __name__ == '__main__':
    import sys

    sys.exit(main(sys.argv))
