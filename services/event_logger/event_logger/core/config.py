"""Configuration module."""

# pylint: disable=missing-docstring
from functools import lru_cache
from typing import Optional

from pydantic import Field, PostgresDsn, SecretStr
from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    ## Common settings
    project_name: str = "OPiDi Event logger service"
    project_environment: str = Field(
        "development",
        description="The environment the service is running in",
        alias="environment",
    )

    ## Database settings
    db_host: Optional[str] = Field(None, description="The database host")
    db_port: Optional[int] = Field(5432, description="The database port")
    db_user: Optional[str] = Field(None, description="The database user")
    db_password: Optional[SecretStr] = Field(None, description="The database password")
    db_name: Optional[str] = Field("opidi", description="The database name")

    @property
    def db_url(self) -> Optional[PostgresDsn]:
        if self.db_host is None:
            return None
        return PostgresDsn(
            f"postgresql://{self.db_user}:{self.db_password.get_secret_value()}@{self.db_host}:{self.db_port}/{self.db_name}"
        )

    @property
    def is_development(self) -> bool:
        return (
            True
            if self.project_environment.lower() in ["development", "dev", "local"]
            else False
        )

    model_config = SettingsConfigDict(
        case_sensitive=False,
        env_file=".env",
        env_file_encoding="utf-8",
        strict=False,
        extra="ignore",
    )


@lru_cache()
def get_settings():
    return Settings()


settings = get_settings()
