from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError
from sqlalchemy.orm import sessionmaker

from event_logger.core.config import settings
from event_logger.models.event import Base

# Database connection string
if settings.is_development and settings.db_host == None:
    # if we are working locally, we will ramp up a sqlite database
    DATABASE_URL = f"sqlite:///./.data/{settings.db_name}.db"
else:
    DATABASE_URL = settings.db_url

# Create an engine
engine = create_engine(DATABASE_URL)

# Create a configured "Session" class
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

# Import the Base and models


def init_db():
    """
    Initialize the database, creating tables if they do not exist.
    """
    try:
        print("Creating tables")
        Base.metadata.create_all(bind=engine)
    except OperationalError as e:
        print(f"Error creating tables: {e}")


# Initialize the database
init_db()
