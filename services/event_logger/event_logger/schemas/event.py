import time
from enum import Enum
from typing import Any, Dict, Optional, Union

from pydantic import BaseModel, ConfigDict, Field


class LogLevelEnum(str, Enum):
    CRITICAL = "CRITICAL"
    ERROR = "ERROR"
    WARNING = "WARNING"
    INFO = "INFO"
    DEBUG = "DEBUG"
    NOTSET = "NOTSET"


class Event(BaseModel):
    service: str = Field(..., description="Component that generated the log entry")
    level: LogLevelEnum = Field(LogLevelEnum.INFO, description="Log level of the entry")
    created_at: int = Field(int(time.time()), description="Timestamp of the log entry")
    message: Optional[str] = Field(None, description="Message of the log entry")
    event: Optional[Dict[str, Any]] = Field(
        None, description="Event that generated the log entry"
    )
    extra: Optional[Union[str, Dict[str, Any]]] = Field(
        None, description="Extra information about the log entry"
    )

    model_config = ConfigDict(extra="ignore", from_attributes=True)


class EventsQueryResponse(BaseModel):
    events: list[Event]

    model_config = ConfigDict(extra="ignore", from_attributes=True)
