from logging import Logger

from fastapi import APIRouter, Depends, HTTPException, Response
from sqlalchemy.orm import Session

from event_logger.core.db import SessionLocal
from event_logger.models.event import EventLog
from event_logger.schemas.event import Event, EventsQueryResponse

logger = Logger("event-logger")
router = APIRouter()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@router.post("/events", status_code=204)
async def receive_event(event: Event, db: Session = Depends(get_db)) -> Response:
    """
    Receive an event and log it in the database.

    Args:
        event (Event): The event object containing the event details.
        db (Session, optional): The database session. Defaults to Depends(get_db).

    Returns:
        Response: The HTTP response with status code 204.

    Raises:
        HTTPException: If the event is invalid.

    """
    if event.message is None and event.event is None:
        raise HTTPException(status_code=400, detail="Invalid event")

    logger.info("Received event from %s" % event.service)
    logger.debug("Event details: %s" % event.model_dump_json(exclude_none=True))

    event_log = EventLog(
        service=event.service,
        level=event.level,
        created_at=event.created_at,
        message=event.message,
        event=event.event,
        extra=event.extra,
    )
    db.add(event_log)
    db.commit()
    db.refresh(event_log)

    return Response(status_code=204)


@router.get("/events", response_model=EventsQueryResponse)
async def get_events(limit: int = 10, db: Session = Depends(get_db)) -> list[EventLog]:
    """
    Retrieve a list of events from the database.

    Args:
        limit (int): The maximum number of events to retrieve (default is 10).
        db (Session): The database session.

    Returns:
        list[EventLog]: A list of event logs.

    """
    logger.info("Retrieving %d events" % limit)
    events = db.query(EventLog).order_by(EventLog.created_at.desc()).limit(limit).all()

    return EventsQueryResponse(events=[Event.model_validate(event) for event in events])


@router.get("/events/{service}", response_model=EventsQueryResponse)
async def get_events_by_service(
    service: str, limit: int = 10, db: Session = Depends(get_db)
) -> list[EventLog]:
    """
    Retrieve events by service.

    Args:
        service (str): The name of the service.
        limit (int, optional): The maximum number of events to retrieve. Defaults to 10.
        db (Session, optional): The database session. Defaults to Depends(get_db).

    Returns:
        list[EventLog]: A list of event logs matching the specified service.
    """
    logger.info("Retrieving %d events for service %s" % limit, service)
    events = (
        db.query(EventLog)
        .filter(EventLog.service == service)
        .order_by(EventLog.created_at.desc())
        .limit(limit)
        .all()
    )

    return EventsQueryResponse(events=[Event.model_validate(event) for event in events])
