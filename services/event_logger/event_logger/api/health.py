"""Contains definition of health check endpoint controller."""

from fastapi import APIRouter
from starlette.responses import Response

router = APIRouter(prefix="/health", tags=["Health"])


@router.get("/", status_code=204)
def health() -> Response:
    """Health check endpoint controller."""

    return Response(status_code=204)
