# pylint: disable=missing-module-docstring
from importlib.metadata import version

from fastapi import APIRouter

from event_logger.api.event import router as event_router
from event_logger.api.health import router as health_router

router = APIRouter(prefix="/opidi-event-logger")
router.include_router(event_router)
router.include_router(health_router)
