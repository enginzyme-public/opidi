# pylint: disable=missing-module-docstring
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from event_logger.api import router
from event_logger.core.config import settings
from event_logger.core.db import init_db


async def application_lifespan(
    app: FastAPI,
):  # pylint: disable=missing-function-docstring
    print("Starting application lifespan event")
    init_db()
    yield


def create_application() -> FastAPI:  # pylint: disable=missing-function-docstring
    application = FastAPI(
        title=settings.project_name,
        docs_url="/opidi-event-logger/docs",
        redoc_url="/opidi-event-logger/redoc",
        openapi_url="/opidi-event-logger/openapi.json",
        lifespan=application_lifespan,
    )
    application.include_router(router)
    return application


app = create_application()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
