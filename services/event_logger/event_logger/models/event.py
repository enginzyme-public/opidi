# models/event.py
from sqlalchemy import JSON, Column, Integer, String, func
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.types import Enum as SQLAlchemyEnum

from event_logger.schemas.event import LogLevelEnum

Base = declarative_base()

class EventLog(Base):
    __tablename__ = "event_logs"
    
    event_id = Column(Integer, primary_key=True, autoincrement=True)
    service = Column(String, nullable=False)
    level = Column(SQLAlchemyEnum(LogLevelEnum), nullable=False, default=LogLevelEnum.INFO)
    created_at = Column(Integer, default=func.extract('epoch', func.now()), nullable=False)
    message = Column(String, nullable=True)
    event = Column(JSON, nullable=True)
    extra = Column(JSON, nullable=True)