# Event Logger Service for OPiDi

The Event Logger Service is a Python-based application designed to log events for the OPiDi platform. It utilizes FastAPI for its web framework, SQLAlchemy for database interactions, and Pydantic for data validation.

## Features

- **Event Logging**: Supports logging of various events with different log levels.
- **REST API**: Provides a RESTful API for logging and querying event logs.
- **Database Support**: Uses PostgreSQL for storing event logs.
- **Schema Validation**: Utilizes Pydantic for robust data validation and serialization.

## Requirements

- Python 3.9 or higher
- PostgreSQL
- Docker (optional for containerization)

## Installation

1. Clone the repository:
   ```sh
   git clone https://github.com/your-repository/event-logger.git
   cd event-logger
   ```

2. Install dependencies using Poetry:
   ```sh
   poetry install
   ```

3. Copy `.env.template` to `.env` and adjust the configuration variables accordingly.

## Database Setup

Ensure you have PostgreSQL installed and running. Create a database for the application and update the `.env` file with the database connection details.
Alternatively you can use a SQLite db as local environment.

## Running the Application

1. Initialize the database:
   ```sh
   poetry run python event_logger/core/db.py
   ```

2. Start the FastAPI server:
   ```sh
   poetry run uvicorn event_logger.main:app --reload
   ```

The API will be available at `http://localhost:8000/opidi-event-logger/`.

## Docker

To build and run the application using Docker:

1. Build the Docker image:
   ```sh
   docker build -t event-logger .
   ```

2. Run the container:
   ```sh
   docker run -d -p 8000:8000 event-logger
   ```

## API Documentation

Once the application is running, API documentation is available at `http://localhost:8000/opidi-event-logger/docs`.

## Testing

Run tests using pytest:

```sh
poetry run pytest
```

## Contributing

Contributions are welcome! Please open an issue or submit a pull request with your improvements.

## License

This project is licensed under the MIT License - see the LICENSE file for details.
