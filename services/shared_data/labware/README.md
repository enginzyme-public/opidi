# Labware Definitions

Tracks changes in the custom labware definitions used within the OPD

Use `custom` folder to store labware definitions that you don't want to commit to version control.
