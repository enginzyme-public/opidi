# Protocol CRUD API

Presents a CRUD interface on top of the persistence layer storing objects. This API is designed to manage protocols, labware definitions, pipettes, tags, and categories within a laboratory environment.

## Features

- **Protocols Management**: Create, read, update, and delete protocols.
- **Labware Definitions**: Manage labware definitions including listing and retrieving labware by ID.
- **Pipettes Handling**: CRUD operations for pipette configurations.
- **Tags and Categories**: Organize protocols and labware with tags and categories.

## Getting Started

### Prerequisites

- Python 3.9 or higher
- Docker (for containerization)
- AWS credentials configured (for S3 and DynamoDB interactions).
- Alternatively, you can use [localstack](https://www.localstack.cloud) to emulate S3 and DynamoDB

### Local Installation

1. Clone the repository:
   ```sh
   git clone <repository-url>
   ```
2. Install dependencies using Poetry:
   ```sh
   poetry install
   ```

### Running the Application

1. To run the application locally:
   ```sh
   uvicorn crud_api.main:app --reload
   ```
2. To build and run the application using Docker:
   ```sh
   docker build -t protocol-crud-api .
   docker run -p 80:80 protocol-crud-api
   ```

## API Endpoints

The API is structured under `/opidi-crud/api/v1` prefix. Here are some of the available endpoints:

- `/labware`: Manage labware items.
- `/pipette`: CRUD operations for pipettes.
- `/protocol`: Manage protocols.
- `/tag`: Handle tags for categorization.
- `/category`: Manage categories.

## Configuration

Configuration settings are managed through environment variables. Refer to `.env.template` for a list of required environment variables.

## Maintainer(s)

- tobi@enginzyme.com
- nadim@enginzyme.com
- nahuel@enginzyme.com

## License

MIT License
