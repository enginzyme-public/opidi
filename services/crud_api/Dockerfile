FROM python:3.10-slim-buster AS python-base

ENV PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    POETRY_HOME="/opt/poetry" \
    POETRY_VIRTUALENVS_IN_PROJECT=true \
    POETRY_NO_INTERACTION=1 \
    PYSETUP_PATH="/opt/pysetup" \
    VENV_PATH="/opt/pysetup/.venv"

ENV PATH="$POETRY_HOME/bin:$VENV_PATH/bin:$PATH"


# builder-base is used to build dependencies
FROM python-base AS builder-base
RUN apt-get update \
    && apt-get install --no-install-recommends -y \
    curl \
    build-essential

# Install Poetry - respects $POETRY_VERSION & $POETRY_HOME
RUN curl -sSL https://install.python-poetry.org | python

# We copy our Python requirements here to cache them
# and install only runtime deps using poetry
WORKDIR $PYSETUP_PATH

COPY crud_api/poetry.lock crud_api/pyproject.toml ./

RUN poetry install

FROM python-base AS main

# Copying poetry and venv into image
COPY --from=builder-base $POETRY_HOME $POETRY_HOME
COPY --from=builder-base $PYSETUP_PATH $PYSETUP_PATH

WORKDIR /code

COPY crud_api/ /code/crud_api
COPY shared_data/ /code/shared_data

WORKDIR /code/crud_api

# Required to use TLS enabled DocumentDB (see https://docs.aws.amazon.com/documentdb/latest/developerguide/connect_programmatically.html)
# ADD https://s3.amazonaws.com/rds-downloads/rds-combined-ca-bundle.pem /code/crud_api/rds-combined-ca-bundle.pem

CMD ["uvicorn", "crud_api.main:app", "--host", "0.0.0.0", "--port", "80"]
