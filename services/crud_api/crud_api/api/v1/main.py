"""Main file."""

from fastapi import APIRouter, Depends

from crud_api.api.v1 import controllers
from crud_api.core.auth import verify_access_method
from crud_api.core.config import settings

if settings.auth_method is not None:
    api_router = APIRouter(dependencies=[Depends(verify_access_method)], prefix="/v1")
else:
    api_router = APIRouter(prefix="/v1")

api_router.include_router(
    controllers.labware_def.router, prefix="/labware_def", tags=["labware_def"]
)
api_router.include_router(
    controllers.labware.router, prefix="/labware", tags=["labware"]
)
api_router.include_router(
    controllers.pipette.router, prefix="/pipette", tags=["pipette"]
)
api_router.include_router(
    controllers.protocol.router, prefix="/protocol", tags=["protocol"]
)
api_router.include_router(controllers.tag.router, prefix="/tag", tags=["tag"])
api_router.include_router(
    controllers.category.router, prefix="/category", tags=["category"]
)
