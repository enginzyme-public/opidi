"""Protocol controller."""

# pylint: disable=redefined-builtin
import json
from datetime import datetime
from typing import List, Optional
from uuid import uuid4

import boto3  # type: ignore
from dateutil.parser import parse
from fastapi import APIRouter, Depends, Header, HTTPException, Query
from pydantic import EmailStr  # pylint: disable=no-name-in-module

from crud_api import schemas
from crud_api.core.auth import get_token_identifier
from crud_api.core.config import settings
from crud_api.models.protocol import ProtocolEntity

router = APIRouter()


def get_s3_client():
    """Get S3 client."""

    if settings.opidi_crud_bucket_host:
        s3_client = boto3.resource(
            "s3",
            endpoint_url=settings.opidi_crud_bucket_host,
            region_name=settings.aws_default_region,
            aws_access_key_id=settings.s3_access_key_id or settings.aws_access_key_id,
            aws_secret_access_key=settings.s3_secret_access_key or settings.aws_secret_access_key,
            config=boto3.session.Config(signature_version='s3v4')
        )
    else:
        s3_client = boto3.resource(
            "s3",
            aws_access_key_id=settings.aws_access_key_id,
            aws_secret_access_key=settings.aws_secret_access_key,
        )

    return s3_client


@router.get("/", response_model=schemas.ProtocolList)
async def list_protocols(  # pylint: disable=invalid-name
    nameIncludes: Optional[str] = None,
    team: Optional[str] = None,
    tagIds: List[str] = Query([]),
    subcategory: Optional[str] = None,
    isShared: Optional[bool] = None,
    isVerified: Optional[bool] = None,
    nextToken: Optional[str] = None,
    limit: int = 100,
) -> schemas.ProtocolList:
    """
    Retrieve protocols.
    """

    try:
        condition = None

        if nameIncludes:
            if condition is None:
                condition = ProtocolEntity.name.contains(nameIncludes)
            else:
                condition &= ProtocolEntity.name.contains(nameIncludes)

        if isShared is not None:
            if condition is None:
                condition = ProtocolEntity.isShared == isShared
            else:
                condition &= ProtocolEntity.isShared == isShared

        if isVerified is not None:
            if condition is None:
                condition = ProtocolEntity.isVerified == isVerified
            else:
                condition &= ProtocolEntity.isVerified == isVerified

        if team:
            if condition is None:
                condition = ProtocolEntity.team == team  # type: ignore
            else:
                condition &= ProtocolEntity.team == team

        if tagIds:
            if condition is None:
                condition = ProtocolEntity.tagIds.contains(tagIds)
            else:
                condition &= ProtocolEntity.tagIds.contains(tagIds)

        if subcategory:
            if condition is None:
                condition = ProtocolEntity.subcategory == subcategory
            else:
                condition &= ProtocolEntity.subcategory == subcategory

        results_iter = ProtocolEntity.scan(
            filter_condition=condition,
        )

        protocols = sorted(
            [
                schemas.Protocol(**protocol.to_simple_dict())
                for protocol in results_iter
            ],
            key=lambda x: x.modifiedAt,
            reverse=True,
        )

        initialIndex = 0
        if nextToken:
            initialIndex = next(
                (i for i, x in enumerate(protocols) if str(x.id) == nextToken), 0
            )

        lastIndex = initialIndex + limit
        if lastIndex > len(protocols):
            lastIndex = len(protocols)

        next_token = None
        if initialIndex + limit <= len(protocols) - 1:
            next_token = str(protocols[initialIndex + limit].id)

        protocol_list = schemas.ProtocolList(
            protocols=protocols[initialIndex:lastIndex],
            nextToken=next_token,
        )

        return protocol_list
    except Exception as exc:
        print(exc)
        raise HTTPException(status_code=500, detail=f"Error: {exc}") from exc


@router.get("/batch", response_model=schemas.ProtocolList)
async def batch_get_protocols(  # pylint: disable=invalid-name
    protocolIds: list[str] = Query([]),
) -> schemas.ProtocolList:
    """
    Batch get protocols by list of ids
    """
    if len(protocolIds) == 0:
        protocols = []
    else:
        protocols = [
            schemas.Protocol(**protocol.to_simple_dict())
            for protocol in ProtocolEntity.batch_get(protocolIds)
        ]

    protocol_list = schemas.ProtocolList(protocols=protocols)

    return protocol_list


@router.post("/", response_model=schemas.Protocol)
async def create_protocol(
    *,
    protocol_in: schemas.ProtocolCreate,
    user_email: EmailStr = Depends(get_token_identifier),
) -> schemas.Protocol:
    """
    Create new protocol.
    """

    createdAt: datetime = datetime.now()
    createdBy: str = user_email
    modifiedAt: datetime = datetime.now()

    protocol_id = str(uuid4())

    try:
        nested_protocol = protocol_in.data

        if nested_protocol.get('metadata'):
            createdAt = parse(nested_protocol['metadata'].get('createdAt'))
            createdBy = nested_protocol['metadata'].get('createdBy')
            modifiedAt = parse(
                nested_protocol['metadata'].get('modifiedAt', str(createdAt))
            )

        # upload nested protocol to s3
        s3_client = get_s3_client()
        s3_client.Bucket(settings.opidi_crud_bucket_name).put_object(
            Key=f"{protocol_id}.json",
            Body=json.dumps(nested_protocol),
        )

        protocol_item = ProtocolEntity(
            id=protocol_id,
            name=protocol_in.name,
            description=protocol_in.description,
            isVerified=protocol_in.isVerified,
            isShared=protocol_in.isShared,
            team=protocol_in.team,
            tagIds=protocol_in.tagIds,
            subcategory=protocol_in.subcategory,
            createdAt=createdAt,
            modifiedAt=modifiedAt,
            createdBy=createdBy,
            parameterizedUserInput=(
                [_input.model_dump() for _input in protocol_in.parameterizedUserInput]
                if protocol_in.parameterizedUserInput
                else None
            ),
        )
        protocol_item.save()

        parsed_protocol = schemas.Protocol(**protocol_item.to_simple_dict())

        parsed_protocol.data = nested_protocol

        return parsed_protocol

    except Exception as exc:
        raise HTTPException(status_code=500, detail=f"Error: {exc}") from exc


@router.put("/{id}", response_model=schemas.Protocol)
async def update_protocol(
    *,
    id: str,
    protocol_in: schemas.ProtocolUpdate,
) -> schemas.Protocol:
    """
    Update a protocol.
    """

    try:
        protocol_item = ProtocolEntity.get(id)

        actions = [ProtocolEntity.modifiedAt.set(datetime.now())]

        if protocol_in.name:
            actions.append(ProtocolEntity.name.set(protocol_in.name))

        if protocol_in.description:
            actions.append(ProtocolEntity.description.set(protocol_in.description))

        if protocol_in.isVerified:
            actions.append(ProtocolEntity.isVerified.set(protocol_in.isVerified))

        if protocol_in.isShared:
            actions.append(ProtocolEntity.isShared.set(protocol_in.isShared))

        if protocol_in.team:
            actions.append(ProtocolEntity.team.set(protocol_in.team))

        if protocol_in.tagIds:
            actions.append(ProtocolEntity.tagIds.set(protocol_in.tagIds))

        if protocol_in.subcategory:
            actions.append(ProtocolEntity.subcategory.set(protocol_in.subcategory))

        if protocol_in.parameterizedUserInput is not None:
            actions.append(
                ProtocolEntity.parameterizedUserInput.set(
                    [_input.dict() for _input in protocol_in.parameterizedUserInput]
                )
            )

        protocol_item.update(actions=actions)  # type: ignore

        if protocol_in.data:
            s3_client = get_s3_client()
            s3_client.Bucket(settings.opidi_crud_bucket_name).put_object(
                Key=f"{id}.json",
                Body=json.dumps(protocol_in.data),
            )

        parsed_protocol = schemas.Protocol(**protocol_item.to_simple_dict())

        return parsed_protocol

    except Exception as exc:
        raise HTTPException(status_code=500, detail=f"Error: {exc}") from exc


@router.get("/{id}", response_model=schemas.Protocol)
async def read_protocol(
    *,
    id: str,
) -> schemas.Protocol:
    """
    Get protocol by ID.
    """

    try:
        protocol_item = ProtocolEntity.get(id)

        parsed_protocol = schemas.Protocol(**protocol_item.to_simple_dict())

        # get nested protocol from s3
        s3_client = get_s3_client()
        s3_object = s3_client.Object(settings.opidi_crud_bucket_name, f"{id}.json")
        nested_protocol = json.loads(s3_object.get()["Body"].read().decode("utf-8"))

        parsed_protocol.data = nested_protocol

        return parsed_protocol

    except Exception as exc:
        raise HTTPException(status_code=500, detail=f"Error: {exc}") from exc


@router.delete("/{id}", response_model=schemas.Protocol)
async def delete_protocol(
    *,
    id: str,
) -> schemas.Protocol:
    """
    Delete a protocol.
    """

    try:
        protocol_item = ProtocolEntity.get(id)

        parsed_protocol = schemas.Protocol(**protocol_item.to_simple_dict())

        protocol_item.delete()

        return parsed_protocol

    except Exception as exc:
        raise HTTPException(status_code=500, detail=f"Error: {exc}") from exc


@router.post("/admin-add", response_model=schemas.Protocol)
async def admin_create_protocol(
    *,
    protocol_in: schemas.ProtocolCreate,
    secret: str = Header(...),
    email: EmailStr = Header(...),
) -> schemas.Protocol:
    """
    Create new protocol.
    """

    if secret != settings.admin_secret:
        raise HTTPException(status_code=403, detail="Forbidden")

    try:
        current_time = datetime.now()
        protocol_id = str(uuid4())
        nested_protocol = protocol_in.data

        # upload nested protocol to s3
        s3_client = get_s3_client()
        s3_client.Bucket(settings.opidi_crud_bucket_name).put_object(
            Key=f"{protocol_id}.json",
            Body=json.dumps(nested_protocol),
        )

        protocol_item = ProtocolEntity(
            id=protocol_id,
            name=protocol_in.name,
            description=protocol_in.description,
            isVerified=protocol_in.isVerified,
            isShared=protocol_in.isShared,
            team=protocol_in.team,
            tagIds=protocol_in.tagIds,
            subcategory=protocol_in.subcategory,
            createdAt=current_time,
            modifiedAt=current_time,
            createdBy=email,
            parameterizedUserInput=(
                [_input.dict() for _input in protocol_in.parameterizedUserInput]
                if protocol_in.parameterizedUserInput
                else None
            ),
        )
        protocol_item.save()

        parsed_protocol = schemas.Protocol(**protocol_item.to_simple_dict())

        parsed_protocol.data = nested_protocol

        return parsed_protocol

    except Exception as exc:
        raise HTTPException(status_code=500, detail=f"Error: {exc}") from exc
