"""Labware definition controller."""

# pylint: disable=redefined-builtin
from typing import Any, Dict

from fastapi import APIRouter, Depends
from starlette.exceptions import HTTPException

from crud_api import schemas
from crud_api.deps.labware import get_labware_map

router = APIRouter()


@router.get("/", response_model=schemas.LabwareList)
def list_labwares(
    labwares: Dict[Any, Any] = Depends(get_labware_map)
) -> schemas.LabwareList:
    """
    Retrieve labwares.
    """
    return schemas.LabwareList(labwares=list(labwares.values()), nextToken=None)


@router.get("/{id}", response_model=schemas.Labware)
def read_labware(  # pylint: disable=invalid-name
    *, id: str, labwares: Dict[Any, Any] = Depends(get_labware_map)
) -> schemas.Labware:
    """
    Get labware by ID.
    """
    labware = labwares.get(id)
    if not labware:
        raise HTTPException(status_code=400, detail="labware not found")
    return schemas.Labware(**labware)
