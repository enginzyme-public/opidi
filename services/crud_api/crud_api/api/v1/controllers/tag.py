"""Tag controller."""

# pylint: disable=redefined-builtin
import json
from datetime import datetime
from typing import Optional
from uuid import uuid4

from fastapi import APIRouter, Depends, HTTPException
from pydantic import EmailStr  # pylint: disable=no-name-in-module

from crud_api import schemas
from crud_api.core.auth import get_token_identifier
from crud_api.models.tag import TagEntity

router = APIRouter()


@router.get("/", response_model=schemas.TagList)
async def list_tags(  # pylint: disable=invalid-name
    nameIncludes: Optional[str] = None,
    nextToken: Optional[str] = None,
    limit: int = 100,
) -> schemas.TagList:
    """
    Retrieve tags.
    """

    try:
        condition = None

        if nameIncludes:
            if condition is None:
                condition = TagEntity.name.contains(nameIncludes)
            else:
                condition &= TagEntity.name.contains(nameIncludes)

        last_evaluated_key = None
        if nextToken:
            last_evaluated_key = {
                "id": {
                    "S": nextToken,
                }
            }

        results_iter = TagEntity.scan(
            filter_condition=condition,
            last_evaluated_key=last_evaluated_key,
            limit=limit,
        )

        next_token = None

        if results_iter.last_evaluated_key:
            next_token = results_iter.last_evaluated_key["id"]["S"]

        tag_list = schemas.TagList(
            tags=[schemas.Tag(**tag.to_simple_dict()) for tag in results_iter],
            nextToken=next_token,
        )

        return tag_list

    except Exception as exc:
        raise HTTPException(status_code=500, detail=f"Error: {exc}") from exc


@router.post("/", response_model=schemas.Tag)
async def create_tag(
    *,
    tag_in: schemas.TagCreate,
    user_email: EmailStr = Depends(get_token_identifier),
) -> schemas.Tag:
    """
    Create new tag.
    """

    try:
        current_time = datetime.now()

        tag_item = TagEntity(
            id=str(uuid4()),
            name=tag_in.name,
            createdAt=current_time,
            modifiedAt=current_time,
            createdBy=user_email,
        )
        tag_item.save()

        parsed_tag = schemas.Tag(**tag_item.to_simple_dict())

        return parsed_tag

    except Exception as exc:
        raise HTTPException(status_code=500, detail=f"Error: {exc}") from exc


@router.put("/{id}", response_model=schemas.Tag)
async def update_tag(
    *,
    id: str,
    tag_in: schemas.TagUpdate,
) -> schemas.Tag:
    """
    Update a tag.
    """

    try:
        tag_item = TagEntity.get(id)

        actions = [TagEntity.modifiedAt.set(datetime.now())]

        if tag_in.name:
            actions.append(TagEntity.name.set(tag_in.name))

        tag_item.update(actions=actions)  # type: ignore

        parsed_tag = schemas.Tag(**tag_item.to_simple_dict())

        return parsed_tag

    except Exception as exc:
        raise HTTPException(status_code=500, detail=f"Error: {exc}") from exc


@router.get("/{id}", response_model=schemas.Tag)
async def read_tag(
    *,
    id: str,
) -> schemas.Tag:
    """
    Get tag by ID.
    """

    try:
        tag_item = TagEntity.get(id)

        parsed_tag = schemas.Tag(**tag_item.to_simple_dict())

        return parsed_tag

    except Exception as exc:
        raise HTTPException(status_code=500, detail=f"Error: {exc}") from exc


@router.delete("/{id}", response_model=schemas.Tag)
async def delete_tag(
    *,
    id: str,
) -> schemas.Tag:
    """
    Delete a tag.
    """

    try:
        tag_item = TagEntity.get(id)

        parsed_tag = schemas.Tag(**tag_item.to_simple_dict())

        tag_item.delete()

        return parsed_tag

    except Exception as exc:
        raise HTTPException(status_code=500, detail=f"Error: {exc}") from exc
