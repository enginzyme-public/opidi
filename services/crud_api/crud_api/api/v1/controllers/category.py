"""Project controller."""

from fastapi import APIRouter

from crud_api import schemas

router = APIRouter()


@router.get("/", response_model=schemas.CategoryList)
def list_categories() -> schemas.CategoryList:
    """
    Retrieve categories.
    """

    return schemas.CategoryList(
        categories=[
            schemas.Category(name="Automation - Testing", id="1"),
            schemas.Category(name="Automation - Checking", id="2"),
            schemas.Category(name="Biocat - Metal Deposition", id="3"),
            schemas.Category(name="Biocat - Immobilization", id="4"),
            schemas.Category(name="Biocat - X-linking", id="5"),
            schemas.Category(name="Biocat - Washes", id="6"),
            schemas.Category(name="Biocat - Leaching", id="7"),
            schemas.Category(name="Biocat - Activity assay", id="8"),
            schemas.Category(name="Biocat - Recyclability", id="9"),
        ]
    )
