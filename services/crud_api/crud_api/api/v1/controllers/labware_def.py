"""Labware definition controller."""

# pylint: disable=redefined-builtin
from typing import Any, Dict

from fastapi import APIRouter, Depends
from starlette.exceptions import HTTPException

from crud_api import schemas
from crud_api.deps.labware import get_labware_definitions_map

router = APIRouter()


@router.get("/", response_model=schemas.LabwareDefList)
def list_labware_defs(
    labware_defs: Dict[Any, Any] = Depends(get_labware_definitions_map)
) -> schemas.LabwareDefList:
    """
    Retrieve labware_defs.
    """
    return schemas.LabwareDefList(
        labwareDefs=list(labware_defs.values()), nextToken=None
    )


@router.get("/{id}", response_model=schemas.LabwareDef)
def read_labware_def(  # pylint: disable=invalid-name
    *, id: str, labware_defs: Dict[Any, Any] = Depends(get_labware_definitions_map)
) -> schemas.LabwareDef:
    """
    Get labware_def by ID.
    """
    labware_def = labware_defs.get(id)
    if not labware_def:
        raise HTTPException(status_code=400, detail="labware definition not found")
    return schemas.LabwareDef(**labware_def)
