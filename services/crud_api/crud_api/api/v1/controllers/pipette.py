"""Pipette definition controller."""

# pylint: disable=redefined-builtin
from typing import Any, Dict

from fastapi import APIRouter, Depends
from starlette.exceptions import HTTPException

from crud_api import schemas
from crud_api.deps.pipette import get_pipette_map

router = APIRouter()


@router.get("/", response_model=schemas.PipetteList)
def list_pipettes(
    pipettes: Dict[Any, Any] = Depends(get_pipette_map)
) -> schemas.PipetteList:
    """
    Retrieve pipettes.
    """
    return schemas.PipetteList(pipettes=list(pipettes.values()), nextToken=None)


@router.get("/{id}", response_model=schemas.Pipette)
def read_pipette(  # pylint: disable=invalid-name
    *, id: str, pipettes: Dict[Any, Any] = Depends(get_pipette_map)
) -> schemas.Pipette:
    """
    Get pipette by ID.
    """
    pipette = pipettes.get(id)
    if not pipette:
        raise HTTPException(status_code=400, detail="pipette definition not found")
    return schemas.Pipette(**pipette)
