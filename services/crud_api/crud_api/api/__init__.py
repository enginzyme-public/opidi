# pylint: disable=missing-module-docstring
from importlib.metadata import version

from fastapi import APIRouter

from crud_api.api.health import router as health_router
from crud_api.api.v1.main import api_router as v1_router

router = APIRouter(prefix="/opidi-crud/api")
router.include_router(v1_router)
router.include_router(health_router)


@router.get("/version", status_code=200, tags=["Package Versions"])
def get_app_version() -> str:
    """Return the version of the crud-api."""

    return version("crud_api")
