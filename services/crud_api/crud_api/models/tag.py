"""Tag model module."""

from pynamodb.attributes import UnicodeAttribute, UTCDateTimeAttribute
from pynamodb.models import Model

from crud_api.core.config import settings


class TagEntity(Model):
    """Tag ORM model."""

    class Meta:  # pylint: disable=missing-class-docstring
        table_name = settings.opidi_tag_crud_table_name
        region = settings.aws_default_region
        host = settings.opidi_tag_crud_table_host

    id = UnicodeAttribute(hash_key=True, null=False)
    name = UnicodeAttribute(null=False)
    createdAt = UTCDateTimeAttribute(null=False)
    modifiedAt = UTCDateTimeAttribute(null=False)
    createdBy = UnicodeAttribute(null=False)
