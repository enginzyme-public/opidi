"""Protocol model module."""

from pynamodb.attributes import (
    BooleanAttribute,
    ListAttribute,
    MapAttribute,
    UnicodeAttribute,
    UnicodeSetAttribute,
    UTCDateTimeAttribute,
)
from pynamodb.models import Model

from crud_api.core.config import settings


class ParameterizedUserInputMap(MapAttribute[str, str]):
    """ParameterizedUserInputMap ORM model."""
    type = UnicodeAttribute(null=True) # step or sequence
    id = UnicodeAttribute(null=True) # id of the step or the sequence
    path = UnicodeAttribute(null=True) # path from the root to the step or the sequence
    label = UnicodeAttribute(null=False)
    dataType = UnicodeAttribute(null=False)


class ProtocolEntity(Model):
    """Protocol ORM model."""

    class Meta:  # pylint: disable=missing-class-docstring
        table_name = settings.opidi_protocol_crud_table_name
        region = settings.aws_default_region
        host = settings.opidi_protocol_crud_table_host

    id = UnicodeAttribute(hash_key=True, null=False)
    name = UnicodeAttribute(null=False)
    description = UnicodeAttribute(null=False)
    isVerified = BooleanAttribute(null=False)
    isShared = BooleanAttribute(null=False)
    createdAt = UTCDateTimeAttribute(null=False)
    modifiedAt = UTCDateTimeAttribute(null=False)
    createdBy = UnicodeAttribute(null=False)
    team = UnicodeAttribute(null=True)
    tagIds = UnicodeSetAttribute(null=True)
    subcategory = UnicodeAttribute(null=True)
    parameterizedUserInput = ListAttribute(of=ParameterizedUserInputMap, null=True)
