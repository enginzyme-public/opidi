"""Pipette schemas."""

# pylint: disable=missing-class-docstring
from typing import Any, Dict, List, Optional

from pydantic import BaseModel  # pylint: disable=no-name-in-module


# Shared properties
class PipetteBase(BaseModel):
    pipette: Dict[Any, Any]


# Additional properties to return via API
class Pipette(PipetteBase):
    id: str


class PipetteList(BaseModel):
    pipettes: List[Pipette]
    nextToken: Optional[str] = None
