"""Protocol schemas."""

# pylint: disable=missing-class-docstring
from datetime import datetime
from enum import Enum
from typing import Any, Dict, List, Optional, Set, Union
from uuid import UUID

from pydantic import BaseModel, EmailStr  # pylint: disable=no-name-in-module


class InputType(str, Enum):
    step = "step"
    sequence = "sequence"


class ParameterizedUserInputMap(BaseModel):
    type: InputType = InputType.step  # Step or Sequence
    id: Union[int, str] = None  # Step or Sequence id
    path: str = None  # Path from the root to the step or the sequence
    label: str
    dataType: str


# Shared properties
class ProtocolBase(BaseModel):
    name: str
    description: str
    isVerified: bool = False
    isShared: bool = False
    team: Optional[str] = None
    tagIds: Optional[Set[str]] = None
    subcategory: Optional[str] = None
    data: Optional[Dict[str, Any]] = None
    parameterizedUserInput: Optional[List[ParameterizedUserInputMap]] = None


# Properties to receive via API on creation
class ProtocolCreate(ProtocolBase):
    team: str
    subcategory: str


# Properties to receive via API on update
class ProtocolUpdate(ProtocolBase):
    pass


class Protocol(ProtocolBase):
    id: UUID
    createdBy: EmailStr
    createdAt: datetime
    modifiedAt: datetime


class ProtocolList(BaseModel):
    protocols: List[Protocol]
    nextToken: Optional[str] = None
