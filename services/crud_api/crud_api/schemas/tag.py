"""Tag schemas."""

# pylint: disable=missing-class-docstring
from datetime import datetime
from typing import List, Optional
from uuid import UUID

from pydantic import BaseModel, EmailStr  # pylint: disable=no-name-in-module


# Shared properties
class TagBase(BaseModel):
    name: str


# Properties to receive via API on creation
class TagCreate(TagBase):
    pass


# Properties to receive via API on update
class TagUpdate(TagBase):
    pass


class Tag(TagBase):
    id: UUID
    createdBy: EmailStr
    createdAt: datetime
    modifiedAt: datetime


class TagList(BaseModel):
    tags: List[Tag]
    nextToken: Optional[str] = None
