"""Labware schemas."""

# pylint: disable=missing-class-docstring
from typing import Any, Dict, List, Optional

from pydantic import BaseModel  # pylint: disable=no-name-in-module


# Shared properties
class LabwareBase(BaseModel):
    labware: Dict[Any, Any]


# Additional properties to return via API
class Labware(LabwareBase):
    id: str


class LabwareList(BaseModel):
    labwares: List[Labware]
    nextToken: Optional[str] = None
