"""Labware Definition schemas."""

# pylint: disable=missing-class-docstring
from typing import Any, Dict, List, Optional

from pydantic import BaseModel  # pylint: disable=no-name-in-module


# Shared properties
class LabwareDefBase(BaseModel):
    labwareDef: Dict[Any, Any]


# Additional properties to return via API
class LabwareDef(LabwareDefBase):
    id: str


class LabwareDefList(BaseModel):
    labwareDefs: List[LabwareDef]
    nextToken: Optional[str] = None
