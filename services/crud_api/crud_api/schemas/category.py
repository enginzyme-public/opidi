from typing import List

from pydantic import BaseModel


class Category(BaseModel):
    """Protocol category"""

    id: str
    name: str


class CategoryList(BaseModel):
    """Protocol category list"""

    categories: List[Category]
