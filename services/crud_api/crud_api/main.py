# pylint: disable=missing-module-docstring
import boto3  # type: ignore
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from crud_api.api import router
from crud_api.core.config import settings
from crud_api.models.protocol import ProtocolEntity
from crud_api.models.tag import TagEntity


async def application_lifespan(
    app: FastAPI,
):  # pylint: disable=missing-function-docstring
    """Create tables if they don't exist."""

    if settings.opidi_protocol_crud_table_host and not ProtocolEntity.exists():
        print("Creating protocol table...")
        ProtocolEntity.create_table(
            read_capacity_units=1, write_capacity_units=1, wait=True
        )

    if settings.opidi_tag_crud_table_host and not TagEntity.exists():
        print("Creating tag table...")
        TagEntity.create_table(read_capacity_units=1, write_capacity_units=1, wait=True)

    if settings.opidi_crud_bucket_host:
        s3_client = boto3.resource(
            "s3",
            endpoint_url=settings.opidi_crud_bucket_host,
            region_name=settings.aws_default_region,
            aws_access_key_id=settings.s3_access_key_id or settings.aws_access_key_id,
            aws_secret_access_key=settings.s3_secret_access_key or settings.aws_secret_access_key,
            config=boto3.session.Config(signature_version='s3v4')
        )

        # Create bucket if it doesn't exist
        if (
            not s3_client.Bucket(settings.opidi_crud_bucket_name)
            in s3_client.buckets.all()
        ):
            print("Creating bucket...")
            s3_client.create_bucket(
                Bucket=settings.opidi_crud_bucket_name,
                CreateBucketConfiguration={
                    "LocationConstraint": settings.aws_default_region
                },
            )
    yield


def create_application() -> FastAPI:  # pylint: disable=missing-function-docstring
    application = FastAPI(
        title=settings.project_name,
        docs_url="/opidi-crud/docs",
        redoc_url="/opidi-crud/redoc",
        openapi_url="/opidi-crud/openapi.json",
        lifespan=application_lifespan,
    )
    application.include_router(router)
    return application


app = create_application()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
