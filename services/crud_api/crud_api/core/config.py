"""Configuration module."""

# pylint: disable=missing-docstring
from functools import lru_cache
from typing import Dict, Optional

from pydantic import Field
from pydantic_settings import BaseSettings, SettingsConfigDict
import os

class Settings(BaseSettings):
    ## Common settings
    project_name: str = "OPiDi CRUD Service"

    ## Auth settings

    google_client_id: Optional[str] = Field(None, description="Google client ID")

    admin_secret: Optional[str] = Field(
        None, description="The secret key to authenticate as an admin"
    )

    session_timeout: Optional[int] = Field(120, description="The session timeout in minutes")

    ## AWS settings
    aws_default_region: str = Field("eu-north-1", description="The default AWS region")
    aws_access_key_id: str = Field("fake-key", description="The AWS access key ID")
    aws_secret_access_key: str = Field(
        "fake-key", description="The AWS secret access key"
    )

    ## Extra S3 settings
    s3_access_key_id: Optional[str] = Field(None, description="The S3 access key ID")
    s3_secret_access_key: Optional[str] = Field(None, description="The S3 access key ID")

    ## DynamoDB settings
    opidi_protocol_crud_table_name: str = Field(
        "opidi-protocol", description="The name of the DynamoDB table for protocols"
    )
    opidi_protocol_crud_table_host: Optional[str] = Field(
        None, description="The host of the DynamoDB table for protocols"
    )
    opidi_tag_crud_table_name: str = Field(
        "opidi-tag", description="The name of the DynamoDB table for tags"
    )
    opidi_tag_crud_table_host: Optional[str] = Field(
        None, description="The URL host of the DynamoDB table for tags"
    )

    ## S3 settings
    opidi_crud_bucket_name: str = Field(
        "enginzyme-opidi-protocol-dev", description="The name of the S3 bucket"
    )
    opidi_crud_bucket_host: Optional[str] = Field(
        None, description="The URL host of the S3 bucket"
    )

    ## Robot info
    opirun_tokens: Optional[Dict[str, str]] = Field({}, description="opirun tokens")

    ## Event logging
    event_logger_url: Optional[str] = Field(None, description="Event logger URL")

    @property
    def token2robot(self) -> Dict[str, str]:
        return {v: k for k, v in self.opirun_tokens.items()}

    @property
    def auth_method(self) -> Optional[str]:
        if self.google_client_id:
            return "jwt"
        elif self.admin_secret:
            return "secret-key"
        else:
            return None

    model_config = SettingsConfigDict(
        case_sensitive=False,
        env_file=".env",
        env_file_encoding="utf-8",
        strict=False,
        extra="ignore",
    )


@lru_cache()
def get_settings():
    return Settings()


settings = get_settings()