import jwt
import warnings
from cachetools import TTLCache, cached
from fastapi import Depends, Header, HTTPException
from google.auth.transport import requests
from google.oauth2 import id_token

from crud_api.core.config import settings

# Convert session timeout from minutes to seconds
SESSION_TIMEOUT_SECONDS = settings.session_timeout * 60
token_cache = TTLCache(maxsize=1000, ttl=SESSION_TIMEOUT_SECONDS)

def verify_secret_key(auth_token: str) -> bool:
    """Verify the API key provided by the user.

    This function compares the provided `auth_token` with the admin secret key
    stored in the settings module.

    Parameters
    ----------
    auth_token : str
        The API key to be verified.

    Returns
    -------
    bool
        True if the API key is valid, False otherwise.
    """

    return auth_token == settings.admin_secret


def verify_token_with_google(token: str) -> dict:
    """Verify the token with Google servers."""
    try:
        idinfo = id_token.verify_oauth2_token(
            token, requests.Request(), settings.google_client_id
        )
        return idinfo
    except ValueError:
        raise HTTPException(status_code=401, detail="Invalid token")

@cached(token_cache)
def get_verified_token(token: str) -> dict:
    """Get a verified token, either from cache or by verifying with Google.   Note the use of the token_cache to
    define the TTL of the token, .a.k.a. the Session Timeout"""
    print("Verifying token...")
    return verify_token_with_google(token)

def verify_jwt_key(auth_token: str) -> bool:
    """
    # This will either return a cached result or verify with Google
    Verify the JWT token provided by the user.


    Parameters
    ----------
    auth_token : str
        The JWT token provided by the user.

    Returns
    -------
    bool
        True if the JWT token is valid, False otherwise.
    """
    try:

        verified_token = get_verified_token(auth_token)
        return True
    except HTTPException:
        return False

def verify_opirun_token(opirun_token: str) -> bool:
    """
    Verify the Opirun token provided by the user.

    Parameters
    ----------
    opirun_token : str
        The Opirun token provided by the user.

    Returns
    -------
    bool
        True if the Opirun token is valid, False otherwise.
    """
    if not settings.opirun_tokens:
        warnings.warn("No robot configuration found... Opirun connection will not work")
        return False

    if opirun_token in settings.opirun_tokens.values():
        return True
    return False


def verify_access_method(
    auth_token: str = Header(
        default="",
        alias="Bearer-Token",
        description="Token to authenticate",
    ),
    opirun_token: str = Header(
        default="",
        alias="X-Opirun-Token",
        description="Token to authenticate",
    ),
) -> None:
    """This function will verify the access method provided by the user.

    Parameters
    ----------
    auth_token : str
        Token to authenticate.

    Raises
    ----------
        HTTPException: Invalid credentials.
    """

    if opirun_token:
        if not verify_opirun_token(opirun_token):
            raise HTTPException(
                status_code=401,
                detail="Robot token is invalid. Check configuration",
            )
    elif settings.auth_method == "secret-key":
        if not verify_secret_key(auth_token):
            raise HTTPException(
                status_code=401,
                detail="Invalid credentials",
            )
    elif settings.auth_method == "jwt":
        if not verify_jwt_key(auth_token):
            raise HTTPException(
                status_code=401,
                detail="Invalid credentials",
            )
    else:
        raise HTTPException(
            status_code=401,
            detail="Invalid credentials",
        )


def get_token_identifier(
        auth_token: str = Header(
            default="",
            alias="Bearer-Token",
            description="Token to authenticate",
        ),
        opirun_token: str = Header(
            default="",
            alias="X-Opirun-Token",
            description="Token to authenticate",
        ),
) -> str:
    """
    Gets the email address or identifier from the token.

    Args:
        auth_token (str): JWT token.
        opirun_token (str): Opirun-specific token.

    Returns:
        str: Email address or identifier from the token.
    """
    if opirun_token:
        return settings.token2robot.get(opirun_token)

    if settings.auth_method == "secret-key":
        return "admin@opidi.com"

    if auth_token:
        try:
            # Decode the JWT token without verification
            decoded_token = jwt.decode(auth_token, options={"verify_signature": False})
            email = decoded_token.get("email")
            if email:
                return email
        except jwt.DecodeError:
            # If decoding fails, return False or handle as needed
            return "unknown@opidi.com"

