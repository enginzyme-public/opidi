"""Labware dependencies."""

import json
from functools import lru_cache
from pathlib import Path
from typing import Any, Dict


@lru_cache
def get_labware_map() -> Dict[Any, Any]:
    """
    Get the labware map.
    """
    base_path = Path(".").resolve()
    print(base_path)
    folder_path = base_path.parent / "shared_data/labware"
    labware_map = {}

    for child in folder_path.glob('*/*.json'):
        if child.is_file():
            labware_obj = json.loads(child.read_text())
            labware_id = labware_obj["parameters"]["loadName"]
            labware_map[labware_id] = {
                "id": labware_id,
                "labware": {
                    "labware_id": labware_obj["parameters"]["loadName"],
                    "labware_name": labware_obj["metadata"]["displayName"],
                    "labware_type": (
                        "shakerLabware"
                        if "3000t" in labware_id.lower()
                        else labware_obj["metadata"]["displayCategory"]
                    ),
                },
            }

    return labware_map


@lru_cache
def get_labware_definitions_map() -> Dict[Any, Any]:
    """
    Get the labware definitions map.
    """

    base_path = Path(".").resolve()
    folder_path = base_path.parent / "shared_data/labware"

    labware_definitions = {}

    for child in folder_path.glob("*/*.json"):
        if child.is_file():
            labware_obj = json.loads(child.read_text())
            labware_id = labware_obj["parameters"]["loadName"]
            labware_definitions[labware_id] = {
                "id": labware_id,
                "labwareDef": labware_obj,
            }

    return labware_definitions
