"""Pipette dependencies."""

import json
from functools import lru_cache
from pathlib import Path
from typing import Any, Dict


@lru_cache
def get_pipette_map() -> Dict[Any, Any]:
    """
    Get the pipette map.
    """

    base_path = Path(".").resolve()
    folder_path = base_path.parent / "shared_data/pipettes"

    pipette_map = {}

    for child in folder_path.glob("*.json"):
        if child.is_file():
            pipette_obj = json.loads(child.read_text())
            pipette_id = pipette_obj["id"]
            pipette_map[pipette_id] = {"id": pipette_id, "pipette": pipette_obj}

    return pipette_map
