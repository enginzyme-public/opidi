
# Device API

Device API is a Python-based application designed to provide API access to liquid handling robot devices. It is built with support for the Opentrons API v2, facilitating the generation of Opentrons protocol files from a set of instructions in a JSON file.
## Features

- **Protocol Generation**: Generate Opentrons protocol files from JSON instructions, supporting a custom protocol designer webapp.
- **Simulation and Testing**: Simulate protocols with support for custom labware definitions, including integration with external modules like the BioShake 3000T.
- **Notification and Logging**: Exception handling with notifications via Slack and logging using AWS Cloudwatch.

## Getting Started

### Prerequisites

- Python 3.10
- Poetry for dependency management
- Opentrons Transpiler CLI pacakge (If you clone the OPiDi repo it should be found [here](../opentrons_service/README.md))

### Installation

1. Clone the repository:

    ```sh
    git clone https://yourrepositoryurl.com/device_api.git
    ```

2. Install dependencies using Poetry:

    ```sh
    cd device_api
    poetry install
    ```

### Usage

To generate a protocol file from a JSON instructions file:

```sh
python opentrons_cli.py --input <<ROBOT_INSTRUCTIONS_JSON_FILE>> --output <<PYTHON_ROBOT_PROTOCOL>>
```

To simulate a protocol:

```sh
python opentrons_simulate.py
```

## Contributing

Contributions are welcome! Please read through the [contributing guidelines](https://enginzyme.gitlab.io/opentrons/opidi/). Included are directions for opening issues, coding standards, and notes on development.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.

## Acknowledgments

- Opentrons for their API and labware library
- [QInstruments](https://www.qinstruments.com/automation/bioshake-3000-t/) for the BioShake 3000T integration

## Contact

For any inquiries, please reach out to Tobi Ogunbayo <tobi@enginzyme.com>.