# pylint: disable=missing-module-docstring
from fastapi import APIRouter

from device_api.api.health import router as health_router
from device_api.api.v1.main import api_router as v1_router

router = APIRouter(prefix="/opidi-device/api")
router.include_router(v1_router)
router.include_router(health_router)
