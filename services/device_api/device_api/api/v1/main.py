"""Main file."""

from fastapi import APIRouter, Depends
from device_api.core.auth import verify_access_method

from device_api.api.v1 import controllers

api_router = APIRouter(dependencies=[Depends(verify_access_method)], prefix="/v1")
api_router.include_router(controllers.generator.router, prefix="/generate")
api_router.include_router(controllers.simulator.router, prefix="/simulate")
