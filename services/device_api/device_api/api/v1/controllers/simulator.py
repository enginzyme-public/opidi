"""Labware definition controller."""

# pylint: disable=redefined-builtin
from typing import Any, Dict

from fastapi import APIRouter, Depends

from device_api.api.v1 import schemas
from device_api.deps.labware import get_labware_map
from opentrons_service.opentrons_generator import generate_protocol_string
from opentrons_service.opentrons_simulator import run_simulation

router = APIRouter()


@router.post("/", response_model=schemas.SimulationDump)
async def simulate(
    *,
    protocol: schemas.Protocol,
    labware_map: Dict[Any, Any] = Depends(get_labware_map),
) -> schemas.SimulationDump:
    """
    Generate simulation dump.
    """
    protocol = generate_protocol_string(protocol.data)
    simulation, is_successful = run_simulation(protocol, labware_map)
    return schemas.SimulationDump(simulation=simulation, isSuccessful=is_successful)
