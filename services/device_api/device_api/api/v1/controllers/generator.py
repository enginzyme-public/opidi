"""Labware definition controller."""

# pylint: disable=redefined-builtin
from fastapi import APIRouter

from device_api.api.v1 import schemas
from opentrons_service.opentrons_generator import generate_protocol_string

router = APIRouter()


@router.post("/", response_model=schemas.ProtocolDump)
def generate(
    *,
    protocol: schemas.Protocol,
) -> schemas.ProtocolDump:
    """
    Generate protocol dump.
    """
    return schemas.ProtocolDump(protocol=generate_protocol_string(protocol.data))
