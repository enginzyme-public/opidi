"""Controller schemas."""

from typing import Any, Dict

from pydantic import BaseModel  # pylint: disable=no-name-in-module


class ProtocolDump(BaseModel):
    """Protocol dump schema."""

    protocol: str


class SimulationDump(BaseModel):
    """Simulation dump schema."""

    simulation: str
    isSuccessful: bool


class Protocol(BaseModel):
    """Protocol schema."""

    data: Dict[Any, Any]
