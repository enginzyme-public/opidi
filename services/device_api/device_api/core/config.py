"""Configuration module."""

# pylint: disable=missing-docstring
from functools import lru_cache
from typing import Dict, Optional

from pydantic import BaseSettings, Field


class Settings(BaseSettings):
    ## Common settings
    project_name: str = "OPiDi Device Service"

    ## Auth settings

    google_client_id: Optional[str] = Field(None, description="Google client ID")

    session_timeout: Optional[int] = Field(120, description="The session timeout in minutes")

    admin_secret: Optional[str] = Field(
        None, description="The secret key to authenticate as an admin"
    )

    event_logger_url: Optional[str] = Field(None, description="Event logger URL")

    ## Robot info
    opirun_tokens: Optional[Dict[str, str]] = Field({}, description="opirun tokens")

    @property
    def token2robot(self) -> Dict[str, str]:
        return {v: k for k, v in self.opirun_tokens.items()}

    @property
    def auth_method(self) -> Optional[str]:
        if self.google_client_id:
            return "jwt"
        elif self.admin_secret:
            return "secret-key"
        else:
            return None

    class Config:
        case_sensitive = False
        env_file = '.env'
        env_file_encoding = 'utf-8'
        extra = 'ignore'
        strict = False


@lru_cache()
def get_settings():
    return Settings()


settings = get_settings()