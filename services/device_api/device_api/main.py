# pylint: disable=missing-module-docstring
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from device_api.api import router
from device_api.core.config import settings


def create_application() -> FastAPI:  # pylint: disable=missing-function-docstring
    application = FastAPI(
        title=settings.project_name,
        docs_url="/opidi-device/docs",
        redoc_url="/opidi-device/redoc",
        openapi_url="/opidi-device/openapi.json",
    )
    application.include_router(router)
    return application


app = create_application()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
