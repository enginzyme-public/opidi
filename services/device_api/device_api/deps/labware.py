"""Labware dependencies."""

import json
import os
from functools import lru_cache
from pathlib import Path


@lru_cache
def get_labware_map() -> dict:
    """Returns a labware map object obtained from files in labware folder.

    Returns:
        dict: The said labware object
    """
    base_path = Path(".").resolve()
    folder_path = base_path.parent / "shared_data/labware"
    labware_map = {}

    for child in folder_path.glob('*/*.json'):
        if child.is_file():
            labware_obj = json.loads(child.read_text())
            labware_id = labware_obj["parameters"]["loadName"]
            labware_map[labware_id] = labware_obj

    return labware_map
