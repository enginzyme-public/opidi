services:

  traefik:
    image: traefik:v3.1
    container_name: traefik
    command:
      - "--api.insecure=true"
      - "--providers.docker=true"
      - "--entrypoints.web.address=:80"
      - "--log.format=json"
      - "--log.level=INFO"
    ports:
      - "80:80"
      - "4200:8080"
    # labels:
    #   # Basic Auth Middleware for Traefik Dashboard
    #   ## you can generate a given user/password using htpasswd
    #   ## > echo $(htpasswd -nb user password) | sed -e s/\\$/\\$\\$/g
    #   - "traefik.http.middlewares.simple-auth.basicauth.users=user:$$apr1$$IPB1ATLZ$$hRsaaAwg/CwyyHDtp3cwK."
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock:ro"
    networks:
      - opidi-network

  dynamodb-local:
    command: "-jar DynamoDBLocal.jar -sharedDb -dbPath ./data -port 4566"
    image: "amazon/dynamodb-local:latest"
    container_name: dynamodb-local
    ports:
     - "4566:4566"
    volumes:
     - "${DYNAMODB_LOCAL_DATA_PATH:-./.data/dynamodb}:/home/dynamodblocal/data"
    working_dir: /home/dynamodblocal
    networks:
      - opidi-network

  minio:
    container_name: minio
    image: minio/minio:latest
    volumes:
      - "${MINIO_STORAGE_LOCATION:-./.data/minio}:/data"
    ports:
      - "${MINIO_PORT:-9000}:${MINIO_PORT:-9000}"
      - "9001:9001" ## Console
    networks:
      - opidi-network
    restart: always      
    environment:
      - MINIO_ROOT_USER=${AWS_ACCESS_KEY_ID:-fakeKey}
      - MINIO_ROOT_PASSWORD=${AWS_SECRET_ACCESS_KEY:-fakeSecret}
      - MINIO_API_PORT_NUMBER=${MINIO_PORT:-9000}
    command: server /data --console-address ":9001"
    healthcheck:
      test: ["CMD", "mc", "ready", "local"]
      interval: 10s
      timeout: 5s
      retries: 5

  crud-api:
    build:
      context: ./services
      dockerfile: ./crud_api/Dockerfile
    depends_on:
      minio:
        condition: service_healthy
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.crud-api.rule=PathPrefix(`/opidi-crud`)"
      - "traefik.http.services.crud-api.loadbalancer.server.port=80"
    container_name: crud-api
    environment:
      - AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID:-fakeKey}
      - AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY:-fakeSecret}
      - GOOGLE_CLIENT_ID=${GOOGLE_CLIENT_ID:-}
      - OPIDI_CRUD_BUCKET_HOST=${OPIDI_CRUD_BUCKET_HOST:-http://minio:9000}
      - OPIDI_PROTOCOL_CRUD_TABLE_HOST=${OPIDI_PROTOCOL_CRUD_TABLE_HOST:-http://dynamodb-local:4566}
      - OPIDI_TAG_CRUD_TABLE_HOST=${OPIDI_TAG_CRUD_TABLE_HOST:-http://dynamodb-local:4566}
      - OPIRUN_TOKENS=${OPIRUN_TOKENS:-{}}
      - SESSION_TIMEOUT=${SESSION_TIMEOUT:-3600}
    networks:
      - opidi-network
    develop:
      watch:
        - action: sync+restart
          path: ./services/crud_api
          target: /code/crud_api
          ignore:
            - .venv/
        - action: rebuild
          path: ./services/crud_api/pyproject.toml

  device-api:
    build:
      context: ./services
      dockerfile: ./device_api/Dockerfile
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.device-api.rule=PathPrefix(`/opidi-device`)"
      - "traefik.http.services.device-api.loadbalancer.server.port=80"
    container_name: device-api
    environment:
      - GOOGLE_CLIENT_ID=${GOOGLE_CLIENT_ID:-}
      - OPIRUN_TOKENS=${OPIRUN_TOKENS:-{}}
      - SESSION_TIMEOUT=${SESSION_TIMEOUT:-3600}
    networks:
      - opidi-network
    develop:
      watch:
        - action: sync+restart
          path: ./services/device_api
          target: /code/device_api
          ignore:
            - .venv/
        - action: rebuild
          path: ./services/device_api/pyproject.toml

  event-logger:
    build:
      context: ./services
      dockerfile: ./event_logger/Dockerfile
    container_name: event-logger
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.event-logger.rule=PathPrefix(`/opidi-event-logger`)"
      - "traefik.http.services.event-logger.loadbalancer.server.port=80"
      #- "traefik.http.routers.event-logger.middlewares=simple-auth"
    networks:
      - opidi-network
    volumes:
      - "./.data:/code/event_logger/.data"
    develop:
      watch:
        - action: sync+restart
          path: ./services/event_logger
          target: /code/event_logger
          ignore:
            - .venv/
        - action: rebuild
          path: ./services/event_logger/pyproject.toml
  
  frontend:
    build:
      context: ./client
      dockerfile: Dockerfile
      args:
        - VUE_APP_MOCK_BACKEND=${VUE_APP_MOCK_BACKEND:+1}
        - VUE_APP_LOG_GROUP_NAME=${VUE_APP_LOG_GROUP_NAME:-opidi-dev}
        - VUE_APP_GOOGLE_AUTH_CLIENT_ID=${VUE_APP_GOOGLE_AUTH_CLIENT_ID:-}
        - APP_VERSION=${APP_VERSION:-0.0.0-alpha}
    container_name: frontend
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.frontend.rule=PathPrefix(`/`)"
      - "traefik.http.routers.frontend.entrypoints=web"
      - "traefik.http.services.frontend.loadbalancer.server.port=80"
    networks:
      - opidi-network
    depends_on:
      - crud-api
      - device-api
    develop:
      watch:
        - action: sync+restart
          path: ./client
          target: /app
          ignore:
            - node_modules/
        - action: rebuild
          path: ./client/package.json

networks:
  opidi-network:

